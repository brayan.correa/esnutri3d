using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Imagenes : MonoBehaviour
{
    [SerializeField] RawImage imageIcon;
    public string url;

    void Start()
    {
        StartCoroutine(Get(url));
    }

    public IEnumerator Get(string url)
    {
        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                if (www.isDone)
                {
                    
                    var result = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);

                    Root root = JsonUtility.FromJson<Root>(result);

                

                    StartCoroutine(GetImage(root.data.image.secure_url));

                }
                else
                {
                    Debug.Log("Error! no se obtuvo la DATA.");
                }
            }

            www.Dispose();


        }

    }


    public IEnumerator GetImage(string url)
    {

        using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(url))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {

                imageIcon.texture = ((DownloadHandlerTexture)www.downloadHandler).texture;

            }


            www.Dispose();

        }
    }

}





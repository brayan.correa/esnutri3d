using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro; // Libreria para el manejo de textos en la interfaz de usuario

public class EnemyViewController : MonoBehaviour
{

    public TextMeshProUGUI nombreText;
    public TextMeshProUGUI indicadorText;
    public TextMeshProUGUI medidaPrevencionText;
    public TextMeshProUGUI recomendacionText;
    

    public void DisplayEnemyData(string nombre, string indicador, string medidaPrevencion, string recomendacion)
    {
        nombreText.text = nombre;
        indicadorText.text = indicador;
        medidaPrevencionText.text = medidaPrevencion;
        recomendacionText.text = recomendacion;

    }
}



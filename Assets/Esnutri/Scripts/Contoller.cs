using System.Collections; 
using System.Collections.Generic; 
using UnityEngine.EventSystems; 
using UnityEngine; 
using UnityEngine.Networking; 
using UnityEngine.UI; 
using DG.Tweening; 
using TMPro; 
using System; 
 
using SimpleJSON;  
using Newtonsoft.Json;  
 
public class Contoller : MonoBehaviour 
{ 
     
    [SerializeField] 
    private RectTransform panelInput; 
    [SerializeField] 
    private InputField inputID; 
    [SerializeField]  
    public RawImage imageIcon2; 
    private string url_api ="https://esnutriapi.herokuapp.com/api/productos/"; 
    private string buscarNombre = "buscarNombre/";
    private string id_actual = "PLATANITOS NATUCHIPS 185G LIMON"; 
    private string mostrartodo = "all"; 
    public EnemyViewController enemyViewController; 
    [SerializeField]
    public Dropdown dropdown; 
    [SerializeField] 
    public InputField selectedName; 
 
 
 
    void Start() 
    { 
        StartCoroutine(Get(id_actual));   
        StartCoroutine(GetTodos(mostrartodo));      
    } 
 
 
    void DropdownItemSelected(Dropdown dropdown) 
    { 
        int index = dropdown.value; 
        selectedName.text = dropdown.options[index].text;

    }
 

    public IEnumerator Get(string id) 
    { 
        using (UnityWebRequest www = UnityWebRequest.Get(url_api + buscarNombre + id)) 
        { 
            yield return www.SendWebRequest(); // metodo que devuelve la respuesta que da el servidor 
 
            if (www.isNetworkError) // si ocurre errores de comunicación se muestra un mensaje en consola 
            { 
                Debug.Log(www.error); 
            } 
            else 
            { 
                if (www.isDone) 
                { 
                    var result = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data); 
                    //Debug.Log(result); 
 
                    var root = JsonUtility.FromJson<Root>(result); 
 
                    enemyViewController.DisplayEnemyData(root.data.nombre, root.data.indicador, root.data.medidaPrevencion, root.data.recomendacion); 
 
 
                    StartCoroutine(GetImage(root.data.image.secure_url)); 
 
       
                } 
                else 
                { 
                    Debug.Log("Error! no se obtuvo la DATA."); 
                } 
            } 
            //www.Dispose(); 
        } 
 
    } 
 
    public IEnumerator GetTodos(string mostrar) 
    { 
        using (UnityWebRequest www = UnityWebRequest.Get(url_api + mostrar)) 
        { 
            yield return www.SendWebRequest(); // metodo que devuelve la respuesta que da el servidor 
 
            if (www.isNetworkError) // si ocurre errores de comunicación se muestra un mensaje en consola 
            { 
                Debug.Log(www.error); 
            } 
            else 
            { 
                if (www.isDone) 
                { 
                    
                    JSONNode result = JSON.Parse(www.downloadHandler.text);    
                      
                    List<string> items = new List<string>(); 
 
                    for (int i = 0; i <= 100; i++) 
                    { 
                        items.Add(result["data"][i]["nombre"]); 
                    } 
                                           
                    foreach(var item in items) 
                    { 
                        dropdown.options.Add(new Dropdown.OptionData(){text = item}); 
                    }

                    
 
                    DropdownItemSelected(dropdown);

                    dropdown.onValueChanged.AddListener(delegate {DropdownItemSelected(dropdown); }); 
    
                } 
                else 
                { 
                    Debug.Log("Error! no se obtuvo la DATA."); 
                } 
            } 
 
            //www.Dispose(); 
 
        } 
 
    } 
 
    public void MostrarPanelInput() 
    { 
        panelInput.DOAnchorPos(new Vector2(-195.0f, -156.6615f),0.05f);  
    } 
 
    public void BuscarID() 
    {
        if (selectedName.text==inputID.text) 
        {
            StartCoroutine(Get(inputID.text));
            
        }else
        {
            Debug.Log("Error! producto no existe");
        }
    } 
 
     
    public IEnumerator GetImage(string url_api) 
    { 
 
        using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(url_api)) 
        { 
            yield return www.SendWebRequest(); 
 
            if (www.isNetworkError) 
            { 
                Debug.Log(www.error); 
            } 
            else 
            { 
                imageIcon2.texture = ((DownloadHandlerTexture)www.downloadHandler).texture; 
            } 
 
            //www.Dispose(); 
 
        } 
    } 
  
 
}
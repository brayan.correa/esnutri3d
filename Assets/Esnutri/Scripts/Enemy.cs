using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
[System.Serializable]
public class Imagen
{
    public string public_id;
    public string secure_url;
}

[System.Serializable]
public class Data
{
    public Imagen image;
    public string nombre;
    public string indicador;
    public string medidaPrevencion;
    public string recomendacion;
}


[System.Serializable]
public class Root
{
    public string mensaje;
    public string tipo;
    public Data data;
    public string mensaje_alterno;
    public string url;
}

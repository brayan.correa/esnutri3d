using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class CloudManager : MonoBehaviour
{
    [SerializeField] private CloudRecoBehaviour cloudRecoBehaviour;
    [SerializeField] private GameObject[] targets;
    private bool isScanning;

    private void Awake()
    {
        cloudRecoBehaviour.RegisterOnInitializedEventHandler(OnIntialized);
        cloudRecoBehaviour.RegisterOnStateChangedEventHandler(OnStateChanged);
        cloudRecoBehaviour.RegisterOnNewSearchResultEventHandler(OnNewSearchResult);

    }

    private void Disable()
    {
        cloudRecoBehaviour.UnregisterOnInitializedEventHandler(OnIntialized);
        cloudRecoBehaviour.UnregisterOnStateChangedEventHandler(OnStateChanged);
        cloudRecoBehaviour.UnregisterOnNewSearchResultEventHandler(OnNewSearchResult);

    }

    private void OnIntialized(CloudRecoBehaviour cloudRecoBehaviour)
    {
        Debug.Log("Cloud inicializada");

    }

    private void OnStateChanged(bool scanning)
    {
        isScanning = scanning;
        if (isScanning)
        {
            Debug.Log("Escaneado target");
        }
    }

    private void OnNewSearchResult(CloudRecoBehaviour.CloudRecoSearchResult result)
    {
        foreach (var target in targets)
        {
            if (result.TargetName == target.gameObject.name)
            {
                //cloudRecoBehaviour.EnableTracking(result, target.gameObject);
                cloudRecoBehaviour.EnableObservers(result, target.gameObject);
            }

        }
    }
}

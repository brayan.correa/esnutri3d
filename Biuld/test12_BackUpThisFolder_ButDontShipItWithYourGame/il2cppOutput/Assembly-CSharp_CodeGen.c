﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void PanelManager::OnEnable()
extern void PanelManager_OnEnable_m5A7DC3931168B59A585EAEFA1AB80F7D3EE8140F (void);
// 0x00000002 System.Void PanelManager::OpenPanel(UnityEngine.Animator)
extern void PanelManager_OpenPanel_m456B82F7B62A00937F60CE6014C1F3698414B7B4 (void);
// 0x00000003 UnityEngine.GameObject PanelManager::FindFirstEnabledSelectable(UnityEngine.GameObject)
extern void PanelManager_FindFirstEnabledSelectable_m0C277F0C1F1112D83086EB3AE8B85A5E06AEBAB7 (void);
// 0x00000004 System.Void PanelManager::CloseCurrent()
extern void PanelManager_CloseCurrent_m824C6139CDBD415DE8E4CF1FB557FCE79D69B2C7 (void);
// 0x00000005 System.Collections.IEnumerator PanelManager::DisablePanelDeleyed(UnityEngine.Animator)
extern void PanelManager_DisablePanelDeleyed_m1DF8E917EEC8FD14E76954ED3F2918C76B65E5AF (void);
// 0x00000006 System.Void PanelManager::SetSelected(UnityEngine.GameObject)
extern void PanelManager_SetSelected_m59DAF072CE220595B0AD4BC0BEA1C00856943B71 (void);
// 0x00000007 System.Void PanelManager::cambiarEscena(System.String)
extern void PanelManager_cambiarEscena_mF1C9633FDF51D589F19105CE55AAD7511E7D5EB2 (void);
// 0x00000008 System.Void PanelManager::Quit()
extern void PanelManager_Quit_mD1B36EF6861BAF23C1FA645A6F2DBF788708C9B6 (void);
// 0x00000009 System.Void PanelManager::.ctor()
extern void PanelManager__ctor_mA86BE5A7EC580FAD466DFEC12569D38F584473BB (void);
// 0x0000000A System.Void PanelManager/<DisablePanelDeleyed>d__10::.ctor(System.Int32)
extern void U3CDisablePanelDeleyedU3Ed__10__ctor_m4282B9D12EB2BB6D413E69F116601539D4A98B99 (void);
// 0x0000000B System.Void PanelManager/<DisablePanelDeleyed>d__10::System.IDisposable.Dispose()
extern void U3CDisablePanelDeleyedU3Ed__10_System_IDisposable_Dispose_mA29BD3E3DE14A63123458C1B873D3B9520CCD0E7 (void);
// 0x0000000C System.Boolean PanelManager/<DisablePanelDeleyed>d__10::MoveNext()
extern void U3CDisablePanelDeleyedU3Ed__10_MoveNext_mDCC2DE4CAA28ED5E8135119B9A4E38D075FEC235 (void);
// 0x0000000D System.Object PanelManager/<DisablePanelDeleyed>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisablePanelDeleyedU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA0AE880F4D89D9DC62386E0448E6FE576A4AE66 (void);
// 0x0000000E System.Void PanelManager/<DisablePanelDeleyed>d__10::System.Collections.IEnumerator.Reset()
extern void U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_Reset_m17342096F195588A8C598895C595B0B029F598C9 (void);
// 0x0000000F System.Object PanelManager/<DisablePanelDeleyed>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_get_Current_m47973E705B6316F9392B95B9A84ED6431CF8A0F0 (void);
// 0x00000010 System.Void CloudManager::Awake()
extern void CloudManager_Awake_mC281B36156D900D74224243FEE514F425453FA8F (void);
// 0x00000011 System.Void CloudManager::Disable()
extern void CloudManager_Disable_m4346BFFA3CF6C43E5EC91F725CFB7DBB65CA1B41 (void);
// 0x00000012 System.Void CloudManager::OnIntialized(Vuforia.CloudRecoBehaviour)
extern void CloudManager_OnIntialized_m5EB013F3EC8466B0704E2728D7AAAE8A26D72771 (void);
// 0x00000013 System.Void CloudManager::OnStateChanged(System.Boolean)
extern void CloudManager_OnStateChanged_mADC49A5CE10B6F5C3720568C7E00685F39EC3FE4 (void);
// 0x00000014 System.Void CloudManager::OnNewSearchResult(Vuforia.CloudRecoBehaviour/CloudRecoSearchResult)
extern void CloudManager_OnNewSearchResult_m374FEE74A66B7C34FF61C91BEE164933AA761B68 (void);
// 0x00000015 System.Void CloudManager::.ctor()
extern void CloudManager__ctor_mC283BEFBBE11285AF9FF014862945F5C4F854522 (void);
// 0x00000016 System.Void Contoller::Start()
extern void Contoller_Start_m5AB996E9B9013C449C87EC2C85F6C5598C884609 (void);
// 0x00000017 System.Collections.IEnumerator Contoller::Get(System.String)
extern void Contoller_Get_m0DAF5050136FA5C1FC2EDC3BDE08EB74C12C2E15 (void);
// 0x00000018 System.Void Contoller::.ctor()
extern void Contoller__ctor_mBCDDD49F99F6643440F8B1CBEAE2D2B0F94C00D0 (void);
// 0x00000019 System.Void Contoller/<Get>d__3::.ctor(System.Int32)
extern void U3CGetU3Ed__3__ctor_mA159298432F3E24048EDB77AEDBB9E4D1F6EC0F0 (void);
// 0x0000001A System.Void Contoller/<Get>d__3::System.IDisposable.Dispose()
extern void U3CGetU3Ed__3_System_IDisposable_Dispose_m74C148758410BBC62D5CFFACA9B0F6350534B57B (void);
// 0x0000001B System.Boolean Contoller/<Get>d__3::MoveNext()
extern void U3CGetU3Ed__3_MoveNext_m86C2A4DE58A32ECCD939F62EB5859B2CC563E497 (void);
// 0x0000001C System.Void Contoller/<Get>d__3::<>m__Finally1()
extern void U3CGetU3Ed__3_U3CU3Em__Finally1_m1377F257BFEDDD1598352FD9195295745F765CCF (void);
// 0x0000001D System.Object Contoller/<Get>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0ECE5FB46A7214EB170815BECDC8F70C1D7E71B (void);
// 0x0000001E System.Void Contoller/<Get>d__3::System.Collections.IEnumerator.Reset()
extern void U3CGetU3Ed__3_System_Collections_IEnumerator_Reset_m89030D1AA002A4EAD07A81FB98B960BA35C5B0F9 (void);
// 0x0000001F System.Object Contoller/<Get>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CGetU3Ed__3_System_Collections_IEnumerator_get_Current_mFB56B088ACC934B4B786EDD606420E0F55A78E0B (void);
// 0x00000020 System.Void Imagen::.ctor()
extern void Imagen__ctor_m1E4F65ECE22DD51FC4F858BD84846A905B323F7E (void);
// 0x00000021 System.Void Data::.ctor()
extern void Data__ctor_mAD0738081D5922E1E6C75A516F6564FBB8468214 (void);
// 0x00000022 System.Void Root::.ctor()
extern void Root__ctor_mC5B98C2C36B9EA08AB28CAEDCE2E1BF81AFB8B0A (void);
// 0x00000023 System.Void EnemyViewController::DisplayEnemyData(System.String,System.String,System.String,System.String)
extern void EnemyViewController_DisplayEnemyData_m73408A8C09F0A67C8774933C2C1321A90EDA5914 (void);
// 0x00000024 System.Void EnemyViewController::.ctor()
extern void EnemyViewController__ctor_m8B405F9846E5572838B7CEF310FF2F5EE16C660C (void);
// 0x00000025 System.Void Imagenes::Start()
extern void Imagenes_Start_m8D420C7EB9CF2B51B32D2B661FF225F430888916 (void);
// 0x00000026 System.Collections.IEnumerator Imagenes::Get(System.String)
extern void Imagenes_Get_m71D5919B4D892D16500885AC3EF1FD1C5AD9A3DE (void);
// 0x00000027 System.Collections.IEnumerator Imagenes::GetImage(System.String)
extern void Imagenes_GetImage_m40970D88BB34A8029ACBFB952D007431A6367A1A (void);
// 0x00000028 System.Void Imagenes::.ctor()
extern void Imagenes__ctor_m409A3B5D97F11113F2257943E3A63FAC356B9265 (void);
// 0x00000029 System.Void Imagenes/<Get>d__3::.ctor(System.Int32)
extern void U3CGetU3Ed__3__ctor_mFD87C05F3E31FF55056C3ECA3B2C4F2A91FD54A5 (void);
// 0x0000002A System.Void Imagenes/<Get>d__3::System.IDisposable.Dispose()
extern void U3CGetU3Ed__3_System_IDisposable_Dispose_m576F7E3961492100BADBFA8934F6A9DFE1A33BF9 (void);
// 0x0000002B System.Boolean Imagenes/<Get>d__3::MoveNext()
extern void U3CGetU3Ed__3_MoveNext_m4995759CCADE54ED2CE1C6F972FE657535639ACE (void);
// 0x0000002C System.Void Imagenes/<Get>d__3::<>m__Finally1()
extern void U3CGetU3Ed__3_U3CU3Em__Finally1_m49F0031B1412A7285F5C8BD28318651B6DEF39A9 (void);
// 0x0000002D System.Object Imagenes/<Get>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39BAD77A234981A273187217200971C1ECDEB705 (void);
// 0x0000002E System.Void Imagenes/<Get>d__3::System.Collections.IEnumerator.Reset()
extern void U3CGetU3Ed__3_System_Collections_IEnumerator_Reset_mB9AA0093AD2911BD64B077435DF1BC3C6813E3D7 (void);
// 0x0000002F System.Object Imagenes/<Get>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CGetU3Ed__3_System_Collections_IEnumerator_get_Current_m25824D3EE53C6676C4061158728129214FC0E962 (void);
// 0x00000030 System.Void Imagenes/<GetImage>d__4::.ctor(System.Int32)
extern void U3CGetImageU3Ed__4__ctor_m1E528EDD44E60B531AF5B17A8EA768FA6EE16943 (void);
// 0x00000031 System.Void Imagenes/<GetImage>d__4::System.IDisposable.Dispose()
extern void U3CGetImageU3Ed__4_System_IDisposable_Dispose_mCE49B34AE9701268C498020428670B4662CA93A1 (void);
// 0x00000032 System.Boolean Imagenes/<GetImage>d__4::MoveNext()
extern void U3CGetImageU3Ed__4_MoveNext_m5C7EC5AAD0835753028F19500918F88B0B51A6AD (void);
// 0x00000033 System.Void Imagenes/<GetImage>d__4::<>m__Finally1()
extern void U3CGetImageU3Ed__4_U3CU3Em__Finally1_mDF25EED0CBEDEAAEE90F2F5762C0D12E2086B9F7 (void);
// 0x00000034 System.Object Imagenes/<GetImage>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetImageU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF7FDBB8FE1250B2EAA7DF161C154F52E73AF4EDD (void);
// 0x00000035 System.Void Imagenes/<GetImage>d__4::System.Collections.IEnumerator.Reset()
extern void U3CGetImageU3Ed__4_System_Collections_IEnumerator_Reset_m505EB0E89DC8051E7ECB1B4B187E36A3C1B30B9B (void);
// 0x00000036 System.Object Imagenes/<GetImage>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CGetImageU3Ed__4_System_Collections_IEnumerator_get_Current_mA2C7F47B1A72A6E086D6D5BDF18AE886C951C550 (void);
// 0x00000037 System.Void OpenURL::video()
extern void OpenURL_video_m7479216005BAD28AA7D1008C169FFFAB83D62D49 (void);
// 0x00000038 System.Void OpenURL::unl()
extern void OpenURL_unl_m62AB9FD319D7A50FAA37E254AF2B8D62CAA8A028 (void);
// 0x00000039 System.Void OpenURL::cis()
extern void OpenURL_cis_mD204576686D6F0A5865075F039DFFA687D27CFEC (void);
// 0x0000003A System.Void OpenURL::medicina()
extern void OpenURL_medicina_m40C4E22F5E71EA03F920E92A2881E9B47F26D3A5 (void);
// 0x0000003B System.Void OpenURL::.ctor()
extern void OpenURL__ctor_m9DCD529CEA922A3F80E72D9C86916C3BD48D41CB (void);
// 0x0000003C System.Void usuarioAPI::Start()
extern void usuarioAPI_Start_mB8B20531333BB6CAF9847CA3B83A630E3B401AF0 (void);
// 0x0000003D System.Collections.IEnumerator usuarioAPI::Get(System.String)
extern void usuarioAPI_Get_m9EC3376098D94E2F3E3CE05132CBF198F6215ED5 (void);
// 0x0000003E System.Void usuarioAPI::.ctor()
extern void usuarioAPI__ctor_m8A157C87F0850D986CD0B72A341B524858C7BCCF (void);
// 0x0000003F System.Void usuarioAPI/<Get>d__2::.ctor(System.Int32)
extern void U3CGetU3Ed__2__ctor_mD4AB00CC5B78B052F14DEC8DAB200C2F3EFBF114 (void);
// 0x00000040 System.Void usuarioAPI/<Get>d__2::System.IDisposable.Dispose()
extern void U3CGetU3Ed__2_System_IDisposable_Dispose_m2DB2C208FC244A0278BC7CCF314583D09D851760 (void);
// 0x00000041 System.Boolean usuarioAPI/<Get>d__2::MoveNext()
extern void U3CGetU3Ed__2_MoveNext_m29C1678896E956BD8B33BA563636D1394A6E5AAA (void);
// 0x00000042 System.Void usuarioAPI/<Get>d__2::<>m__Finally1()
extern void U3CGetU3Ed__2_U3CU3Em__Finally1_mDCA482D4936441601EF934917F2A6B8B4042F14A (void);
// 0x00000043 System.Object usuarioAPI/<Get>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m77A30FE653A3743517FA523ADDF7D3EF1369A599 (void);
// 0x00000044 System.Void usuarioAPI/<Get>d__2::System.Collections.IEnumerator.Reset()
extern void U3CGetU3Ed__2_System_Collections_IEnumerator_Reset_mE35D23A1D5013B2E2BA5A83B009D9F1CAAF067E9 (void);
// 0x00000045 System.Object usuarioAPI/<Get>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CGetU3Ed__2_System_Collections_IEnumerator_get_Current_mD6E242EBC2C9EBD763F1DE3093E805545128A43C (void);
// 0x00000046 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7 (void);
// 0x00000047 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46 (void);
// 0x00000048 System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m9AB8FA8A32EA23F2E55795D8301ED0BF6A59F722 (void);
// 0x00000049 System.Void ChatController::.ctor()
extern void ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719 (void);
// 0x0000004A System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_mF83641F913F3455A3AE6ADCEA5DEB2A323FCB58F (void);
// 0x0000004B System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m0F0C6DD803E99B2C15F3369ABD94EC273FADC75B (void);
// 0x0000004C System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435 (void);
// 0x0000004D System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9 (void);
// 0x0000004E System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138 (void);
// 0x0000004F System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m432062D94FDEF42B01FAB69EBC06A4D137C525C2 (void);
// 0x00000050 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m8088B5A404D1CB754E73D37137F9A288E47E7E9C (void);
// 0x00000051 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mF689BF83350416D2071533C92042BF12AC52F0C0 (void);
// 0x00000052 System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3CCB9B113B234F43186B26439E10AD6609DD565 (void);
// 0x00000053 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m3EF23BF40634D4262D8A2AE3DB14140FEFB4BF52 (void);
// 0x00000054 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C119A46A09AD8F0D4DE964F6B335BE2A460FAA (void);
// 0x00000055 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9 (void);
// 0x00000056 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0 (void);
// 0x00000057 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3 (void);
// 0x00000058 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D (void);
// 0x00000059 TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3 (void);
// 0x0000005A System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C (void);
// 0x0000005B TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82 (void);
// 0x0000005C System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5 (void);
// 0x0000005D TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C (void);
// 0x0000005E System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3 (void);
// 0x0000005F TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908 (void);
// 0x00000060 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427 (void);
// 0x00000061 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D (void);
// 0x00000062 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF (void);
// 0x00000063 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052 (void);
// 0x00000064 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1 (void);
// 0x00000065 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A (void);
// 0x00000066 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6 (void);
// 0x00000067 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m5A891393BC3211CFEF2390B5E9899129CBDAC189 (void);
// 0x00000068 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m8242C5F9626A3C1330927FEACF3ECAD287500475 (void);
// 0x00000069 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_mCB9E9ACB06AC524273C163743C9191CAF9C1FD33 (void);
// 0x0000006A System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mF0691C407CA44C2E8F2D7CD6C9C2099693CBE7A6 (void);
// 0x0000006B System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m2809D6FFF57FAE45DC5BB4DD579328535E255A02 (void);
// 0x0000006C System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB (void);
// 0x0000006D System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E (void);
// 0x0000006E System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963 (void);
// 0x0000006F System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2 (void);
// 0x00000070 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2 (void);
// 0x00000071 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC (void);
// 0x00000072 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9 (void);
// 0x00000073 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4 (void);
// 0x00000074 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m242187966C9D563957FB0F76C467B25C25D91D69 (void);
// 0x00000075 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m7AD303D116E090426086312CD69BFA256CD28B0D (void);
// 0x00000076 System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m5F93878ED8166F8F4507EE8353856FAEABBBF1C9 (void);
// 0x00000077 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F5CE0A24226CB5F890D4C2A9FAD81A2696CE6F6 (void);
// 0x00000078 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m553F892690ED74A33F57B1359743D31F8BB93C2A (void);
// 0x00000079 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m50D65AEFE4D08E48AC72E017E00CD43273E1BDBD (void);
// 0x0000007A System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA (void);
// 0x0000007B System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB (void);
// 0x0000007C System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m515F107569D5BDE7C81F5DFDAB4A298A5399EB5A (void);
// 0x0000007D System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mFFD5DC6FCF8EC489FF249BE7F91D4336F2AD76AC (void);
// 0x0000007E System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mDCA96D0D1226C44C15F1FD85518F0711E6B395D9 (void);
// 0x0000007F System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m109B5747CD8D1CF40DAC526C54BFB07223E1FB46 (void);
// 0x00000080 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC9F90586F057E3728D9F93BB0E12197C9B994EEA (void);
// 0x00000081 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA4DCEFD742C012A03C20EF42A873B5BFF07AF87A (void);
// 0x00000082 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29 (void);
// 0x00000083 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1 (void);
// 0x00000084 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mDEE8E96AE811C5A84CB2C04440CD4662E2F918D3 (void);
// 0x00000085 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D (void);
// 0x00000086 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6 (void);
// 0x00000087 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90 (void);
// 0x00000088 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27 (void);
// 0x00000089 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788 (void);
// 0x0000008A System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5 (void);
// 0x0000008B System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F (void);
// 0x0000008C System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m31AE86C54785402EB078A40F37D83FEA9216388F (void);
// 0x0000008D System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878 (void);
// 0x0000008E System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72 (void);
// 0x0000008F System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695 (void);
// 0x00000090 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A (void);
// 0x00000091 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371 (void);
// 0x00000092 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88 (void);
// 0x00000093 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_m9F466F9C9554AA7488F4607E7FAC9A5C61F46D56 (void);
// 0x00000094 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B (void);
// 0x00000095 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_m2B0F8A634812D7FE998DD35188C5F07797E4FB0D (void);
// 0x00000096 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mCF53541AABFDC14249868837689AC287470F4E71 (void);
// 0x00000097 System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_mB9586A9B61959C3BC38EFB8FC83109785F93F6AC (void);
// 0x00000098 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A34F7423FA726A91524CBA0CDD2A25E4AF8EE95 (void);
// 0x00000099 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1C76BF8EAC2CDC2BAC58755622763B9318DA51CA (void);
// 0x0000009A System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m289720A67EB6696F350EAC41DAAE3B917031B7EA (void);
// 0x0000009B System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8 (void);
// 0x0000009C System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE (void);
// 0x0000009D System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807 (void);
// 0x0000009E System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93 (void);
// 0x0000009F System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690 (void);
// 0x000000A0 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_mD2C2C4CA7AFBAAC9F4B04CB2896DB9B32B015ACB (void);
// 0x000000A1 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m462DE1568957770D72704E93D2461D8371C0D362 (void);
// 0x000000A2 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22 (void);
// 0x000000A3 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_m39944C7E44F317ACDEC971C8FF2DEC8EA1CCC1C2 (void);
// 0x000000A4 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m54C900BFB8433103FA97A4E50B2C941D431B5A51 (void);
// 0x000000A5 System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_m50CEEC92FE0C83768B366E9F9B5B1C9DEF85928E (void);
// 0x000000A6 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CB1783D2DD0399E051969089A36819EDC66FCB (void);
// 0x000000A7 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mB6C5974E8F57160AE544E1D2FD44621EEF3ACAB5 (void);
// 0x000000A8 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m5BDAFBB20F42A6E9EC65B6A2365F5AD98F42A1C5 (void);
// 0x000000A9 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D (void);
// 0x000000AA System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6 (void);
// 0x000000AB System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0 (void);
// 0x000000AC System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m7CB9C7DF4657B7B70F6ED6EEB00C0F422D8B0CAA (void);
// 0x000000AD System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mA57DA4D469190B581B5DCB406E9FB70DD33511F2 (void);
// 0x000000AE System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mE1C3343B7258BAADC74C1A060E71C28951D39D45 (void);
// 0x000000AF System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1819CF068B92E7EA9EEFD7F93CA316F38DF644BA (void);
// 0x000000B0 System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9B7AEE80C1E70D2D2FF5811A54AFD6189CD7F5A9 (void);
// 0x000000B1 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5C22C5D235424F0613697F05E72ADB4D1A3420C8 (void);
// 0x000000B2 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C (void);
// 0x000000B3 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39 (void);
// 0x000000B4 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95 (void);
// 0x000000B5 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689 (void);
// 0x000000B6 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m050ECF4852B6A82000133662D6502577DFD57C3A (void);
// 0x000000B7 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mAA4D3653F05692839313CE180250A44378024E52 (void);
// 0x000000B8 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_m0E52802FD4239665709F086E6E0B235CDE67E9B1 (void);
// 0x000000B9 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025 (void);
// 0x000000BA System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_m40A144070AB46560F2B3919EA5CB8BD51F8DDF45 (void);
// 0x000000BB System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m7942532282ACF3B429FAD926284352907FFE087B (void);
// 0x000000BC System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_m2D07AF9391894BCE39624FA2DCFA87AC6F8119AE (void);
// 0x000000BD System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m754C680B2751A9F05DBF253431A3CB42885F7854 (void);
// 0x000000BE System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mD12057609EFCBCA8E7B61B0421D4A7C5A206C8C3 (void);
// 0x000000BF System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9FD7DAB922AE6A58166112C295ABFF6E19E1D186 (void);
// 0x000000C0 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_mDF8D4C69F022D088AFC0E109FC0DBE0C9B938CAC (void);
// 0x000000C1 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m2F2F21F38D2DD8AE3D066E64850D404497A131C5 (void);
// 0x000000C2 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_mC5102728A86DCB2171E54CFEDFA7BE6F29AB355C (void);
// 0x000000C3 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D9A6269831C00345D245D0EED2E5FC20BBF4683 (void);
// 0x000000C4 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mE5E0678716735BDF0D632FE43E392981E75A1C4D (void);
// 0x000000C5 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m3E9D4960A972BD7601F6454E6F9A614AA21D553E (void);
// 0x000000C6 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2 (void);
// 0x000000C7 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49 (void);
// 0x000000C8 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148 (void);
// 0x000000C9 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE (void);
// 0x000000CA System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E (void);
// 0x000000CB System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m352D68463FC9600F9139AD78F0176368562F63C6 (void);
// 0x000000CC System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_mD3C24C6814482113FD231827E550FBBCC91424A0 (void);
// 0x000000CD System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m83285E807FA4462B99B68D1EB12B2360238C53EB (void);
// 0x000000CE System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m588E025C05E03684A11ABC91B50734A349D28CC8 (void);
// 0x000000CF System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2412DC176F8CA3096658EB0E27AC28218DAEC03A (void);
// 0x000000D0 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mCCE19093B7355F3E23834E27A8517661DF833797 (void);
// 0x000000D1 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mE53E0B4DBE6AF5DAC110C3F626B34C5965845E54 (void);
// 0x000000D2 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m1ECB51A93EE3B236301948784A3260FD72814923 (void);
// 0x000000D3 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m461761745A9C5FF4F7995C3DB33DB43848AEB05B (void);
// 0x000000D4 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m1FC162511DF31A9CDBD0101083FBCB11380554C4 (void);
// 0x000000D5 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A5E330ACDAD25422A7D642301F58E6C1EE1B041 (void);
// 0x000000D6 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_m5A7148435B35A0A84329416FF765D45F6AA0F4E1 (void);
// 0x000000D7 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m066140B8D4CD5DE3527A3A05183AE89B487B5D55 (void);
// 0x000000D8 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_m9A84A570D2582918A6B1287139527E9AB2CF088D (void);
// 0x000000D9 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66 (void);
// 0x000000DA System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233 (void);
// 0x000000DB System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084 (void);
// 0x000000DC System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m3CC1B812C740BAE87C6B5CA94DC64E6131F42A7C (void);
// 0x000000DD System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839 (void);
// 0x000000DE System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063 (void);
// 0x000000DF System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1 (void);
// 0x000000E0 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A (void);
// 0x000000E1 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C (void);
// 0x000000E2 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A (void);
// 0x000000E3 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079 (void);
// 0x000000E4 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_m1CC40A8236B2161050D19C4B2EBFF34B96645723 (void);
// 0x000000E5 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43 (void);
// 0x000000E6 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E (void);
// 0x000000E7 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6 (void);
// 0x000000E8 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mB421E2CFB617397137CF1AE9CC2F49E46EB3F0AE (void);
// 0x000000E9 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mD88D899DE3321CC15502BB1174709BE290AB6215 (void);
// 0x000000EA System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m180B102DAED1F3313F2F4BB6CF588FF96C8CAB79 (void);
// 0x000000EB System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mE0538FFAFE04A286F937907D0E4664338DCF1559 (void);
// 0x000000EC System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m72BF9241651D44805590F1DBADF2FD864D209779 (void);
// 0x000000ED System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF (void);
// 0x000000EE System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E (void);
// 0x000000EF System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797 (void);
// 0x000000F0 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B (void);
// 0x000000F1 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A (void);
// 0x000000F2 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503 (void);
// 0x000000F3 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE (void);
// 0x000000F4 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87 (void);
// 0x000000F5 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B (void);
// 0x000000F6 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D (void);
// 0x000000F7 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m5B53EF1608E98B6A56AAA386085A3216B35A51EE (void);
// 0x000000F8 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF (void);
// 0x000000F9 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98 (void);
// 0x000000FA System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65 (void);
// 0x000000FB System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97 (void);
// 0x000000FC System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772 (void);
// 0x000000FD System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m1FD258EC7A53C8E1ECB18EB6FFEFC6239780C398 (void);
// 0x000000FE System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301 (void);
// 0x000000FF System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491 (void);
// 0x00000100 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F (void);
// 0x00000101 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3 (void);
// 0x00000102 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_mAF25D6E90A6CB17EE041885B32579A2AEDBFCC36 (void);
// 0x00000103 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848 (void);
// 0x00000104 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1 (void);
// 0x00000105 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B (void);
// 0x00000106 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m16733B3DFF4C0F625AA66B5DF9D3B04D723E49CC (void);
// 0x00000107 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1 (void);
// 0x00000108 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0245999D5FAAF8855583609DB16CAF48E9450262 (void);
// 0x00000109 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_mF965F484C619EFA1359F7DB6495C1C79A89001BF (void);
// 0x0000010A System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m5C44B8CC0AB09A205BB1649931D2AC7C6F016E60 (void);
// 0x0000010B System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9600944C968C16121129C479F8B25D8E8B7FDD1 (void);
// 0x0000010C System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m319AC50F2DE1572FB7D7AF4F5F65958D01477899 (void);
// 0x0000010D System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_mC19EC9CE0C245B49D987C18357571FF3462F1D2C (void);
// 0x0000010E System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C (void);
// 0x0000010F System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354 (void);
// 0x00000110 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2 (void);
// 0x00000111 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76 (void);
// 0x00000112 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_m0CF9C49A1033B4475C04A417440F39490FED64A8 (void);
// 0x00000113 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_m2A69F06CF58FA46B689BD4166DEF5AD15FA2FA88 (void);
// 0x00000114 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D (void);
// 0x00000115 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m10C4D98A634474BAA883419ED308835B7D91C01A (void);
// 0x00000116 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mB3756FBFDD731F3CC1EFF9AB132FF5075C8411F8 (void);
// 0x00000117 System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mD694A3145B54B9C5EB351853752B9292DBFF0273 (void);
// 0x00000118 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C3A529011A51B9A994106D3C1271548B02D405 (void);
// 0x00000119 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m15291DCCCEC264095634B26DD6F24D52360BDAF0 (void);
// 0x0000011A System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m0B8F21A4589C68BA16A8340938BB44C980260CC9 (void);
// 0x0000011B System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867 (void);
// 0x0000011C System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304 (void);
// 0x0000011D System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23 (void);
// 0x0000011E System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E (void);
// 0x0000011F System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_mE7A41CEFDB0008A1CD15F156EFEE1C895A92EE77 (void);
// 0x00000120 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_m5FD933D6BF976B64FC0B80614DE5112377D1DC38 (void);
// 0x00000121 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB (void);
// 0x00000122 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m440985E6DF2F1B461E2964101EA242FFD472A25A (void);
// 0x00000123 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m74112773E1FD645722BC221FA5256331C068EAE7 (void);
// 0x00000124 System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mA6858F6CA14AAE3DFB7EA13748E10E063BBAB934 (void);
// 0x00000125 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DD4F3768C9025EFAC0BFDBB942FEF7953FB20BE (void);
// 0x00000126 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m2F84864A089CBA0B878B7AC1EA39A49B82682A90 (void);
// 0x00000127 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m3106DAC17EF56701CBC9812DD031932B04BB730B (void);
// 0x00000128 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E (void);
// 0x00000129 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21 (void);
// 0x0000012A System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5 (void);
// 0x0000012B System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1 (void);
// 0x0000012C System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_mF8641640C828A9664AE03AF01CB4832E14EF436D (void);
// 0x0000012D System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_m06D25FE7F9F3EFF693DDC889BF725F01D0CF2A6F (void);
// 0x0000012E System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198 (void);
// 0x0000012F System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_mBE5C0E4A0F65F07A7510D171683AD319F76E6C6D (void);
// 0x00000130 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m4DD41FA568ABBC327FA38C0E345EFB6F1A71C2C8 (void);
// 0x00000131 System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_mDD84A4116FCAAF920F86BA72F890CE0BE76AF348 (void);
// 0x00000132 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m250CC96EC17E74D79536FDA4EB6F5B5F985C0845 (void);
// 0x00000133 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5A5869FEFA67D5E9659F1145B83581D954550C1A (void);
// 0x00000134 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m496F1BFEADA21FFB684F8C1996EAB707CFA1C5F0 (void);
// 0x00000135 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0 (void);
// 0x00000136 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47 (void);
// 0x00000137 System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47 (void);
// 0x00000138 System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB (void);
// 0x00000139 System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mFF049D0455A7DD19D6BDACBEEB737B4AAE32DDA7 (void);
// 0x0000013A System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_m632BD9DC8FB193AF2D5B540524B11AF139FDF5F0 (void);
// 0x0000013B System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422 (void);
// 0x0000013C System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m8C69A89B34AA3D16243E69F1E0015856C791CC8A (void);
// 0x0000013D System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m8E51A05E012CCFA439DCF10A8B5C4FA196E4344A (void);
// 0x0000013E System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m7A5B8E07B89E628DB7119F7F61311165A2DBC4D6 (void);
// 0x0000013F System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m974E92A444C6343E94C76BB6CC6508F7AE4FD36E (void);
// 0x00000140 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m6DBC52A95A92A54A1801DC4CEE548FA568251D5E (void);
// 0x00000141 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m110CD16E89E725B1484D24FFB1753768F78A988B (void);
// 0x00000142 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDE5E71C88F5096FD70EB061287ADF0B847732821 (void);
// 0x00000143 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m14B89756695EE73AEBB6F3A613F65E1343A8CC2C (void);
// 0x00000144 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F (void);
// 0x00000145 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809 (void);
// 0x00000146 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m65A93388CC2CF58CD2E08CC8EF682A2C97C558FF (void);
// 0x00000147 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mBE4B6E5B6D8AAE9340CD59B1FA9DFE9A34665E98 (void);
// 0x00000148 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795 (void);
// 0x00000149 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m1943C34BBEAF121203BA8C5D725E991283A4A3BB (void);
// 0x0000014A System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m145D2DA1026419984AD79D5D62FBC38C9441AB53 (void);
// 0x0000014B System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_mCE7A826C5E4854C2C509C77BD18F5A9B6D691B02 (void);
// 0x0000014C System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD80368E9B7E259311C03E406B75161ED6F7618E3 (void);
// 0x0000014D System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m07746C332D2D8CE5DEA59873C26F2FAD4B369B42 (void);
// 0x0000014E System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m71D7F84D9DEF63BEC6B44866515DDCF35B142A19 (void);
// 0x0000014F SimpleJSON.JSONNodeType SimpleJSON.JSONNode::get_Tag()
// 0x00000150 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_m77F15891BEC7ED659BFBC392555178B558747AD8 (void);
// 0x00000151 System.Void SimpleJSON.JSONNode::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_mC6F47073D979B943286B2EAB1A6D0380AFE58A09 (void);
// 0x00000152 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_m466B08DF2E30B20606697EC7AE043C2791DC6768 (void);
// 0x00000153 System.Void SimpleJSON.JSONNode::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m045530804B67FC5E2E57E497219F27ED70CE437E (void);
// 0x00000154 System.String SimpleJSON.JSONNode::get_Value()
extern void JSONNode_get_Value_m2A9961ACC3D4BCBB028012CD79B619DCBD82A839 (void);
// 0x00000155 System.Void SimpleJSON.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_mE8CD0E68E0E2B0A716F56B0FE9B988EC2BAD773A (void);
// 0x00000156 System.Int32 SimpleJSON.JSONNode::get_Count()
extern void JSONNode_get_Count_m260DDA50B8AFB98F5946E54B9EADD05891A82C8B (void);
// 0x00000157 System.Boolean SimpleJSON.JSONNode::get_IsNumber()
extern void JSONNode_get_IsNumber_m6B495FE576572E9FC7999740C63980BCB65AD768 (void);
// 0x00000158 System.Boolean SimpleJSON.JSONNode::get_IsString()
extern void JSONNode_get_IsString_mBDE2CAF25E51CDA450074BE9DC81D834903BA392 (void);
// 0x00000159 System.Boolean SimpleJSON.JSONNode::get_IsBoolean()
extern void JSONNode_get_IsBoolean_m13F16853C0F6D76D0AB6B7E866923A0632C108A2 (void);
// 0x0000015A System.Boolean SimpleJSON.JSONNode::get_IsNull()
extern void JSONNode_get_IsNull_m6443A7B3540D725ED3ACA0038A74CE0346A31F8D (void);
// 0x0000015B System.Boolean SimpleJSON.JSONNode::get_IsArray()
extern void JSONNode_get_IsArray_m52DCDB47E4CB2673FDCECCD3BE9DD2D90B5C948F (void);
// 0x0000015C System.Boolean SimpleJSON.JSONNode::get_IsObject()
extern void JSONNode_get_IsObject_m237FE2EA3382DD9762ED426B49F46183F5EF39AB (void);
// 0x0000015D System.Boolean SimpleJSON.JSONNode::get_Inline()
extern void JSONNode_get_Inline_m7A5B6C07F44EFEEDD80FD72580C32C0579041F4C (void);
// 0x0000015E System.Void SimpleJSON.JSONNode::set_Inline(System.Boolean)
extern void JSONNode_set_Inline_m18362F10F03DDCD1FF29B4868C3EA793D39AE7F6 (void);
// 0x0000015F System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode)
extern void JSONNode_Add_mB05F1A32B54A9A1223F9AC6A6A737836FA1F4E7E (void);
// 0x00000160 System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode)
extern void JSONNode_Add_mDAF96580EAF3B9FF23888A8549BED7A98439075D (void);
// 0x00000161 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.String)
extern void JSONNode_Remove_mF56C4223700DF4F1D5AE12BCD69C492C2487FA59 (void);
// 0x00000162 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m7B5E0BC0A29C35857D7B10857A8C52C0E3DFB615 (void);
// 0x00000163 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(SimpleJSON.JSONNode)
extern void JSONNode_Remove_mE2CFD05512C25BD11EA4160CAAF88B8154D9DBE5 (void);
// 0x00000164 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_Children()
extern void JSONNode_get_Children_m3E2D70DBCA2C8311F65A47B766668728392B1F89 (void);
// 0x00000165 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_DeepChildren()
extern void JSONNode_get_DeepChildren_m891CB892AEA834980686ED760B952A86DC1E8725 (void);
// 0x00000166 System.String SimpleJSON.JSONNode::ToString()
extern void JSONNode_ToString_m4CC464630B0AEEDD82AEB6B069690949AF569345 (void);
// 0x00000167 System.String SimpleJSON.JSONNode::ToString(System.Int32)
extern void JSONNode_ToString_m1F607CB90F49115510B7CF5228733578E9AD41F2 (void);
// 0x00000168 System.Void SimpleJSON.JSONNode::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
// 0x00000169 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNode::GetEnumerator()
// 0x0000016A System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode::get_Linq()
extern void JSONNode_get_Linq_m8569DB478533504290D9A09ECA0DF12F116122DA (void);
// 0x0000016B SimpleJSON.JSONNode/KeyEnumerator SimpleJSON.JSONNode::get_Keys()
extern void JSONNode_get_Keys_mC3401CC91BBD9D1166EF8EFC0C87A820FC543D1B (void);
// 0x0000016C SimpleJSON.JSONNode/ValueEnumerator SimpleJSON.JSONNode::get_Values()
extern void JSONNode_get_Values_mF8FB164A48A169146D00EBA3F51D4C8E380C1930 (void);
// 0x0000016D System.Double SimpleJSON.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_m9A8E3EC46E4545BCBFA26B99C0F013067D2F0AE4 (void);
// 0x0000016E System.Void SimpleJSON.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_mCDBB05BD0AE82EEF0C4842F5A9205B8F4C858015 (void);
// 0x0000016F System.Int32 SimpleJSON.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_mE4A3FCC1D91362D077C2ACF418ACAB43771B1FE6 (void);
// 0x00000170 System.Void SimpleJSON.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m12FCF0B7E45E17EA0456AE44EFEF0C8731603F50 (void);
// 0x00000171 System.Single SimpleJSON.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_m0D044C1F3FC35086783A4BAF506EA96DC997D050 (void);
// 0x00000172 System.Void SimpleJSON.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_m55FCE24DF60B37724DACCCF0A759522B2561DE92 (void);
// 0x00000173 System.Boolean SimpleJSON.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_m902380F5939671ACBBB7EFA01A48F1A082B1FD9C (void);
// 0x00000174 System.Void SimpleJSON.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_m6097FD196A8C7BB156125363D1C1D3EF0EB67CD3 (void);
// 0x00000175 System.Int64 SimpleJSON.JSONNode::get_AsLong()
extern void JSONNode_get_AsLong_m31250905C6F4BED9B1059009E064D07D609C4C18 (void);
// 0x00000176 System.Void SimpleJSON.JSONNode::set_AsLong(System.Int64)
extern void JSONNode_set_AsLong_m8D29780DEA1458A2F9C33805432DB1554950ECF4 (void);
// 0x00000177 SimpleJSON.JSONArray SimpleJSON.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_m2D0890FDDA140528CAB44B1B6B3E34B26383ACC7 (void);
// 0x00000178 SimpleJSON.JSONObject SimpleJSON.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_m72F6D406BECA2FB0A24B20E0A353FDB8E409CA1B (void);
// 0x00000179 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_mA884A397C3152BDB411767FDE9EDC274A8904523 (void);
// 0x0000017A System.String SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m31D5D161C5F21186F90F99E09EC78CED5188C349 (void);
// 0x0000017B SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Double)
extern void JSONNode_op_Implicit_m35AB6AB8F232F07E3226DEB06AB832F5C2BD7A8A (void);
// 0x0000017C System.Double SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m0039518B4032423C4CDC8C8B07790AE1779E8E25 (void);
// 0x0000017D SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Single)
extern void JSONNode_op_Implicit_m6ECC1712EB162E1AC774A36E31AF31A1FCC5E659 (void);
// 0x0000017E System.Single SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m8312FC9381D54EBF755C7B41D525C6EEAD0EC135 (void);
// 0x0000017F SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Int32)
extern void JSONNode_op_Implicit_mD0756B05030DAE5F5D4A3E57708D5EEEF6A362BB (void);
// 0x00000180 System.Int32 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mE793F9163050BD78846F2C2A288A968E8F9C11AD (void);
// 0x00000181 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Int64)
extern void JSONNode_op_Implicit_mE052DB6EC3A2453BBDE1511230CEF2B3AC21211E (void);
// 0x00000182 System.Int64 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mEA60633B7DA23EFD6583A825ED6A52BBFB327EEE (void);
// 0x00000183 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Boolean)
extern void JSONNode_op_Implicit_m2BD30AE4D30506EF634EA264A90E018FF66FF1E4 (void);
// 0x00000184 System.Boolean SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m057A1C72683211B58678B1448C260F747434DF40 (void);
// 0x00000185 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void JSONNode_op_Implicit_m5D106035AB428BDA4B6671950E8E5747771CBD7F (void);
// 0x00000186 System.Boolean SimpleJSON.JSONNode::op_Equality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Equality_m6961ED452D3A120FE9908FFB96260DF98A47A8B3 (void);
// 0x00000187 System.Boolean SimpleJSON.JSONNode::op_Inequality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m2DF69DE99CD87AA07CE1200892E24EA22B351943 (void);
// 0x00000188 System.Boolean SimpleJSON.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_mE1B8A846783529B1E54786975A6A2396089A88DE (void);
// 0x00000189 System.Int32 SimpleJSON.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_m0A263555D1F0E6766A61692A7E1BC3546B2BC984 (void);
// 0x0000018A System.Text.StringBuilder SimpleJSON.JSONNode::get_EscapeBuilder()
extern void JSONNode_get_EscapeBuilder_m351C10540498042FC3ED424E3D9C08DCB16071FF (void);
// 0x0000018B System.String SimpleJSON.JSONNode::Escape(System.String)
extern void JSONNode_Escape_mC20A370D25C7B269E4707FF5CEC7062C470C416A (void);
// 0x0000018C System.Void SimpleJSON.JSONNode::ParseElement(SimpleJSON.JSONNode,System.String,System.String,System.Boolean)
extern void JSONNode_ParseElement_m978282020EF903D5D8F2F2B7B05074A5D1C3F2EF (void);
// 0x0000018D SimpleJSON.JSONNode SimpleJSON.JSONNode::Parse(System.String)
extern void JSONNode_Parse_m51FFFB4953A8D875B9D2DD5E032D131A149956E0 (void);
// 0x0000018E System.Void SimpleJSON.JSONNode::.ctor()
extern void JSONNode__ctor_mF8F2893483161D3B7B9877B63C69063D26A5C353 (void);
// 0x0000018F System.Boolean SimpleJSON.JSONNode/Enumerator::get_IsValid()
extern void Enumerator_get_IsValid_mBC273331DC1699FF46BD3621AE5059A54AD98BA6 (void);
// 0x00000190 System.Void SimpleJSON.JSONNode/Enumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void Enumerator__ctor_mF21239C69620D815F8CD34F022BE18E9DAF9CB10 (void);
// 0x00000191 System.Void SimpleJSON.JSONNode/Enumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void Enumerator__ctor_mAC4ED0FA4B083E2652E865A41EA5C74A49478EFE (void);
// 0x00000192 System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode/Enumerator::get_Current()
extern void Enumerator_get_Current_mDE6750203413E1069D0520793D6AA0B2527CB20E (void);
// 0x00000193 System.Boolean SimpleJSON.JSONNode/Enumerator::MoveNext()
extern void Enumerator_MoveNext_m238CF072385A1106BEDEFCE33BA2B0DBE999758A (void);
// 0x00000194 System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_mCC61CE3EDCF1AC94A84E031F2E89F8054C94A015 (void);
// 0x00000195 System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m122732DF448B45E8E82956E07AC8314C60E28C29 (void);
// 0x00000196 System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(SimpleJSON.JSONNode/Enumerator)
extern void ValueEnumerator__ctor_m7BA4BAD5FEBAC4054F71575B728DC27EC4080F0A (void);
// 0x00000197 SimpleJSON.JSONNode SimpleJSON.JSONNode/ValueEnumerator::get_Current()
extern void ValueEnumerator_get_Current_mAA24A52FDEB7160BD268193175388EACB41B7CE2 (void);
// 0x00000198 System.Boolean SimpleJSON.JSONNode/ValueEnumerator::MoveNext()
extern void ValueEnumerator_MoveNext_m5B596A2EF2FF395EDA8F5CAB97C0789498D250C9 (void);
// 0x00000199 SimpleJSON.JSONNode/ValueEnumerator SimpleJSON.JSONNode/ValueEnumerator::GetEnumerator()
extern void ValueEnumerator_GetEnumerator_m765261287A2C0AEF757B94994826F43951387E4C (void);
// 0x0000019A System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_m6EA81E2BED4CA5194A7306D8B324F7356E37F80A (void);
// 0x0000019B System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_mA6338E82A9F8AA19A1744352B4FE54103AD70405 (void);
// 0x0000019C System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(SimpleJSON.JSONNode/Enumerator)
extern void KeyEnumerator__ctor_m526EA1364C367B83C931F4208CDD816BD02810EA (void);
// 0x0000019D SimpleJSON.JSONNode SimpleJSON.JSONNode/KeyEnumerator::get_Current()
extern void KeyEnumerator_get_Current_m8FBFEE52D4438AAF3E10AB4370B34FBB8E66B3C2 (void);
// 0x0000019E System.Boolean SimpleJSON.JSONNode/KeyEnumerator::MoveNext()
extern void KeyEnumerator_MoveNext_m42FE2CEE808A7E065895BA333B7FBD2F3AEE032F (void);
// 0x0000019F SimpleJSON.JSONNode/KeyEnumerator SimpleJSON.JSONNode/KeyEnumerator::GetEnumerator()
extern void KeyEnumerator_GetEnumerator_mD4687B4D6D10E4D6870CBBECC680689A62A95C0B (void);
// 0x000001A0 System.Void SimpleJSON.JSONNode/LinqEnumerator::.ctor(SimpleJSON.JSONNode)
extern void LinqEnumerator__ctor_m9FD8AB1580F3D94C5C36D070DBE85E023ED36E30 (void);
// 0x000001A1 System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode/LinqEnumerator::get_Current()
extern void LinqEnumerator_get_Current_m28F0BE4D9B5736F5BD79197C1895EAC1592EBAAF (void);
// 0x000001A2 System.Object SimpleJSON.JSONNode/LinqEnumerator::System.Collections.IEnumerator.get_Current()
extern void LinqEnumerator_System_Collections_IEnumerator_get_Current_m6B6C12C7E8CD21DF513FCDCB4E88E454790B6FF0 (void);
// 0x000001A3 System.Boolean SimpleJSON.JSONNode/LinqEnumerator::MoveNext()
extern void LinqEnumerator_MoveNext_mCA8604B6E8D857CF16003E674048C05E29447819 (void);
// 0x000001A4 System.Void SimpleJSON.JSONNode/LinqEnumerator::Dispose()
extern void LinqEnumerator_Dispose_m5D6A54C4B712D138739726323D5BEA50A4E12E32 (void);
// 0x000001A5 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode/LinqEnumerator::GetEnumerator()
extern void LinqEnumerator_GetEnumerator_m4A9F0720F0C0964F91032AB8B8776F09DC70A90B (void);
// 0x000001A6 System.Void SimpleJSON.JSONNode/LinqEnumerator::Reset()
extern void LinqEnumerator_Reset_m56B65E398518EF57070307FDC48069DFE37BC57B (void);
// 0x000001A7 System.Collections.IEnumerator SimpleJSON.JSONNode/LinqEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_mB63F02D713868ABF87DAB18ABFD5D832F4D805A4 (void);
// 0x000001A8 System.Void SimpleJSON.JSONNode/<get_Children>d__40::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__40__ctor_mED3CB3E682749015A8491DF12C31487745A4248B (void);
// 0x000001A9 System.Void SimpleJSON.JSONNode/<get_Children>d__40::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__40_System_IDisposable_Dispose_m278BD93CDDCDA8A33C819FE8B1B7BE4FEE90D06B (void);
// 0x000001AA System.Boolean SimpleJSON.JSONNode/<get_Children>d__40::MoveNext()
extern void U3Cget_ChildrenU3Ed__40_MoveNext_mD6AE82A70820FAF1AFEC7896955955E990067CB8 (void);
// 0x000001AB SimpleJSON.JSONNode SimpleJSON.JSONNode/<get_Children>d__40::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m75DE45763814DF424576E7C517E1D7EFDBAD9F45 (void);
// 0x000001AC System.Void SimpleJSON.JSONNode/<get_Children>d__40::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_Reset_m5375DEDEB65015D0634741057877D504BF18526F (void);
// 0x000001AD System.Object SimpleJSON.JSONNode/<get_Children>d__40::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_get_Current_m05E8F0D8A29155DCA5EA0D8B6ABA68AF7236F9C6 (void);
// 0x000001AE System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<get_Children>d__40::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m2F120D821A756BC0938593FF8F6184A49710DFA3 (void);
// 0x000001AF System.Collections.IEnumerator SimpleJSON.JSONNode/<get_Children>d__40::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerable_GetEnumerator_mB1CE5E0B30277A97EA1C6D3E337523DEA5B11757 (void);
// 0x000001B0 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__42::.ctor(System.Int32)
extern void U3Cget_DeepChildrenU3Ed__42__ctor_mB98C64BA2B81334A1830986688EA632F32DC2EDB (void);
// 0x000001B1 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__42::System.IDisposable.Dispose()
extern void U3Cget_DeepChildrenU3Ed__42_System_IDisposable_Dispose_mA6F2AF3124A2E4A2DCB86C54AF4E31F08EA8AF3F (void);
// 0x000001B2 System.Boolean SimpleJSON.JSONNode/<get_DeepChildren>d__42::MoveNext()
extern void U3Cget_DeepChildrenU3Ed__42_MoveNext_m6568ACC95807B31FA819569F541B6EDAAD92F579 (void);
// 0x000001B3 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__42::<>m__Finally1()
extern void U3Cget_DeepChildrenU3Ed__42_U3CU3Em__Finally1_mFB16A1F4090D60071A16E8ED7E86DC487305AF73 (void);
// 0x000001B4 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__42::<>m__Finally2()
extern void U3Cget_DeepChildrenU3Ed__42_U3CU3Em__Finally2_m9B19E684C42ABCBC8476BDD436F5EB28D9C731AC (void);
// 0x000001B5 SimpleJSON.JSONNode SimpleJSON.JSONNode/<get_DeepChildren>d__42::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m927BE2DB8EF13D247334ACBD9AD5F9D7312E13DC (void);
// 0x000001B6 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__42::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_Reset_m3B21F82F9CF8A35C11BD0D4B635E29F13507C5C3 (void);
// 0x000001B7 System.Object SimpleJSON.JSONNode/<get_DeepChildren>d__42::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_get_Current_mF0F87CE545AB2B74799D13F35D5007A8C95F0CB3 (void);
// 0x000001B8 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<get_DeepChildren>d__42::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF23F7F01784F070DC30E80C7B2525BB08F1F729F (void);
// 0x000001B9 System.Collections.IEnumerator SimpleJSON.JSONNode/<get_DeepChildren>d__42::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerable_GetEnumerator_m60B68168D69C58711A46585D0A262C6CFA0C8B5F (void);
// 0x000001BA System.Boolean SimpleJSON.JSONArray::get_Inline()
extern void JSONArray_get_Inline_mBA0C9AEBB7420DBDFD977C0F54CC237E8F2BE3E5 (void);
// 0x000001BB System.Void SimpleJSON.JSONArray::set_Inline(System.Boolean)
extern void JSONArray_set_Inline_m731089F5D0FA649ED210518DC299635A8D86A1DC (void);
// 0x000001BC SimpleJSON.JSONNodeType SimpleJSON.JSONArray::get_Tag()
extern void JSONArray_get_Tag_m360EB078D7897D6D52783B8CDA6B736D014E97BC (void);
// 0x000001BD System.Boolean SimpleJSON.JSONArray::get_IsArray()
extern void JSONArray_get_IsArray_mA7B4EF5B0128FB64ACEB7EAC66FA3522991980AF (void);
// 0x000001BE SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_m6AF64AE0DD2A5AAB8C0E271BF0CAB8AA1FD32E17 (void);
// 0x000001BF SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_m8BE9047FC512840E6A4594560EDF86BB4E0FF657 (void);
// 0x000001C0 System.Void SimpleJSON.JSONArray::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_mBCD05590C34BC589B786E753B9FE796EBA3F6725 (void);
// 0x000001C1 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_mE18312128B02B505BA656D7F444B05A6769710AE (void);
// 0x000001C2 System.Void SimpleJSON.JSONArray::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_mE4E0DE5133E60AF49E46FEDAD00D2A04349C0855 (void);
// 0x000001C3 System.Int32 SimpleJSON.JSONArray::get_Count()
extern void JSONArray_get_Count_mB71218A2D8288D0665C467844F7351D301FDAFDD (void);
// 0x000001C4 System.Void SimpleJSON.JSONArray::Add(System.String,SimpleJSON.JSONNode)
extern void JSONArray_Add_mD1FBE0F0FC20E7415014B7FF21939592EBB0C9A1 (void);
// 0x000001C5 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_m79500DBD9751A04C02756470A4D22DDCF9C97FEC (void);
// 0x000001C6 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(SimpleJSON.JSONNode)
extern void JSONArray_Remove_m64C3EBFE3DB5BE130232769DC43000E84589E674 (void);
// 0x000001C7 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::get_Children()
extern void JSONArray_get_Children_m733AE4C5816E51E6F86441110606489A0406AA91 (void);
// 0x000001C8 System.Void SimpleJSON.JSONArray::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONArray_WriteToStringBuilder_m9F23115433028794DCAC019F82EEFD946990D994 (void);
// 0x000001C9 System.Void SimpleJSON.JSONArray::.ctor()
extern void JSONArray__ctor_m92FFF2DC8E1425398814F50D4B253EB459B8477F (void);
// 0x000001CA System.Void SimpleJSON.JSONArray/<get_Children>d__22::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__22__ctor_m85868694A6F76D6658184E05C60FF81F01F77A15 (void);
// 0x000001CB System.Void SimpleJSON.JSONArray/<get_Children>d__22::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_m2F364B8DD4640833161EE743D458FAF77BCAED9F (void);
// 0x000001CC System.Boolean SimpleJSON.JSONArray/<get_Children>d__22::MoveNext()
extern void U3Cget_ChildrenU3Ed__22_MoveNext_mD1A93CD03B10AD9224ACD34AA4478BA4EB448AE5 (void);
// 0x000001CD System.Void SimpleJSON.JSONArray/<get_Children>d__22::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_mE8C769C27FDCFEA9BDE25D788AE87EC9565F4C5F (void);
// 0x000001CE SimpleJSON.JSONNode SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m9FB6C47800DA9AC5BD99118430A954370D30FA70 (void);
// 0x000001CF System.Void SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_m5FDF47F215C3ECA0B88DBDBA67D9E23939FB6E75 (void);
// 0x000001D0 System.Object SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_mB7369D4C4D04908F6F69E55E0FDF6835418E9671 (void);
// 0x000001D1 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mE9E5D91FED8686D7D44613FE3F2E5A67765D924E (void);
// 0x000001D2 System.Collections.IEnumerator SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m64A7C394D254404FA1EE6C54C42CB33064B85573 (void);
// 0x000001D3 System.Boolean SimpleJSON.JSONObject::get_Inline()
extern void JSONObject_get_Inline_mCDF2154366BEFF9E547918F999E7F3C7C4865F84 (void);
// 0x000001D4 System.Void SimpleJSON.JSONObject::set_Inline(System.Boolean)
extern void JSONObject_set_Inline_m7F048A7565E5A53FDB610D44B7CA75A314CB7A7A (void);
// 0x000001D5 SimpleJSON.JSONNodeType SimpleJSON.JSONObject::get_Tag()
extern void JSONObject_get_Tag_mD57D6BCAD1C677B88693FD508129CFAD661F4FBD (void);
// 0x000001D6 System.Boolean SimpleJSON.JSONObject::get_IsObject()
extern void JSONObject_get_IsObject_m9F72861BE5A0DB2888AA3CBEC82718E08DD71E93 (void);
// 0x000001D7 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONObject::GetEnumerator()
extern void JSONObject_GetEnumerator_m8912E3D1EA302655BB5701B53EB19437238BABDA (void);
// 0x000001D8 SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.String)
extern void JSONObject_get_Item_m219B9BA37D800A5DFEAA14E4EECA375B3565BF96 (void);
// 0x000001D9 System.Void SimpleJSON.JSONObject::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_m1AC7334DBA67D0CB6C9549B83B3FFA75CF226AEF (void);
// 0x000001DA SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.Int32)
extern void JSONObject_get_Item_m5C2EDBE7B154A3FC1CC43616C4C40255B4D95652 (void);
// 0x000001DB System.Void SimpleJSON.JSONObject::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_mFB6E61E3FA394B7D2CA01CC957A6A253642D109B (void);
// 0x000001DC System.Int32 SimpleJSON.JSONObject::get_Count()
extern void JSONObject_get_Count_m9109E9A81559A9006EE160CA6A0F3291C71F2D08 (void);
// 0x000001DD System.Void SimpleJSON.JSONObject::Add(System.String,SimpleJSON.JSONNode)
extern void JSONObject_Add_m25BD208A0AC0F0223FD93FBCB42785B12A6E1A18 (void);
// 0x000001DE SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.String)
extern void JSONObject_Remove_m34280FDB4512E61F42781475E492BE98514830C9 (void);
// 0x000001DF SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.Int32)
extern void JSONObject_Remove_mD1B01E22A9C1FEE83A00ECDFD8E0D8A422F8E4C2 (void);
// 0x000001E0 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(SimpleJSON.JSONNode)
extern void JSONObject_Remove_m51B998A7997D184A1A20359D512C6B5A1B825404 (void);
// 0x000001E1 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject::get_Children()
extern void JSONObject_get_Children_m03D7227DE57F0BE2977FC0436C0DE48858650B7C (void);
// 0x000001E2 System.Void SimpleJSON.JSONObject::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONObject_WriteToStringBuilder_m931DC8805C6B8F09617958EFDAEA957751EB2EAE (void);
// 0x000001E3 System.Void SimpleJSON.JSONObject::.ctor()
extern void JSONObject__ctor_m8007967452F5257DC9F5DF2B78B411BFD4B6D6AB (void);
// 0x000001E4 System.Void SimpleJSON.JSONObject/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m6976B4CF7F93E28364B390F81E55DAD60BB141C1 (void);
// 0x000001E5 System.Boolean SimpleJSON.JSONObject/<>c__DisplayClass21_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m8B35D441B276B749481FF797FC51A256A7A56105 (void);
// 0x000001E6 System.Void SimpleJSON.JSONObject/<get_Children>d__23::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__23__ctor_mD7DD2FB8F14148B45EC4AC3A595DC1AFE369FC99 (void);
// 0x000001E7 System.Void SimpleJSON.JSONObject/<get_Children>d__23::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m85C7654E92BD35A4525D4F3E70FFD57B5CD497AF (void);
// 0x000001E8 System.Boolean SimpleJSON.JSONObject/<get_Children>d__23::MoveNext()
extern void U3Cget_ChildrenU3Ed__23_MoveNext_mB9E90BB01F42BA8913E7CD7AEA18FBEADFC46CC0 (void);
// 0x000001E9 System.Void SimpleJSON.JSONObject/<get_Children>d__23::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m0FEB12CB9C8CC84E25A44F9FC928D8EB8F4DF9DD (void);
// 0x000001EA SimpleJSON.JSONNode SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mFB2B5F5F93F044ADBF11AF1C59D305BFE295D063 (void);
// 0x000001EB System.Void SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_mE65CCEC3E7FCE86596AF14C5821DA3D5F76C34E3 (void);
// 0x000001EC System.Object SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m3B0D1EE4EF3C849E2A702C9B9DB6F65FA70890D9 (void);
// 0x000001ED System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m42B5EADBEE1E083675385176B87BCAB7C4FA0873 (void);
// 0x000001EE System.Collections.IEnumerator SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_mC4F14551458D7F9F3DDC7610F078982D1C04DBD4 (void);
// 0x000001EF SimpleJSON.JSONNodeType SimpleJSON.JSONString::get_Tag()
extern void JSONString_get_Tag_m68B0FF9ADDC3E203E5D60BB10639AEABACA34D44 (void);
// 0x000001F0 System.Boolean SimpleJSON.JSONString::get_IsString()
extern void JSONString_get_IsString_m933985E37AE8A887A2039A9BAC7698F083BCD6E3 (void);
// 0x000001F1 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONString::GetEnumerator()
extern void JSONString_GetEnumerator_m1CB9E437FC8622F3FE05D0AC12024D144747E0B8 (void);
// 0x000001F2 System.String SimpleJSON.JSONString::get_Value()
extern void JSONString_get_Value_mEAD2BD372A2C517E83233BA5F6E309745AA5E9B4 (void);
// 0x000001F3 System.Void SimpleJSON.JSONString::set_Value(System.String)
extern void JSONString_set_Value_mB974D9B82AB8F9FAB84DCA99B8BD4B7C1C08ED00 (void);
// 0x000001F4 System.Void SimpleJSON.JSONString::.ctor(System.String)
extern void JSONString__ctor_m1DD5FB9A4147F72A0ED5F773FF82FA269241AD19 (void);
// 0x000001F5 System.Void SimpleJSON.JSONString::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONString_WriteToStringBuilder_mDF24D860FBF8E71F6F04799DD70F7700CE41D818 (void);
// 0x000001F6 System.Boolean SimpleJSON.JSONString::Equals(System.Object)
extern void JSONString_Equals_m1C60B537E558E6DF85ACF3EF9FF43BF9A3CF5435 (void);
// 0x000001F7 System.Int32 SimpleJSON.JSONString::GetHashCode()
extern void JSONString_GetHashCode_m979A74F84B4C0F45BF63D75DE1146490F743EE00 (void);
// 0x000001F8 SimpleJSON.JSONNodeType SimpleJSON.JSONNumber::get_Tag()
extern void JSONNumber_get_Tag_m7C6E217E85B6161812496B63E5D371B910AAC856 (void);
// 0x000001F9 System.Boolean SimpleJSON.JSONNumber::get_IsNumber()
extern void JSONNumber_get_IsNumber_mFABFD0C9C4905CFB34A62700A1BD335F53E4214E (void);
// 0x000001FA SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNumber::GetEnumerator()
extern void JSONNumber_GetEnumerator_m4D13E84756AEED9FCD7EFEEE4D01187DD049C596 (void);
// 0x000001FB System.String SimpleJSON.JSONNumber::get_Value()
extern void JSONNumber_get_Value_mBC5AB046D134B1E54C228C9C1C2231F8448CD56D (void);
// 0x000001FC System.Void SimpleJSON.JSONNumber::set_Value(System.String)
extern void JSONNumber_set_Value_m2264762BBD76F39DDC5DF3160910A44FBEFDE54C (void);
// 0x000001FD System.Double SimpleJSON.JSONNumber::get_AsDouble()
extern void JSONNumber_get_AsDouble_m8C004121700A7E7EB2B77ED223187227E33DE60B (void);
// 0x000001FE System.Void SimpleJSON.JSONNumber::set_AsDouble(System.Double)
extern void JSONNumber_set_AsDouble_m8E17AF8C0E9AE0EF6E25D86CB1B119904ADC0558 (void);
// 0x000001FF System.Int64 SimpleJSON.JSONNumber::get_AsLong()
extern void JSONNumber_get_AsLong_mF96069F806F51121CBFE8847D9E0D312F05986BB (void);
// 0x00000200 System.Void SimpleJSON.JSONNumber::set_AsLong(System.Int64)
extern void JSONNumber_set_AsLong_m541EF4E20CD8683CA860E0B969CECF7B71E2A357 (void);
// 0x00000201 System.Void SimpleJSON.JSONNumber::.ctor(System.Double)
extern void JSONNumber__ctor_m1CE3527102D15EBC3A183E3519895E291CAC1D90 (void);
// 0x00000202 System.Void SimpleJSON.JSONNumber::.ctor(System.String)
extern void JSONNumber__ctor_m39FDDE1A9EFEE9C4F2498E531D12B97AA49A1BA5 (void);
// 0x00000203 System.Void SimpleJSON.JSONNumber::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNumber_WriteToStringBuilder_mD311BC3C1EE3E159C43801EB214F084E567367F2 (void);
// 0x00000204 System.Boolean SimpleJSON.JSONNumber::IsNumeric(System.Object)
extern void JSONNumber_IsNumeric_mE6C07226FABFDD425449643925B667C05C52D41D (void);
// 0x00000205 System.Boolean SimpleJSON.JSONNumber::Equals(System.Object)
extern void JSONNumber_Equals_mC04BB811CCAF20E70AE696AE74ECFDF5DA888688 (void);
// 0x00000206 System.Int32 SimpleJSON.JSONNumber::GetHashCode()
extern void JSONNumber_GetHashCode_m976ADFE41037830524798C7E6AFE08006B5F77AD (void);
// 0x00000207 SimpleJSON.JSONNodeType SimpleJSON.JSONBool::get_Tag()
extern void JSONBool_get_Tag_m82CE84C4C89E157D4DB036B9F0745343C005C338 (void);
// 0x00000208 System.Boolean SimpleJSON.JSONBool::get_IsBoolean()
extern void JSONBool_get_IsBoolean_m2671AE98710859611DF47E6BC58E6582C3A5B445 (void);
// 0x00000209 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONBool::GetEnumerator()
extern void JSONBool_GetEnumerator_mA07A10A6111713F7AD09FF03D09A6028556094D9 (void);
// 0x0000020A System.String SimpleJSON.JSONBool::get_Value()
extern void JSONBool_get_Value_mBEA89869448B0B597758D5BF2A3B576CA0BB64E3 (void);
// 0x0000020B System.Void SimpleJSON.JSONBool::set_Value(System.String)
extern void JSONBool_set_Value_mC960EE4083CA91D0059BE24661AFC06E131E2CFC (void);
// 0x0000020C System.Boolean SimpleJSON.JSONBool::get_AsBool()
extern void JSONBool_get_AsBool_mE04224144EAD0A9AD2F3B14BC0C68557A3BF22AC (void);
// 0x0000020D System.Void SimpleJSON.JSONBool::set_AsBool(System.Boolean)
extern void JSONBool_set_AsBool_m88EDF61A5ABBFF3ECF723312852E14F3C60AE365 (void);
// 0x0000020E System.Void SimpleJSON.JSONBool::.ctor(System.Boolean)
extern void JSONBool__ctor_mBB02E388CFB96B99E84561FCFF68147F00391C58 (void);
// 0x0000020F System.Void SimpleJSON.JSONBool::.ctor(System.String)
extern void JSONBool__ctor_m8CFB6AA78095EA003AB9B5EDD8932E8E0B01A1B9 (void);
// 0x00000210 System.Void SimpleJSON.JSONBool::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONBool_WriteToStringBuilder_m82C70C80863730E8A22EE7A5B099C765F2E1D91E (void);
// 0x00000211 System.Boolean SimpleJSON.JSONBool::Equals(System.Object)
extern void JSONBool_Equals_m2671F40DA8F1128BA1451FE7066515C6E0C50D45 (void);
// 0x00000212 System.Int32 SimpleJSON.JSONBool::GetHashCode()
extern void JSONBool_GetHashCode_mC5B59375A9EE9978A5ADD1A24ECEE3FC920836DB (void);
// 0x00000213 SimpleJSON.JSONNull SimpleJSON.JSONNull::CreateOrGet()
extern void JSONNull_CreateOrGet_m68ED6000156701E566B1EA9DDC5284299B0C9105 (void);
// 0x00000214 System.Void SimpleJSON.JSONNull::.ctor()
extern void JSONNull__ctor_m909243259F39D10FA6FEB176474DEF9C9972D76B (void);
// 0x00000215 SimpleJSON.JSONNodeType SimpleJSON.JSONNull::get_Tag()
extern void JSONNull_get_Tag_m89A7F368EA6269874235F85E43AE82254AAFD41E (void);
// 0x00000216 System.Boolean SimpleJSON.JSONNull::get_IsNull()
extern void JSONNull_get_IsNull_m1174212D6379871AC361EF06FA05DD510FC55595 (void);
// 0x00000217 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNull::GetEnumerator()
extern void JSONNull_GetEnumerator_m16D254C74386D1A0AB2EFD1DE0EAF409C73B7686 (void);
// 0x00000218 System.String SimpleJSON.JSONNull::get_Value()
extern void JSONNull_get_Value_mB15431220D7D0B45CE002A204DF9E070CF78DBE0 (void);
// 0x00000219 System.Void SimpleJSON.JSONNull::set_Value(System.String)
extern void JSONNull_set_Value_mAF0CD2E912EF772E0892EB4ABB77294F689CF20A (void);
// 0x0000021A System.Boolean SimpleJSON.JSONNull::get_AsBool()
extern void JSONNull_get_AsBool_m6F3817CD49ED7CC10C180D31D84ED4B0151C78CE (void);
// 0x0000021B System.Void SimpleJSON.JSONNull::set_AsBool(System.Boolean)
extern void JSONNull_set_AsBool_m5717BC3921B7DE0683E9160B3816628B5CBC663D (void);
// 0x0000021C System.Boolean SimpleJSON.JSONNull::Equals(System.Object)
extern void JSONNull_Equals_m8A39CAD3A41E9584C434B90A1360C62B3E158DE6 (void);
// 0x0000021D System.Int32 SimpleJSON.JSONNull::GetHashCode()
extern void JSONNull_GetHashCode_m74BE6286F06C6E7D5E35381E8BD27215117D9061 (void);
// 0x0000021E System.Void SimpleJSON.JSONNull::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNull_WriteToStringBuilder_mB5B78BFA6A4943319926C1B2AE93F68C7B9B5FFD (void);
// 0x0000021F System.Void SimpleJSON.JSONNull::.cctor()
extern void JSONNull__cctor_m49F440C5442212437C3A1CDAF32B864961BE534B (void);
// 0x00000220 SimpleJSON.JSONNodeType SimpleJSON.JSONLazyCreator::get_Tag()
extern void JSONLazyCreator_get_Tag_m1CB86FEA25328F1BE9CC01F6D020C9450E9F466E (void);
// 0x00000221 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONLazyCreator::GetEnumerator()
extern void JSONLazyCreator_GetEnumerator_m720BF0642A079A8BD44F6D650CF4D833DEF67757 (void);
// 0x00000222 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode)
extern void JSONLazyCreator__ctor_m0B3625D19DDD8DBDBB45822FAABCE266FA4EE694 (void);
// 0x00000223 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode,System.String)
extern void JSONLazyCreator__ctor_m02E2D630C60045F25A3AC001B7A17DF2D5D197B4 (void);
// 0x00000224 System.Void SimpleJSON.JSONLazyCreator::Set(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Set_mEF6EB64379EBE960F050C24D45EDCA4B6D404958 (void);
// 0x00000225 SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_m562D16AE7F1F0CACA5ED050B390B63F98EBC77B1 (void);
// 0x00000226 System.Void SimpleJSON.JSONLazyCreator::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m42894F9D00193BC7138C5D451E1B0BBD1BFE1084 (void);
// 0x00000227 SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_mF7AE3ADFBE062BF3B83FECCE0EF10F10996DE0CD (void);
// 0x00000228 System.Void SimpleJSON.JSONLazyCreator::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m0107997E3B3CB75FACD86FB487C5D9416171CBEC (void);
// 0x00000229 System.Void SimpleJSON.JSONLazyCreator::Add(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_mA8451EE34FEA0205B6BD6527AB46E5926451F49F (void);
// 0x0000022A System.Void SimpleJSON.JSONLazyCreator::Add(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_mDC69A4E203B73054072D1575EC4CF20D95064F61 (void);
// 0x0000022B System.Boolean SimpleJSON.JSONLazyCreator::op_Equality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_m7C4199B28912BE4C1AE6009F94C6FE07776923C5 (void);
// 0x0000022C System.Boolean SimpleJSON.JSONLazyCreator::op_Inequality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_m8E4E9C09E420FE4E5A0AB54B63CFAEF2244B5F3B (void);
// 0x0000022D System.Boolean SimpleJSON.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_m753939907CFDB1548B0DAAB38E4737EF17B50066 (void);
// 0x0000022E System.Int32 SimpleJSON.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_m878E7AFF42AE5C43F4F643B6AEB25662491316F9 (void);
// 0x0000022F System.Int32 SimpleJSON.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_mE1404FBC99CE4E8EF4ABBE0BDF661206BAC2C44D (void);
// 0x00000230 System.Void SimpleJSON.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_m13146E53FD6A2F7573B752BFF079E0AF6A5FAE74 (void);
// 0x00000231 System.Single SimpleJSON.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_m2600D4B0E1179583EFE268070C66EAC11D380E04 (void);
// 0x00000232 System.Void SimpleJSON.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_m9DCF79C70D4ED3728C12B709A6D95A0F0A057DE0 (void);
// 0x00000233 System.Double SimpleJSON.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_m41D6DF89CD7CEC00F36962068EE072D391EC0B38 (void);
// 0x00000234 System.Void SimpleJSON.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_mB7ABE38136DBEDA7CC9AC12A381322D6C49ADED9 (void);
// 0x00000235 System.Int64 SimpleJSON.JSONLazyCreator::get_AsLong()
extern void JSONLazyCreator_get_AsLong_mFBA0000985629FA20509FA45A6A8B751C9CAC2B8 (void);
// 0x00000236 System.Void SimpleJSON.JSONLazyCreator::set_AsLong(System.Int64)
extern void JSONLazyCreator_set_AsLong_mBD4640D2F347DEF793A631A44026A03D3D5D73A4 (void);
// 0x00000237 System.Boolean SimpleJSON.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_m7D8AF5879C2C8036916AA6B15E22CB4B80412CF4 (void);
// 0x00000238 System.Void SimpleJSON.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_m4DB409DB959182CAA610147A51A2ECDBAFEA6092 (void);
// 0x00000239 SimpleJSON.JSONArray SimpleJSON.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_m493C069A3624597885A7B6E00C82E829A84B47C4 (void);
// 0x0000023A SimpleJSON.JSONObject SimpleJSON.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_mE01B43B261A6A56F4FCE40AB11F3AAF90B7C292D (void);
// 0x0000023B System.Void SimpleJSON.JSONLazyCreator::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONLazyCreator_WriteToStringBuilder_mC9975859B1C42C9F5E507E604121D10B2FB2D93D (void);
// 0x0000023C SimpleJSON.JSONNode SimpleJSON.JSON::Parse(System.String)
extern void JSON_Parse_m9E6F3A67011C765E4352E350D1F400C9A52DC5F6 (void);
// 0x0000023D DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOFade(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOFade_m33AC3DD19DD85217123B580C7A2BCF1D5A90E5D4 (void);
// 0x0000023E DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOPitch(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOPitch_m9F61FEB8419EC9F62B1A60CC36914980430E1456 (void);
// 0x0000023F DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOSetFloat(UnityEngine.Audio.AudioMixer,System.String,System.Single,System.Single)
extern void DOTweenModuleAudio_DOSetFloat_mBA2509B5C802E771D824FBF098E131475FE27937 (void);
// 0x00000240 System.Int32 DG.Tweening.DOTweenModuleAudio::DOComplete(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOComplete_mDFE49D8B71D95F8C3EE7568ADE0F0051DBA56323 (void);
// 0x00000241 System.Int32 DG.Tweening.DOTweenModuleAudio::DOKill(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOKill_mF453317F96B7CA4F9C5344E4DF6CF2FFBCFBE538 (void);
// 0x00000242 System.Int32 DG.Tweening.DOTweenModuleAudio::DOFlip(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOFlip_m7F1F6E6AEA8CFFBE6FABCCAAB2025B2887E17539 (void);
// 0x00000243 System.Int32 DG.Tweening.DOTweenModuleAudio::DOGoto(UnityEngine.Audio.AudioMixer,System.Single,System.Boolean)
extern void DOTweenModuleAudio_DOGoto_m24D7C34EFC1DFA1CE96792B8584CF060830CF066 (void);
// 0x00000244 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPause_m125649DA4069CA21A7D0219FC33B35B9A6B0A367 (void);
// 0x00000245 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlay(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlay_mEBD2ADDF0E4DBC462F53F1CA6F0DE8E928F3C94B (void);
// 0x00000246 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayBackwards(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayBackwards_m6EBE459898E93DFB0E7FA7E959E88B3016E92080 (void);
// 0x00000247 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayForward(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayForward_mAC90AEF47DBC4B1FC509BE40FEEB9CCC92BDB816 (void);
// 0x00000248 System.Int32 DG.Tweening.DOTweenModuleAudio::DORestart(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORestart_m044617D37A19DED114558F22E1AB8784194BE514 (void);
// 0x00000249 System.Int32 DG.Tweening.DOTweenModuleAudio::DORewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORewind_mA04F3C6E68A4E888BFFB6F7B27ABAFA201266D0B (void);
// 0x0000024A System.Int32 DG.Tweening.DOTweenModuleAudio::DOSmoothRewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOSmoothRewind_m9311EB1FF7A31892537EFC587430EE03566C0810 (void);
// 0x0000024B System.Int32 DG.Tweening.DOTweenModuleAudio::DOTogglePause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOTogglePause_m437CA4E9EB383779006AC19D86EBE51D58D67F2D (void);
// 0x0000024C System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m160A4BE74C4A0D2D75CE2C15B1F7E5E69163C973 (void);
// 0x0000024D System.Single DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m8C4722C8715E07655B52CF10C76179595C3DAF28 (void);
// 0x0000024E System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m79B9F6415F81447E5D2F5BE5C0F47E39A8C66038 (void);
// 0x0000024F System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m709A9EE2A6B97DD5552A583951859A1C83FA2278 (void);
// 0x00000250 System.Single DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0::<DOPitch>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_m137E279AEB9AE936DFD3C4158CED13A0C6079A37 (void);
// 0x00000251 System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0::<DOPitch>b__1(System.Single)
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_mF3FE1A2CC1F0A9CFD31297F1BE45C12E810DD00D (void);
// 0x00000252 System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mDBF428719391180E42C26EE06A2AC47547F13E99 (void);
// 0x00000253 System.Single DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0::<DOSetFloat>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__0_m367153ADF53E32C8FC51F280B628B2A2190E64E4 (void);
// 0x00000254 System.Void DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0::<DOSetFloat>b__1(System.Single)
extern void U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__1_mBCC8DF43E3C17A45DC0CDD8FBB051F9F938F49A5 (void);
// 0x00000255 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMove(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMove_m9218B91AF3CA4CB76EC980F3C19DC290C22DD0D3 (void);
// 0x00000256 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveX(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveX_mDA0E3727E8E0A9CC8CA18919C55DBB67CC7E7618 (void);
// 0x00000257 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveY(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveY_m4FA2CA0C0DE64203A16C528F12B7FA9EED027F87 (void);
// 0x00000258 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveZ(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveZ_m4A8887E9003AD195C398DB344BD2400AF5527873 (void);
// 0x00000259 DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DORotate(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern void DOTweenModulePhysics_DORotate_m34B2316F2E1B4FABB69B2F982D8D9DA5AC074DC6 (void);
// 0x0000025A DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DOLookAt(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.AxisConstraint,System.Nullable`1<UnityEngine.Vector3>)
extern void DOTweenModulePhysics_DOLookAt_m35971281812CD6A5EA5B28427A009F2C2E4FE533 (void);
// 0x0000025B DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics::DOJump(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOJump_mD421D5659C38733B5E5B6891253257E5C425F47B (void);
// 0x0000025C DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOPath_m8DA02562B715A7AA662FF9FE0960A11A61B15CD2 (void);
// 0x0000025D DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOLocalPath_mA9EFBF2E389456FB671468302622565E9F66F5EE (void);
// 0x0000025E DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOPath_m420764C2597301E7048F11600713705AD0DADC1A (void);
// 0x0000025F DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOLocalPath_m720A2CA6D647E4D8AAA386F83F4FE96E08C73082 (void);
// 0x00000260 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m6C8A8063B7CCC58E82305177538D7FBF1CE44935 (void);
// 0x00000261 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass0_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mB768E968FA1FD2B9AE13877E2692D758B495D013 (void);
// 0x00000262 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m479175628F7D1842EE3DCF05C86FAD7B511ABBCB (void);
// 0x00000263 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass1_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_mE0FB6A6EF157A4BC22104D4F91C164AA574DB227 (void);
// 0x00000264 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mB0ADB1BBDA7BF0DD9C2414079D2012E8D61404BE (void);
// 0x00000265 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass2_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m79746B44C774CACFDAFA83EFCC9E6D6D03AD82D6 (void);
// 0x00000266 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mBAA31984E2A716B3498B2B0D79CA25A892D748D0 (void);
// 0x00000267 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass3_0::<DOMoveZ>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOMoveZU3Eb__0_m11FF22865EE53FAB3B972AC3B34D3BB01526DF77 (void);
// 0x00000268 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m71B8B5E5A7CFD905A28CFCEF9005FDBE70096CBF (void);
// 0x00000269 UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass4_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDORotateU3Eb__0_m253899ED69AC81AED8238765AD9C3F808B81F9B8 (void);
// 0x0000026A System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mE16BED5275B5EEEF36A20B3C783CC4E7B0D580EE (void);
// 0x0000026B UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass5_0::<DOLookAt>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOLookAtU3Eb__0_m1C50D196B81BA62AC84D966F2515E56293697921 (void);
// 0x0000026C System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m6F040CE636E4D727C99FC1DF62BB42721707EC92 (void);
// 0x0000026D UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__0_m777E7729EA52B752D4212827ED4DB839B6A1F93D (void);
// 0x0000026E System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__1()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__1_mB7EEF6913E4971D9438101C2DBB950BB0877E640 (void);
// 0x0000026F UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__2_mC008279D66615921E8D8F95A88349CAAA4F3ACF3 (void);
// 0x00000270 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__3_mA4E1F014B086C15F0D7F66564619553D524535D2 (void);
// 0x00000271 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::<DOJump>b__4()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__4_m7190E82AA00FCA218DE01487303AC2AAB966BA14 (void);
// 0x00000272 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m36AD8AF752829C4A0601828F600EC8D181BB7F9E (void);
// 0x00000273 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass7_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_m424F1A2360CF360DEAEB2AE4B03929D8415D9D85 (void);
// 0x00000274 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m75DF5195919998D6D0FA3C9F4EE2052A1856612B (void);
// 0x00000275 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_mCAB97E4D7EB96AF59820216D2F20F3B32E9CE914 (void);
// 0x00000276 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m409709C8E90B9323B6467D70BF2E623079B9C8F8 (void);
// 0x00000277 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m8B38DEC85AB1D858ED6289CA714E5A7DC6384424 (void);
// 0x00000278 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_m57E3DDA730AB3A0A591C16CA8F9FEFD864FBD23B (void);
// 0x00000279 System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mEF982F61A1DB0156415F649C9BEA4F7D6BBB08AD (void);
// 0x0000027A UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m330A66B432859CDB4EFBABCA77577716AD33B665 (void);
// 0x0000027B System.Void DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mF002FDBED4B8F0AD918205030DF8CB3791415483 (void);
// 0x0000027C DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMove(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMove_m129881ECAC47858FD6BDF4872DFC10009AA91774 (void);
// 0x0000027D DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveX(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveX_m58FD03E827955317D3A61BDE358C692C085BDC67 (void);
// 0x0000027E DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveY(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveY_m6CA0A894F764679E356EEAC3047063E66AE8507D (void);
// 0x0000027F DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModulePhysics2D::DORotate(UnityEngine.Rigidbody2D,System.Single,System.Single)
extern void DOTweenModulePhysics2D_DORotate_m8FF3AA95400BD0ED1E539A13B9B33D8DCBE72CAE (void);
// 0x00000280 DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics2D::DOJump(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOJump_m2F08ADE05D23A793E24F4C9540D2159DADF8E655 (void);
// 0x00000281 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m4CB0F23C2FD78ED18921BA431C11EC8841878845 (void);
// 0x00000282 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass0_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mACB8350BB439EC9BDAE8AAA9042BA1D2C2260840 (void);
// 0x00000283 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m51B26BC729B71979744C78CCF6EED60B2D14109B (void);
// 0x00000284 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass1_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m57744501683F232996387D707B7BD355F4F01547 (void);
// 0x00000285 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m657C45B791F1EA7BE1759E94B7B75A23390BA39A (void);
// 0x00000286 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass2_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_mB38D7BA656E8BFBDA07E30ADB33ED05B7EA2AAE5 (void);
// 0x00000287 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m478EC1218BEF6E85E06C40AB67A0D7463E600D49 (void);
// 0x00000288 System.Single DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass3_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDORotateU3Eb__0_m8C384490EBDB1B769B4D88A4182E40E46A2A380B (void);
// 0x00000289 System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mFF8025919ABCD075B0E67C3F07356DD3CFC60605 (void);
// 0x0000028A UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__0_mE1774E966F8B4E8EC7EF821C59D3C5E8BF8FB071 (void);
// 0x0000028B System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__1_m72CB74348BBB9FF7126C8670CA7A9E858279DB46 (void);
// 0x0000028C System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__2_mB82162455CF0857F8934DC23F558BC2C6B2A51B1 (void);
// 0x0000028D UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__3_m9EB716BAFAB813B716B78872E6249CE4405E4CEA (void);
// 0x0000028E System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__4_m6CF2D0D226343F9C2144DC7C2F468397EE74B74F (void);
// 0x0000028F System.Void DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::<DOJump>b__5()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__5_m8A4065ED3871AB729FEC6C9B32F403681C9ED206 (void);
// 0x00000290 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOColor_m1D8613D58BC87177BBFB8F8C48803084362CC46F (void);
// 0x00000291 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
extern void DOTweenModuleSprite_DOFade_mD42EB7C410F2D7E86EF154211E97C3D72FAB6D99 (void);
// 0x00000292 DG.Tweening.Sequence DG.Tweening.DOTweenModuleSprite::DOGradientColor(UnityEngine.SpriteRenderer,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleSprite_DOGradientColor_m1D99BA2B2479484B3104790FA7CF734042B56275 (void);
// 0x00000293 DG.Tweening.Tweener DG.Tweening.DOTweenModuleSprite::DOBlendableColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOBlendableColor_m1CD3ADB6D2A6C868FA9A25527B33D1D36D3918D7 (void);
// 0x00000294 System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mB3A38B7C4B2EBEC0CA4CB4BCD0996F70D0D38573 (void);
// 0x00000295 UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m34BED3EF004629B2391C43F22D2ECC75EE20C056 (void);
// 0x00000296 System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m8236490A867654780100440C6D27299ED4F26767 (void);
// 0x00000297 System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m9A4008AB7FCC8A503A7CAED6CBBACDADF8E0A2F0 (void);
// 0x00000298 UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_m5F0C4CC4570355DFDF8A0AC86BB45FA7127558B2 (void);
// 0x00000299 System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_m353548C0500E2640824FDC1FE767DF2645A3FFE4 (void);
// 0x0000029A System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m3C72E35866D164666CDD16DA3FD45D2AE46D9E24 (void);
// 0x0000029B UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_m424C5E007014DBEFF422D4E1C016F8D14B192DE8 (void);
// 0x0000029C System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_mE1792C420EAF86DF15D7258F252C269F8931578E (void);
// 0x0000029D DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m70733D91ED98AB827DD5162868199BCA07B3C65F (void);
// 0x0000029E DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m9CCD69C96E0EC2AF9796758A83BCAE38E8A99257 (void);
// 0x0000029F DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Graphic,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mCF2D26E252987610391CA58F42EBF89D497AB42C (void);
// 0x000002A0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mAB44E96C3BE821A668FD934C349F33DB052BD34B (void);
// 0x000002A1 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m6E4705A5E978F074AC027B2622447F16464797C8 (void);
// 0x000002A2 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFillAmount(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFillAmount_m1701806AB0CE37D1A7BA5E97AC2A1CF2FE381A64 (void);
// 0x000002A3 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOGradientColor(UnityEngine.UI.Image,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUI_DOGradientColor_mD41A204AAA1149EFD8F63003EFB3B5B86851393F (void);
// 0x000002A4 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOFlexibleSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOFlexibleSize_mC1B116AD5E04DEE061C879DB93173AF3BF7BAFA2 (void);
// 0x000002A5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOMinSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOMinSize_m43AE2B4C0C85152C1F0C1A1A07C7C6647BCA50FB (void);
// 0x000002A6 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPreferredSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPreferredSize_m637389A7A4F0CAA593C1327F420E3F0CD7F4FBAC (void);
// 0x000002A7 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Outline,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mE71263080B1924D0D208D8D8B6E134726CF1C502 (void);
// 0x000002A8 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Outline,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m9870943D62346E19D61067631D8E034995D0F093 (void);
// 0x000002A9 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOScale(UnityEngine.UI.Outline,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOScale_mC78CB7454A8AA9419E06B38472C5751284669753 (void);
// 0x000002AA DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos_m5771DC15ABB88CC37D1A22B035E4D2FB638356B7 (void);
// 0x000002AB DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosX_mFC8A1A246CDB7E120FAC42CA43A7356F1F1A26B3 (void);
// 0x000002AC DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosY_m8371486009B6D9D9E5496D940591EBBBDE78AB51 (void);
// 0x000002AD DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3D_m9152258BD8EF0A999624C76BC8149BD46607EF33 (void);
// 0x000002AE DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DX_mBC59977D6899FEAB2C3B45554EB28E27665577A3 (void);
// 0x000002AF DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DY_mD242D5809B7F59D96655054309976DD8C8A45B2A (void);
// 0x000002B0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DZ(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DZ_m5CA72828C6290C08B5AE19AEEA4BACE05315583F (void);
// 0x000002B1 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMax(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMax_mF557034D626FD7CD2809F16F57071E5234A0A01D (void);
// 0x000002B2 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMin(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMin_m13FBF4F90CF58B50BDC27BAF0CBC7C4376DF4016 (void);
// 0x000002B3 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivot(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOPivot_m4294354A52355B2955965B3726DA2CC0A049F7A5 (void);
// 0x000002B4 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotX(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotX_mAF48236E3A05FA8787920BE4F939A4E652D6EA1D (void);
// 0x000002B5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotY(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotY_m478C274ACECBE8C0C0D9D43FF49DFF41DD28595F (void);
// 0x000002B6 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOSizeDelta_mC80355FB2E78330D2EC907DC14F8B6A8179CB09A (void);
// 0x000002B7 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPunchAnchorPos_mA9B79DE480E85187CD9967AEAC55F29FB4C7B587 (void);
// 0x000002B8 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,System.Single,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m44811D5F919207AB4ACA282656C76F690E41A2C4 (void);
// 0x000002B9 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_mA36B5E94FEC480304F89946C695F8DCBF637DC72 (void);
// 0x000002BA DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOJumpAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOJumpAnchorPos_m0D6AE7D1C70F77C307C2F74A7DE5575B38310323 (void);
// 0x000002BB DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DONormalizedPos(UnityEngine.UI.ScrollRect,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DONormalizedPos_mA0AAE9E65E8F1808464E0EFE63F26A76D4C4D86B (void);
// 0x000002BC DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOHorizontalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOHorizontalNormalizedPos_mA1C4D197CF803EA1826D3D23A3A0DD665EDA535B (void);
// 0x000002BD DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOVerticalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOVerticalNormalizedPos_m0CF6EC00B4AC886CEA5E8D99F9F5E26F254D52E0 (void);
// 0x000002BE DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOValue(UnityEngine.UI.Slider,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOValue_mBD157558885F4CC9BB52696CA6E7FCFBBE289D31 (void);
// 0x000002BF DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mF7C94BF952B9549609B1F7A3892A6F02CEB56F04 (void);
// 0x000002C0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m86A4E7965D2563BB1530EB2343AB8DA2EF7FDF01 (void);
// 0x000002C1 DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions> DG.Tweening.DOTweenModuleUI::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern void DOTweenModuleUI_DOText_mB47F3908BD77013A7C5DF5CCFC2E3F4BB60E00F5 (void);
// 0x000002C2 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_mF0936D1AF990F743A105B24F1D6988610F2496A9 (void);
// 0x000002C3 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m0E5CC97374C3B817CB3A04FFA87DCBBA545DB147 (void);
// 0x000002C4 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_mAA670DFB22BE7C9256667D65F40FF487C2AECF14 (void);
// 0x000002C5 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/Utils::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern void Utils_SwitchToRectTransform_m927E0306521B99C5AF717563A4AB216A3846ACFC (void);
// 0x000002C6 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m02AC5FF72AD9F7271BAE9AB34F80D42AA9A9C484 (void);
// 0x000002C7 System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m160924332BA5EF66E12B6FE8BDA1DBC69059EAAD (void);
// 0x000002C8 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mCF3914E7F3A8F729B174AA946341922991E381CC (void);
// 0x000002C9 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m1C5DB32757E72E49AF272F1384712046C6A00253 (void);
// 0x000002CA UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_m9467C7755B2D29380CDBCB4190BFC6A077E64C39 (void);
// 0x000002CB System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m44B0F01B58106E8E85D97C2FBB8A460A0571FE77 (void);
// 0x000002CC System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m4D9DFB7EE2AAC12968C4DF238BD125C654548FF1 (void);
// 0x000002CD UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_mCAA3704209EDD53B81A477BF1AB73EA69F26F845 (void);
// 0x000002CE System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m7B6FA582025F7A33540FA056A8EA424D3BD33BE7 (void);
// 0x000002CF System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mC5C767AD26CB3FE0A493044C71B6CCEC8346C296 (void);
// 0x000002D0 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m312CB07635E6BF7DDA4F7D69B3844ADDDF806465 (void);
// 0x000002D1 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mC81F83EDCE139870DE9369D6478622169885365E (void);
// 0x000002D2 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m15E8C7F3799861E5CCA5AAD2CD1CD28224AA03E7 (void);
// 0x000002D3 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m0B5F0F9301C56DDEB288ADD86745A1D635CE1416 (void);
// 0x000002D4 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m18E36E9B1AFC305E378F79E25EE9A932B959588E (void);
// 0x000002D5 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m5DB0CA121DB5A86ADCD13BB04F71705F65B09E65 (void);
// 0x000002D6 System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0::<DOFillAmount>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m424FEBA1D754E6D82F1013FC993B129F0175C745 (void);
// 0x000002D7 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0::<DOFillAmount>b__1(System.Single)
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m83B635790469064B562A721BEE7A6C83B43903C8 (void);
// 0x000002D8 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m3F4D7A459A8733E43E7A64A569F9EA52B535D2B6 (void);
// 0x000002D9 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0::<DOFlexibleSize>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_mF3831E19E398DAD98077121A417BFD6A74D3EBCB (void);
// 0x000002DA System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0::<DOFlexibleSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m605F371D6A593F9F1A3A249368409471CEAEC646 (void);
// 0x000002DB System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mA6BE9E902B051E8864004E9F53E0D01796C27BEC (void);
// 0x000002DC UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0::<DOMinSize>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m24F632B6998489E51C1E60F23FE8A05B9F9BF80A (void);
// 0x000002DD System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0::<DOMinSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m5E36BA70B14EBA24EE5150746690346E78F966B0 (void);
// 0x000002DE System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m286D59D82E41B5281985D79E488198A7E3537ACC (void);
// 0x000002DF UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0::<DOPreferredSize>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mF535C0871CF853AED0D5A8ACB39FC367D1CB0A64 (void);
// 0x000002E0 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0::<DOPreferredSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m185139D594B32B631796CD61D79F0B48C5431765 (void);
// 0x000002E1 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m11DD7C4B58AFC2CB99F683955907A640E74282C0 (void);
// 0x000002E2 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_mAFCCC57700B3B00BDACF629FB898B3DEDF8C7668 (void);
// 0x000002E3 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m15F5B7ECAA029DC5E8A50857A16A48A8A6D6992B (void);
// 0x000002E4 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m6D4ED0270AF0D5C0A75BF692250CB853EC2F6F79 (void);
// 0x000002E5 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_m2D6173B6882E022ADB4AC6CBA3173304B8E6164F (void);
// 0x000002E6 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_mCE887CF626E5ACCB29B0E98C921AC7A3E9233A7E (void);
// 0x000002E7 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mD1F9075FEC29F490C6679A9D7598D759C0AB4DBE (void);
// 0x000002E8 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0::<DOScale>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_m2385A603E1E66F822FECD5CEACCE771B5758D7B7 (void);
// 0x000002E9 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0::<DOScale>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_m99EB252D563A798BEE0B5A31C659851CD0700BAE (void);
// 0x000002EA System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m84FD04DEF9DEFE8BB6EE6B768471B2332B4E66EA (void);
// 0x000002EB UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0::<DOAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_mDEEE77743125A9F4AF071993A48C0883B8938147 (void);
// 0x000002EC System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0::<DOAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_mEB2040061609C631073D063D8BA71C5AE458CB63 (void);
// 0x000002ED System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m188221CF95D995E27ABFF9D447F3D54149D4B5F2 (void);
// 0x000002EE UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0::<DOAnchorPosX>b__0()
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m18F22E4AA3CBA7FEFF94AA2AA7438EBC5C4DBF3D (void);
// 0x000002EF System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0::<DOAnchorPosX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m76DB47AD4C1DD0E522CD7E49E4C0BF890512FCC7 (void);
// 0x000002F0 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m1D4F00D13860F242587ACB9888B000C3A767492B (void);
// 0x000002F1 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0::<DOAnchorPosY>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_m61698EC758F0F82BBA818949C0BD2F3CFADFAA57 (void);
// 0x000002F2 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0::<DOAnchorPosY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_mD35972634DF21705BA6A9B3DB5CF0D81CECC219B (void);
// 0x000002F3 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mC9174DECC9AB0C0FCA7E0BB5626F00A397552285 (void);
// 0x000002F4 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0::<DOAnchorPos3D>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m04D393C6058D10956299620A3367587B40EADADF (void);
// 0x000002F5 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0::<DOAnchorPos3D>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m29D334493F9D5F6BCADC9E5725DF0A510EA47208 (void);
// 0x000002F6 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_mDCEE61F98BC23EF18977566788E29085B0DC4B49 (void);
// 0x000002F7 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0::<DOAnchorPos3DX>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_m3F4080C62B849D50FF7C58A8257FFEE91BBAB680 (void);
// 0x000002F8 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0::<DOAnchorPos3DX>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_m4263B6642D9E77CB38E01A24F3C737199ACC9F93 (void);
// 0x000002F9 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m8463A61BCCD89F89CBF0F79CF66FA4EBE2FD1E4F (void);
// 0x000002FA UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0::<DOAnchorPos3DY>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m256585104D3EF107F980E00EBCA0AFE9AD81F6CF (void);
// 0x000002FB System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0::<DOAnchorPos3DY>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_m9A7522C9582F9572AF53FFCE8D3D745BE00DB9C0 (void);
// 0x000002FC System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_m15B509E8ABCF2C177192FF9870B15FEE66F4D038 (void);
// 0x000002FD UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_m7C246FCA6BAD4A13638C1361F6680CF5211DFD15 (void);
// 0x000002FE System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_m49E6856829E99FE417EC50159BC5FB0D1A77CBD7 (void);
// 0x000002FF System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_mDD65EA9F35AA085EDC5AB6EA7BCB430AB53E7C4C (void);
// 0x00000300 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0::<DOAnchorMax>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_mD207906E09F8A121195553ACBF187B8D0A3A6006 (void);
// 0x00000301 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0::<DOAnchorMax>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m25FD7BC6C81DCBCB7936948A09EA50E27D3DF5E1 (void);
// 0x00000302 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m7CA9A2889EDB39EBF81C4320023731198C2E28F6 (void);
// 0x00000303 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0::<DOAnchorMin>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_mB7F92CB811EC69B78D4DB72A07EBFE0065B81A96 (void);
// 0x00000304 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0::<DOAnchorMin>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_mE3580800BF86444C99AFAD3651A10A8BA87ACA3D (void);
// 0x00000305 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_mE2C45E43AE15B030C3E9EA28C6FB89B27993353C (void);
// 0x00000306 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0::<DOPivot>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_m965979FB6B275C69A73170025341021C75A3BD73 (void);
// 0x00000307 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0::<DOPivot>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_m4A27FCA6236E27F8F1156DF1A947D5E14C97900E (void);
// 0x00000308 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mAFBDA472706665D20FBB1B5C8CE8C560842578A1 (void);
// 0x00000309 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0::<DOPivotX>b__0()
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mB04DAF8BA4880841E68A83B7182EB53669831FFD (void);
// 0x0000030A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0::<DOPivotX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_mEE841F6ADEDD8942A5BAB79D9F181646759EE2B0 (void);
// 0x0000030B System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_mC59776621BA36942C91F3BFED7DE4391B0625F07 (void);
// 0x0000030C UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0::<DOPivotY>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_m1DF542C0FC15A842BC687B1294E2DE72BFF44B86 (void);
// 0x0000030D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0::<DOPivotY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_mB43317F02A9C33D46CE93B16DD1104D28BFFE75E (void);
// 0x0000030E System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_mCF92B4AFC980734E8F9FBD06D7709D5B610C2AD8 (void);
// 0x0000030F UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0::<DOSizeDelta>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_m2D894FC2DD5FE9C3641758DA07FBB2415E191750 (void);
// 0x00000310 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0::<DOSizeDelta>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_mAB31C40435305381E674D7BCDD36C1FA2D944535 (void);
// 0x00000311 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_mE11542986539E00ECC4ACB110B932CB659280B9E (void);
// 0x00000312 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0::<DOPunchAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_m8C345DF5B27AE5B17D74FCDB6CC68D367576F8F5 (void);
// 0x00000313 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0::<DOPunchAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_mF593BD39BAA9EA695EA52E6B025249916A3ACB21 (void);
// 0x00000314 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m3F5D43CB088C6A3C4D6CC51DD761FA23F832B806 (void);
// 0x00000315 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m0958BB22CF771F2BE804E7BDD83C361073F3581E (void);
// 0x00000316 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mA6D65C9BAD704941C13ACD1DE3D3A6E8FA58A6B1 (void);
// 0x00000317 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m671FD2A8D4DC6429FA93C8A41C7AFC6B0CB50CCF (void);
// 0x00000318 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_m71D55809BE5527F7FA76198ADEAE5E5917555699 (void);
// 0x00000319 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_m7A7AAA1B4D13FE941B7051A2395795B2019BA497 (void);
// 0x0000031A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m6841EBC72678519402C9678A4903856BA01D3E3C (void);
// 0x0000031B UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_mDD03C988E48C946E2051BBCB400D11AA475172C5 (void);
// 0x0000031C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_m0A7D3990B8A6B423B590109635761C4D7845AB7F (void);
// 0x0000031D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__2()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_mB2EB9443C133F983C1AA76FAB66C80F28853B8F4 (void);
// 0x0000031E UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__3()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_m7E2202F4B18C4169BD217DE526E9543C40EF29C0 (void);
// 0x0000031F System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_mDA917A9381F14BC3DACB27C22CAF05FE45DBAC90 (void);
// 0x00000320 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__5()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_m3CC81967E2004B932868626D598B72943C76C21F (void);
// 0x00000321 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_m475CA4BAAE3F4C95D59EC2A5210019E22FD7E72D (void);
// 0x00000322 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0::<DONormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_m1F0943C5D1EA2BAE72905488B7BB7FD05AD5414B (void);
// 0x00000323 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0::<DONormalizedPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_m8CE913B6CB904A504400097D0AA6EDDBD7AE0B45 (void);
// 0x00000324 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m8CB90BDE592351E68684EB4D199BB309EC06F466 (void);
// 0x00000325 System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_mDF3CB62908D4575E14BF7E98CA262F8149BB0E48 (void);
// 0x00000326 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mEB5E8758E0EF969FFADC04CA3147F5A888B40064 (void);
// 0x00000327 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_m1A8D52B69FB8395ABBC9A4F7DF8E36466DB3BC95 (void);
// 0x00000328 System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_m71DE61BFA6209DB829836838A7F1EB7F5C1C951C (void);
// 0x00000329 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_mCB1CE1E45A17FD19D929A7C3EA95C7B492AFCCA2 (void);
// 0x0000032A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m0DF68C5C8156D45B1870272C41290E5B12FF8602 (void);
// 0x0000032B System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0::<DOValue>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_m145C72DAB8DD1E5AD043A2B5900812E0F0E58E83 (void);
// 0x0000032C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0::<DOValue>b__1(System.Single)
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_mF3EAA5B02B860189FE7841E1642095AF9488F134 (void);
// 0x0000032D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m4E36DF0E111FF20A584BABCB272C7514CBAF59A2 (void);
// 0x0000032E UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_m47F62A48B92963FB32511F3875D566F8FC71480B (void);
// 0x0000032F System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_m9541F5AD304519AFA79192889487ABC2E621EE3F (void);
// 0x00000330 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_mEB07972AA46E127DB75E68DDA3FB5A54DFE1A07C (void);
// 0x00000331 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__0_m8FFA77E8FB4FCE8F0125962D1192E052256AA512 (void);
// 0x00000332 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__1_m857FA607EA2B021E9A5DF0AFA774242459924E3E (void);
// 0x00000333 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_mD71C2FD9E280F3A4DC631CA425A24E1AE0265CC8 (void);
// 0x00000334 System.String DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::<DOText>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__0_mE1A296C1F72132B2BF559B54C734121CF98679DC (void);
// 0x00000335 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::<DOText>b__1(System.String)
extern void U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__1_mDB35377C8C2F3FCAA36BDC6104B6123A01C159E7 (void);
// 0x00000336 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_mDA142B56ABBC2597FCB543F5EA51FDE52DDA91E5 (void);
// 0x00000337 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__0_mA59968C9C7B4E749CB08FBD0B6EEF08A1B66B362 (void);
// 0x00000338 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__1_m6FD982BE0D5D355F1C0CF61FEB38A244956BE6E8 (void);
// 0x00000339 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_mF5E51CB763FACE84BD5CD7EBDA60614BF88959E9 (void);
// 0x0000033A UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_m5CA9724149FCD3E48658442157DA3249D8966F6C (void);
// 0x0000033B System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_m800BBDFF60B762B724C762D9C29F15474FB8A698 (void);
// 0x0000033C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m309F737816844D4A4CDC9518D1DE826E30C6D271 (void);
// 0x0000033D UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_mFFFA826A2C8066C0B5695C8153E6788167CCAD86 (void);
// 0x0000033E System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m7C0B3D7601C4F8711E55BDE39FF63F235CE9F904 (void);
// 0x0000033F DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_mC9C2768747A9B142795620CF5501BCCB0385A64A (void);
// 0x00000340 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.String,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_m8B5EA2127C5D9206A1BC6BAC15BD89D5D9077DCC (void);
// 0x00000341 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForCompletion(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForCompletion_mCD910620513CFC66E6360BDF100567E534DA8C47 (void);
// 0x00000342 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForRewind(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForRewind_m80AF71AC60E938C7E0D4FD01CCBF98667DEDB8A1 (void);
// 0x00000343 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForKill(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForKill_m5D2A034D4E6912EE8FF0AE063329807F3399FF95 (void);
// 0x00000344 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForElapsedLoops_m347E45F2B3E7F133E50D93DE5712D9FB0E828A79 (void);
// 0x00000345 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForPosition(DG.Tweening.Tween,System.Single,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForPosition_m39F8AC8FBB5962A4CC8CEFB055C85B13284CC39F (void);
// 0x00000346 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForStart(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForStart_mE7E9F02D5CAF079B0F1C2002B633EA88C18AF3D1 (void);
// 0x00000347 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOOffset(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOOffset_m5FE227A1D7FE40DEBFD0D15E9E2E140611E49BD4 (void);
// 0x00000348 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOTiling(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOTiling_m46B35DDADB688BEB85EB21A5593E240C5FD990A6 (void);
// 0x00000349 System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m4E9AD608BFA5058EA275819037B5EE4E384311AA (void);
// 0x0000034A UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::<DOOffset>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m313F8C26E8A72E7404065072AA78E9C737E35620 (void);
// 0x0000034B System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::<DOOffset>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m4E8A169B481398D90A901BAD32A1CC0BDD77BAE8 (void);
// 0x0000034C System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m34A953C85477B2BD1B635BDAF8885267ACDAFEEA (void);
// 0x0000034D UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::<DOTiling>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m32DCD53537E5D9250D043396A6636431DA4AC5DE (void);
// 0x0000034E System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::<DOTiling>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_m586EE9E78A2967588A473314F2E4DDF7852A89A7 (void);
// 0x0000034F System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForCompletion::get_keepWaiting()
extern void WaitForCompletion_get_keepWaiting_mC42F1D87B76571FEF5C96EB25CD2F233A7FBDDE2 (void);
// 0x00000350 System.Void DG.Tweening.DOTweenCYInstruction/WaitForCompletion::.ctor(DG.Tweening.Tween)
extern void WaitForCompletion__ctor_m194FFB6935EC44D8EBF9596755B410A7F3CAECDA (void);
// 0x00000351 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForRewind::get_keepWaiting()
extern void WaitForRewind_get_keepWaiting_mDC4ACCB00D1A64C9D59ED14620D3472036D2755E (void);
// 0x00000352 System.Void DG.Tweening.DOTweenCYInstruction/WaitForRewind::.ctor(DG.Tweening.Tween)
extern void WaitForRewind__ctor_m8FA908A77DFFAACB814B717915EAFBDA10B2FB36 (void);
// 0x00000353 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForKill::get_keepWaiting()
extern void WaitForKill_get_keepWaiting_mC3BC5155F3822FA2486800624680EA65464044D4 (void);
// 0x00000354 System.Void DG.Tweening.DOTweenCYInstruction/WaitForKill::.ctor(DG.Tweening.Tween)
extern void WaitForKill__ctor_m13C20AC2C9C2809AD51B8CEA30F11AACB4ECC64C (void);
// 0x00000355 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::get_keepWaiting()
extern void WaitForElapsedLoops_get_keepWaiting_mD62F90B0E8F34349F6CBCAD1DEE8C69F6AC9A4A0 (void);
// 0x00000356 System.Void DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::.ctor(DG.Tweening.Tween,System.Int32)
extern void WaitForElapsedLoops__ctor_mA2399269DDE36055552944650E8862464AAFF448 (void);
// 0x00000357 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForPosition::get_keepWaiting()
extern void WaitForPosition_get_keepWaiting_mCE1EBD6DFC3462C603585CE8DD978F582FC0ADA6 (void);
// 0x00000358 System.Void DG.Tweening.DOTweenCYInstruction/WaitForPosition::.ctor(DG.Tweening.Tween,System.Single)
extern void WaitForPosition__ctor_m218039DD8AEC82CDB8BF269764EC8566A4E01F6F (void);
// 0x00000359 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForStart::get_keepWaiting()
extern void WaitForStart_get_keepWaiting_mF6E49FFC399C5642C630865FCCB38FA56DCA890D (void);
// 0x0000035A System.Void DG.Tweening.DOTweenCYInstruction/WaitForStart::.ctor(DG.Tweening.Tween)
extern void WaitForStart__ctor_m733FFDFE0C36F20AA645593347BDAA8D0610D508 (void);
// 0x0000035B System.Void DG.Tweening.DOTweenModuleUtils::Init()
extern void DOTweenModuleUtils_Init_m9F7938AFFD814978DEE9140ED34B6E6647711324 (void);
// 0x0000035C System.Void DG.Tweening.DOTweenModuleUtils::Preserver()
extern void DOTweenModuleUtils_Preserver_m5BBFCFD03A1E22FF9651F2407FB32FBDACFBEFDD (void);
// 0x0000035D System.Void DG.Tweening.DOTweenModuleUtils/Physics::SetOrientationOnPath(DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform)
extern void Physics_SetOrientationOnPath_m7909652CCC78E77ADE33C5BEB4BFF42AA536A7D1 (void);
// 0x0000035E System.Boolean DG.Tweening.DOTweenModuleUtils/Physics::HasRigidbody2D(UnityEngine.Component)
extern void Physics_HasRigidbody2D_m7B055F7D6B725DCE1616DE1BCAC6AF1E402946E6 (void);
// 0x0000035F System.Boolean DG.Tweening.DOTweenModuleUtils/Physics::HasRigidbody(UnityEngine.Component)
extern void Physics_HasRigidbody_m49C216B000D976B6AD22EA36C29BB922F4B02A2B (void);
// 0x00000360 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModuleUtils/Physics::CreateDOTweenPathTween(UnityEngine.MonoBehaviour,System.Boolean,System.Boolean,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void Physics_CreateDOTweenPathTween_mFA2C0271A146797AB71CD136F2A56ECA3A5AAB5C (void);
static Il2CppMethodPointer s_methodPointers[864] = 
{
	PanelManager_OnEnable_m5A7DC3931168B59A585EAEFA1AB80F7D3EE8140F,
	PanelManager_OpenPanel_m456B82F7B62A00937F60CE6014C1F3698414B7B4,
	PanelManager_FindFirstEnabledSelectable_m0C277F0C1F1112D83086EB3AE8B85A5E06AEBAB7,
	PanelManager_CloseCurrent_m824C6139CDBD415DE8E4CF1FB557FCE79D69B2C7,
	PanelManager_DisablePanelDeleyed_m1DF8E917EEC8FD14E76954ED3F2918C76B65E5AF,
	PanelManager_SetSelected_m59DAF072CE220595B0AD4BC0BEA1C00856943B71,
	PanelManager_cambiarEscena_mF1C9633FDF51D589F19105CE55AAD7511E7D5EB2,
	PanelManager_Quit_mD1B36EF6861BAF23C1FA645A6F2DBF788708C9B6,
	PanelManager__ctor_mA86BE5A7EC580FAD466DFEC12569D38F584473BB,
	U3CDisablePanelDeleyedU3Ed__10__ctor_m4282B9D12EB2BB6D413E69F116601539D4A98B99,
	U3CDisablePanelDeleyedU3Ed__10_System_IDisposable_Dispose_mA29BD3E3DE14A63123458C1B873D3B9520CCD0E7,
	U3CDisablePanelDeleyedU3Ed__10_MoveNext_mDCC2DE4CAA28ED5E8135119B9A4E38D075FEC235,
	U3CDisablePanelDeleyedU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEA0AE880F4D89D9DC62386E0448E6FE576A4AE66,
	U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_Reset_m17342096F195588A8C598895C595B0B029F598C9,
	U3CDisablePanelDeleyedU3Ed__10_System_Collections_IEnumerator_get_Current_m47973E705B6316F9392B95B9A84ED6431CF8A0F0,
	CloudManager_Awake_mC281B36156D900D74224243FEE514F425453FA8F,
	CloudManager_Disable_m4346BFFA3CF6C43E5EC91F725CFB7DBB65CA1B41,
	CloudManager_OnIntialized_m5EB013F3EC8466B0704E2728D7AAAE8A26D72771,
	CloudManager_OnStateChanged_mADC49A5CE10B6F5C3720568C7E00685F39EC3FE4,
	CloudManager_OnNewSearchResult_m374FEE74A66B7C34FF61C91BEE164933AA761B68,
	CloudManager__ctor_mC283BEFBBE11285AF9FF014862945F5C4F854522,
	Contoller_Start_m5AB996E9B9013C449C87EC2C85F6C5598C884609,
	Contoller_Get_m0DAF5050136FA5C1FC2EDC3BDE08EB74C12C2E15,
	Contoller__ctor_mBCDDD49F99F6643440F8B1CBEAE2D2B0F94C00D0,
	U3CGetU3Ed__3__ctor_mA159298432F3E24048EDB77AEDBB9E4D1F6EC0F0,
	U3CGetU3Ed__3_System_IDisposable_Dispose_m74C148758410BBC62D5CFFACA9B0F6350534B57B,
	U3CGetU3Ed__3_MoveNext_m86C2A4DE58A32ECCD939F62EB5859B2CC563E497,
	U3CGetU3Ed__3_U3CU3Em__Finally1_m1377F257BFEDDD1598352FD9195295745F765CCF,
	U3CGetU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA0ECE5FB46A7214EB170815BECDC8F70C1D7E71B,
	U3CGetU3Ed__3_System_Collections_IEnumerator_Reset_m89030D1AA002A4EAD07A81FB98B960BA35C5B0F9,
	U3CGetU3Ed__3_System_Collections_IEnumerator_get_Current_mFB56B088ACC934B4B786EDD606420E0F55A78E0B,
	Imagen__ctor_m1E4F65ECE22DD51FC4F858BD84846A905B323F7E,
	Data__ctor_mAD0738081D5922E1E6C75A516F6564FBB8468214,
	Root__ctor_mC5B98C2C36B9EA08AB28CAEDCE2E1BF81AFB8B0A,
	EnemyViewController_DisplayEnemyData_m73408A8C09F0A67C8774933C2C1321A90EDA5914,
	EnemyViewController__ctor_m8B405F9846E5572838B7CEF310FF2F5EE16C660C,
	Imagenes_Start_m8D420C7EB9CF2B51B32D2B661FF225F430888916,
	Imagenes_Get_m71D5919B4D892D16500885AC3EF1FD1C5AD9A3DE,
	Imagenes_GetImage_m40970D88BB34A8029ACBFB952D007431A6367A1A,
	Imagenes__ctor_m409A3B5D97F11113F2257943E3A63FAC356B9265,
	U3CGetU3Ed__3__ctor_mFD87C05F3E31FF55056C3ECA3B2C4F2A91FD54A5,
	U3CGetU3Ed__3_System_IDisposable_Dispose_m576F7E3961492100BADBFA8934F6A9DFE1A33BF9,
	U3CGetU3Ed__3_MoveNext_m4995759CCADE54ED2CE1C6F972FE657535639ACE,
	U3CGetU3Ed__3_U3CU3Em__Finally1_m49F0031B1412A7285F5C8BD28318651B6DEF39A9,
	U3CGetU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39BAD77A234981A273187217200971C1ECDEB705,
	U3CGetU3Ed__3_System_Collections_IEnumerator_Reset_mB9AA0093AD2911BD64B077435DF1BC3C6813E3D7,
	U3CGetU3Ed__3_System_Collections_IEnumerator_get_Current_m25824D3EE53C6676C4061158728129214FC0E962,
	U3CGetImageU3Ed__4__ctor_m1E528EDD44E60B531AF5B17A8EA768FA6EE16943,
	U3CGetImageU3Ed__4_System_IDisposable_Dispose_mCE49B34AE9701268C498020428670B4662CA93A1,
	U3CGetImageU3Ed__4_MoveNext_m5C7EC5AAD0835753028F19500918F88B0B51A6AD,
	U3CGetImageU3Ed__4_U3CU3Em__Finally1_mDF25EED0CBEDEAAEE90F2F5762C0D12E2086B9F7,
	U3CGetImageU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF7FDBB8FE1250B2EAA7DF161C154F52E73AF4EDD,
	U3CGetImageU3Ed__4_System_Collections_IEnumerator_Reset_m505EB0E89DC8051E7ECB1B4B187E36A3C1B30B9B,
	U3CGetImageU3Ed__4_System_Collections_IEnumerator_get_Current_mA2C7F47B1A72A6E086D6D5BDF18AE886C951C550,
	OpenURL_video_m7479216005BAD28AA7D1008C169FFFAB83D62D49,
	OpenURL_unl_m62AB9FD319D7A50FAA37E254AF2B8D62CAA8A028,
	OpenURL_cis_mD204576686D6F0A5865075F039DFFA687D27CFEC,
	OpenURL_medicina_m40C4E22F5E71EA03F920E92A2881E9B47F26D3A5,
	OpenURL__ctor_m9DCD529CEA922A3F80E72D9C86916C3BD48D41CB,
	usuarioAPI_Start_mB8B20531333BB6CAF9847CA3B83A630E3B401AF0,
	usuarioAPI_Get_m9EC3376098D94E2F3E3CE05132CBF198F6215ED5,
	usuarioAPI__ctor_m8A157C87F0850D986CD0B72A341B524858C7BCCF,
	U3CGetU3Ed__2__ctor_mD4AB00CC5B78B052F14DEC8DAB200C2F3EFBF114,
	U3CGetU3Ed__2_System_IDisposable_Dispose_m2DB2C208FC244A0278BC7CCF314583D09D851760,
	U3CGetU3Ed__2_MoveNext_m29C1678896E956BD8B33BA563636D1394A6E5AAA,
	U3CGetU3Ed__2_U3CU3Em__Finally1_mDCA482D4936441601EF934917F2A6B8B4042F14A,
	U3CGetU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m77A30FE653A3743517FA523ADDF7D3EF1369A599,
	U3CGetU3Ed__2_System_Collections_IEnumerator_Reset_mE35D23A1D5013B2E2BA5A83B009D9F1CAAF067E9,
	U3CGetU3Ed__2_System_Collections_IEnumerator_get_Current_mD6E242EBC2C9EBD763F1DE3093E805545128A43C,
	ChatController_OnEnable_m025CE203564D82A1CDCE5E5719DB07E29811D0B7,
	ChatController_OnDisable_mD49D03719CAEBB3F59F24A7FA8F4FD30C8B54E46,
	ChatController_AddToChatOutput_m9AB8FA8A32EA23F2E55795D8301ED0BF6A59F722,
	ChatController__ctor_m39C05E9EB8C8C40664D5655BCAB9EEBCB31F9719,
	DropdownSample_OnButtonClick_mF83641F913F3455A3AE6ADCEA5DEB2A323FCB58F,
	DropdownSample__ctor_m0F0C6DD803E99B2C15F3369ABD94EC273FADC75B,
	EnvMapAnimator_Awake_m1D86ECDDD4A7A6DF98748B11BAC74D2D3B2F9435,
	EnvMapAnimator_Start_mB8A6567BB58BDFD0FC70980AFA952748DF1E80E9,
	EnvMapAnimator__ctor_m465E8527E49D1AA672A9A8A3B96FE78C24D11138,
	U3CStartU3Ed__4__ctor_m432062D94FDEF42B01FAB69EBC06A4D137C525C2,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m8088B5A404D1CB754E73D37137F9A288E47E7E9C,
	U3CStartU3Ed__4_MoveNext_mF689BF83350416D2071533C92042BF12AC52F0C0,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3CCB9B113B234F43186B26439E10AD6609DD565,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m3EF23BF40634D4262D8A2AE3DB14140FEFB4BF52,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C119A46A09AD8F0D4DE964F6B335BE2A460FAA,
	TMP_DigitValidator_Validate_m786CF8A4D85EB9E1BE8785A58007F8796991BDB9,
	TMP_DigitValidator__ctor_m9DC5F1168E5F4963C063C88384ADEBA8980BBFE0,
	TMP_PhoneNumberValidator_Validate_mE50FE1DE042CE58055C824840D77FCDA6A2AF4D3,
	TMP_PhoneNumberValidator__ctor_m70833F265A016119F88136746B4C59F45B5E067D,
	TMP_TextEventHandler_get_onCharacterSelection_mA62049738125E3C48405E6DFF09E2D42300BE8C3,
	TMP_TextEventHandler_set_onCharacterSelection_m6B85C54F4E751BF080324D94FB8DA6286CD5A43C,
	TMP_TextEventHandler_get_onSpriteSelection_m95CDEB7394FFF38F310717EEEFDCD481D96A5E82,
	TMP_TextEventHandler_set_onSpriteSelection_mFFBD9D70A791A3F2065C1063F258465EDA8AC2C5,
	TMP_TextEventHandler_get_onWordSelection_mF22771B4213EEB3AEFCDA390A4FF28FED5D9184C,
	TMP_TextEventHandler_set_onWordSelection_mA7EB31AF14EAADD968857DDAC994F7728B7B02E3,
	TMP_TextEventHandler_get_onLineSelection_mDDF07E7000993FCD6EAF2FBD2D2226EB66273908,
	TMP_TextEventHandler_set_onLineSelection_m098580AA8098939290113692072E18F9A293B427,
	TMP_TextEventHandler_get_onLinkSelection_m87FB9EABE7F917B2F910A18A3B5F1AE3020D976D,
	TMP_TextEventHandler_set_onLinkSelection_m6741C71F7E218C744CD7AA18B7456382E4B703FF,
	TMP_TextEventHandler_Awake_mE2D7EB8218B248F11BE54C507396B9B6B12E0052,
	TMP_TextEventHandler_LateUpdate_mBF0056A3C00834477F7D221BEE17C26784559DE1,
	TMP_TextEventHandler_OnPointerEnter_mF5B4CCF0C9F2EFE24B6D4C7B31C620C91ABBC07A,
	TMP_TextEventHandler_OnPointerExit_mC0561024D04FED2D026BEB3EC183550092823AE6,
	TMP_TextEventHandler_SendOnCharacterSelection_m5A891393BC3211CFEF2390B5E9899129CBDAC189,
	TMP_TextEventHandler_SendOnSpriteSelection_m8242C5F9626A3C1330927FEACF3ECAD287500475,
	TMP_TextEventHandler_SendOnWordSelection_mCB9E9ACB06AC524273C163743C9191CAF9C1FD33,
	TMP_TextEventHandler_SendOnLineSelection_mF0691C407CA44C2E8F2D7CD6C9C2099693CBE7A6,
	TMP_TextEventHandler_SendOnLinkSelection_m2809D6FFF57FAE45DC5BB4DD579328535E255A02,
	TMP_TextEventHandler__ctor_mADE4C28CAE14991CF0B1CC1A9D0EBAF0CF1107AB,
	CharacterSelectionEvent__ctor_m054FE9253D3C4478F57DE900A15AC9A61EC3C11E,
	SpriteSelectionEvent__ctor_m89C1D1F720F140491B28D9B32B0C7202EE8C4963,
	WordSelectionEvent__ctor_m3F52F327A9627042EDB065C1080CEB764F1154F2,
	LineSelectionEvent__ctor_m419828B3E32BC3F6F5AAC88D7B90CF50A74C80B2,
	LinkSelectionEvent__ctor_m4083D6FF46F61AAF956F77FFE849B5166E2579BC,
	Benchmark01_Start_m6CF91B0D99B3AC9317731D0C08B2EDA6AA56B9E9,
	Benchmark01__ctor_m9E12F5F809E8FF4A6EEFCDB016C1F884716347C4,
	U3CStartU3Ed__10__ctor_m242187966C9D563957FB0F76C467B25C25D91D69,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m7AD303D116E090426086312CD69BFA256CD28B0D,
	U3CStartU3Ed__10_MoveNext_m5F93878ED8166F8F4507EE8353856FAEABBBF1C9,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F5CE0A24226CB5F890D4C2A9FAD81A2696CE6F6,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m553F892690ED74A33F57B1359743D31F8BB93C2A,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m50D65AEFE4D08E48AC72E017E00CD43273E1BDBD,
	Benchmark01_UGUI_Start_m565A619941AAFFC17BB16A4A73DF63F7E54E3AFA,
	Benchmark01_UGUI__ctor_m9DCE74210552C6961BF7460C1F812E484771F8EB,
	U3CStartU3Ed__10__ctor_m515F107569D5BDE7C81F5DFDAB4A298A5399EB5A,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mFFD5DC6FCF8EC489FF249BE7F91D4336F2AD76AC,
	U3CStartU3Ed__10_MoveNext_mDCA96D0D1226C44C15F1FD85518F0711E6B395D9,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m109B5747CD8D1CF40DAC526C54BFB07223E1FB46,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mC9F90586F057E3728D9F93BB0E12197C9B994EEA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mA4DCEFD742C012A03C20EF42A873B5BFF07AF87A,
	Benchmark02_Start_mB56F21A9861A3DAF9F4E7F1DD4A023E05B379E29,
	Benchmark02__ctor_mE5DCB1CF4C1FDBA742B51B11427B9DE209630BF1,
	Benchmark03_Awake_mDEE8E96AE811C5A84CB2C04440CD4662E2F918D3,
	Benchmark03_Start_mCCFD9402E218265F6D34A1EA7ACCD3AD3D80380D,
	Benchmark03__ctor_m8A29BB2CC6375B2D3D57B5A90D18F2435352E5F6,
	Benchmark04_Start_mD2F5056019DD08B3DB897F6D194E86AB66E92F90,
	Benchmark04__ctor_m282E4E495D8D1921A87481729549B68BEDAD2D27,
	CameraController_Awake_m2D75756734457ADE0F15F191B63521A47C426788,
	CameraController_Start_m749E20374F32FF190EC51D70C717A8117934F2A5,
	CameraController_LateUpdate_m07E7F5C7D91713F8BB489480304D130570D7858F,
	CameraController_GetPlayerInput_m31AE86C54785402EB078A40F37D83FEA9216388F,
	CameraController__ctor_mE37608FBFBF61F76A1E0EEACF79B040321476878,
	ObjectSpin_Awake_mC05FEB5A72FED289171C58787FE09DBD9356FC72,
	ObjectSpin_Update_m7FB0886C3E6D76C0020E4D38DC1C44AB70BF3695,
	ObjectSpin__ctor_mA786C14AE887FF4012A35FAB3DF59ECF6A77835A,
	ShaderPropAnimator_Awake_m3D158D58F1840CBDA3B887326275893121E31371,
	ShaderPropAnimator_Start_mEF0B5D3EE00206199ABB80CE893AA85DF3FE5C88,
	ShaderPropAnimator_AnimateProperties_m9F466F9C9554AA7488F4607E7FAC9A5C61F46D56,
	ShaderPropAnimator__ctor_m51C29C66EFD7FCA3AE68CDEFD38A4A89BF48220B,
	U3CAnimatePropertiesU3Ed__6__ctor_m2B0F8A634812D7FE998DD35188C5F07797E4FB0D,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mCF53541AABFDC14249868837689AC287470F4E71,
	U3CAnimatePropertiesU3Ed__6_MoveNext_mB9586A9B61959C3BC38EFB8FC83109785F93F6AC,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A34F7423FA726A91524CBA0CDD2A25E4AF8EE95,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m1C76BF8EAC2CDC2BAC58755622763B9318DA51CA,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m289720A67EB6696F350EAC41DAAE3B917031B7EA,
	SimpleScript_Start_mC4159EF79F863FBD86AEA2B81D86FDF04834A6F8,
	SimpleScript_Update_mBD8A31D53D01FEBB9B432077599239AC6A5DEAFE,
	SimpleScript__ctor_mC91E912195EEE18292A8FCA7650739E3DDB81807,
	SkewTextExample_Awake_m2D48E0903620C2D870D5176FCFD12A8989801C93,
	SkewTextExample_Start_m7577B96B07C4EB0666BF6F028074176258009690,
	SkewTextExample_CopyAnimationCurve_mD2C2C4CA7AFBAAC9F4B04CB2896DB9B32B015ACB,
	SkewTextExample_WarpText_m462DE1568957770D72704E93D2461D8371C0D362,
	SkewTextExample__ctor_m711325FB390A6DFA994B6ADF746C9EBF846A0A22,
	U3CWarpTextU3Ed__7__ctor_m39944C7E44F317ACDEC971C8FF2DEC8EA1CCC1C2,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m54C900BFB8433103FA97A4E50B2C941D431B5A51,
	U3CWarpTextU3Ed__7_MoveNext_m50CEEC92FE0C83768B366E9F9B5B1C9DEF85928E,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CB1783D2DD0399E051969089A36819EDC66FCB,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mB6C5974E8F57160AE544E1D2FD44621EEF3ACAB5,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m5BDAFBB20F42A6E9EC65B6A2365F5AD98F42A1C5,
	TeleType_Awake_m8D56A3C1E06AD96B35B88C3AA8C61FB2A03E627D,
	TeleType_Start_m3BFE1E2B1BB5ED247DED9DBEF293FCCBD63760C6,
	TeleType__ctor_m824BBE09CC217EB037FFB36756726A9C946526D0,
	U3CStartU3Ed__4__ctor_m7CB9C7DF4657B7B70F6ED6EEB00C0F422D8B0CAA,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mA57DA4D469190B581B5DCB406E9FB70DD33511F2,
	U3CStartU3Ed__4_MoveNext_mE1C3343B7258BAADC74C1A060E71C28951D39D45,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1819CF068B92E7EA9EEFD7F93CA316F38DF644BA,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9B7AEE80C1E70D2D2FF5811A54AFD6189CD7F5A9,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m5C22C5D235424F0613697F05E72ADB4D1A3420C8,
	TextConsoleSimulator_Awake_m55D28DC1F590D98621B0284B53C8A22D07CD3F7C,
	TextConsoleSimulator_Start_m5667F64AE1F48EBA2FF1B3D2D53E2AFCAB738B39,
	TextConsoleSimulator_OnEnable_mDF58D349E4D62866410AAA376BE5BBAE4153FF95,
	TextConsoleSimulator_OnDisable_m4B3A741D6C5279590453148419B422E8D7314689,
	TextConsoleSimulator_ON_TEXT_CHANGED_m050ECF4852B6A82000133662D6502577DFD57C3A,
	TextConsoleSimulator_RevealCharacters_mAA4D3653F05692839313CE180250A44378024E52,
	TextConsoleSimulator_RevealWords_m0E52802FD4239665709F086E6E0B235CDE67E9B1,
	TextConsoleSimulator__ctor_mBDDE8A2DCED8B140D78D5FE560897665753AB025,
	U3CRevealCharactersU3Ed__7__ctor_m40A144070AB46560F2B3919EA5CB8BD51F8DDF45,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m7942532282ACF3B429FAD926284352907FFE087B,
	U3CRevealCharactersU3Ed__7_MoveNext_m2D07AF9391894BCE39624FA2DCFA87AC6F8119AE,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m754C680B2751A9F05DBF253431A3CB42885F7854,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mD12057609EFCBCA8E7B61B0421D4A7C5A206C8C3,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9FD7DAB922AE6A58166112C295ABFF6E19E1D186,
	U3CRevealWordsU3Ed__8__ctor_mDF8D4C69F022D088AFC0E109FC0DBE0C9B938CAC,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m2F2F21F38D2DD8AE3D066E64850D404497A131C5,
	U3CRevealWordsU3Ed__8_MoveNext_mC5102728A86DCB2171E54CFEDFA7BE6F29AB355C,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D9A6269831C00345D245D0EED2E5FC20BBF4683,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mE5E0678716735BDF0D632FE43E392981E75A1C4D,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m3E9D4960A972BD7601F6454E6F9A614AA21D553E,
	TextMeshProFloatingText_Awake_m600F1825C26BB683047156FD815AE4376D2672F2,
	TextMeshProFloatingText_Start_m8121246A4310A0014ECA36144B9DCE093FE8AE49,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_mA1E370089458CD380E9BA7740C2BC2032F084148,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_mA02B20CF33E43FE99FD5F1B90F7F350262F0BEBE,
	TextMeshProFloatingText__ctor_mD08AF0FB6944A51BC6EA15D6BE4E33AA4A916E3E,
	TextMeshProFloatingText__cctor_m352D68463FC9600F9139AD78F0176368562F63C6,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_mD3C24C6814482113FD231827E550FBBCC91424A0,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m83285E807FA4462B99B68D1EB12B2360238C53EB,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m588E025C05E03684A11ABC91B50734A349D28CC8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2412DC176F8CA3096658EB0E27AC28218DAEC03A,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mCCE19093B7355F3E23834E27A8517661DF833797,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mE53E0B4DBE6AF5DAC110C3F626B34C5965845E54,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m1ECB51A93EE3B236301948784A3260FD72814923,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m461761745A9C5FF4F7995C3DB33DB43848AEB05B,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m1FC162511DF31A9CDBD0101083FBCB11380554C4,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A5E330ACDAD25422A7D642301F58E6C1EE1B041,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_m5A7148435B35A0A84329416FF765D45F6AA0F4E1,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m066140B8D4CD5DE3527A3A05183AE89B487B5D55,
	TextMeshSpawner_Awake_m9A84A570D2582918A6B1287139527E9AB2CF088D,
	TextMeshSpawner_Start_m3EE98071CA27A18904B859A0A6B215BDFEB50A66,
	TextMeshSpawner__ctor_m8409A62C31C4A6B6CEC2F48F1DC9777460C28233,
	TMPro_InstructionOverlay_Awake_m0F92D44F62A9AC086DE3DF1E4C7BFAF645EE7084,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m3CC1B812C740BAE87C6B5CA94DC64E6131F42A7C,
	TMPro_InstructionOverlay__ctor_m247258528E488171765F77A9A3C6B7E079E64839,
	TMP_ExampleScript_01_Awake_m6E620605AE9CCC3789A2D5CFD841E5DAB8592063,
	TMP_ExampleScript_01_Update_m3D4A9AB04728F0ABD4C7C8A462E2C811308D97A1,
	TMP_ExampleScript_01__ctor_m43F9206FDB1606CD28F1A441188E777546CFEA2A,
	TMP_FrameRateCounter_Awake_m99156EF53E5848DE83107BFAC803C33DC964265C,
	TMP_FrameRateCounter_Start_m9B5D0A86D174DA019F3EB5C6E9BD54634B2F909A,
	TMP_FrameRateCounter_Update_m5251EE9AC9DCB99D0871EE83624C8A9012E6A079,
	TMP_FrameRateCounter_Set_FrameCounter_Position_m1CC40A8236B2161050D19C4B2EBFF34B96645723,
	TMP_FrameRateCounter__ctor_mD8804AE37CED37A01DF943624D3C2C48FBC9AE43,
	TMP_TextEventCheck_OnEnable_mABF0C00DDBB37230534C49AD9CA342D96757AA3E,
	TMP_TextEventCheck_OnDisable_m4AE76C19CBF131CB80B73A7C71378CA063CFC4C6,
	TMP_TextEventCheck_OnCharacterSelection_mB421E2CFB617397137CF1AE9CC2F49E46EB3F0AE,
	TMP_TextEventCheck_OnSpriteSelection_mD88D899DE3321CC15502BB1174709BE290AB6215,
	TMP_TextEventCheck_OnWordSelection_m180B102DAED1F3313F2F4BB6CF588FF96C8CAB79,
	TMP_TextEventCheck_OnLineSelection_mE0538FFAFE04A286F937907D0E4664338DCF1559,
	TMP_TextEventCheck_OnLinkSelection_m72BF9241651D44805590F1DBADF2FD864D209779,
	TMP_TextEventCheck__ctor_m8F6CDB8774BDF6C6B909919393AC0290BA2BB0AF,
	TMP_TextInfoDebugTool__ctor_m54C6EE99B1DC2B4DE1F8E870974B3B41B970C37E,
	TMP_TextSelector_A_Awake_m662ED2E3CDB7AE16174109344A01A50AF3C44797,
	TMP_TextSelector_A_LateUpdate_m1A711EC87962C6C5A7157414CD059D984D3BD55B,
	TMP_TextSelector_A_OnPointerEnter_m747F05CBEF90BF713BF726E47CA37DC86D9B439A,
	TMP_TextSelector_A_OnPointerExit_m5D7D8A07591506FB7291E84A951AB5C43DAA5503,
	TMP_TextSelector_A__ctor_m4C56A438A3140D5CF9C7AFB8466E11142F4FA3BE,
	TMP_TextSelector_B_Awake_m773D4C87E67823272DBF597B9CADE82DD3BFFD87,
	TMP_TextSelector_B_OnEnable_m8DA695DB0913F7123C4ADAFD5BEAB4424FA5861B,
	TMP_TextSelector_B_OnDisable_mF2EF7AE0E015218AB77936BD5FD6863F7788F11D,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m5B53EF1608E98B6A56AAA386085A3216B35A51EE,
	TMP_TextSelector_B_LateUpdate_mE1B3969D788695E37240927FC6B1827CC6DD5EFF,
	TMP_TextSelector_B_OnPointerEnter_mBAF5711E20E579D21258BD4040454A64E1134D98,
	TMP_TextSelector_B_OnPointerExit_m40ED8F7E47FF6FD8B38BE96B2216267F61509D65,
	TMP_TextSelector_B_OnPointerClick_m773B56D918B1D0F73C5ABC0EB22FD34D39AFBB97,
	TMP_TextSelector_B_OnPointerUp_mF409D728900872CC323B18DDA7F91265058BE772,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m1FD258EC7A53C8E1ECB18EB6FFEFC6239780C398,
	TMP_TextSelector_B__ctor_mB45DD6360094ADBEF5E8020E8C62404B7E45E301,
	TMP_UiFrameRateCounter_Awake_m3E0ECAD08FA25B61DD75F4D36EC3F1DE5A22A491,
	TMP_UiFrameRateCounter_Start_m11EF02C330E5D834C41F009CF088A3150352567F,
	TMP_UiFrameRateCounter_Update_m568E467033B0FF7C67251895A0772CFA197789A3,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_mAF25D6E90A6CB17EE041885B32579A2AEDBFCC36,
	TMP_UiFrameRateCounter__ctor_mBF5305427799EBC515580C2747FE604A6DFEC848,
	VertexColorCycler_Awake_m8895A9C06DB3EC4379334601DC726F1AFAF543C1,
	VertexColorCycler_Start_m36846DA72BFC7FDFA944A368C9DB62D17A15917B,
	VertexColorCycler_AnimateVertexColors_m16733B3DFF4C0F625AA66B5DF9D3B04D723E49CC,
	VertexColorCycler__ctor_m673CA077DC5E935BABCEA79E5E70116E9934F4C1,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0245999D5FAAF8855583609DB16CAF48E9450262,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_mF965F484C619EFA1359F7DB6495C1C79A89001BF,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m5C44B8CC0AB09A205BB1649931D2AC7C6F016E60,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9600944C968C16121129C479F8B25D8E8B7FDD1,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m319AC50F2DE1572FB7D7AF4F5F65958D01477899,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_mC19EC9CE0C245B49D987C18357571FF3462F1D2C,
	VertexJitter_Awake_m0DF2AC9C728A15EEB427F1FE2426E3C31FBA544C,
	VertexJitter_OnEnable_mCD5C1FDDBA809B04AC6F6CB00562D0AA45BC4354,
	VertexJitter_OnDisable_mB670406B3982BFC44CB6BB05A73F1BE877FDFAF2,
	VertexJitter_Start_mDE6155803CF2B1E6CE0EBAE8DF7DB93601E1DD76,
	VertexJitter_ON_TEXT_CHANGED_m0CF9C49A1033B4475C04A417440F39490FED64A8,
	VertexJitter_AnimateVertexColors_m2A69F06CF58FA46B689BD4166DEF5AD15FA2FA88,
	VertexJitter__ctor_m41E4682405B3C0B19779BA8CB77156D65D64716D,
	U3CAnimateVertexColorsU3Ed__11__ctor_m10C4D98A634474BAA883419ED308835B7D91C01A,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mB3756FBFDD731F3CC1EFF9AB132FF5075C8411F8,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mD694A3145B54B9C5EB351853752B9292DBFF0273,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79C3A529011A51B9A994106D3C1271548B02D405,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m15291DCCCEC264095634B26DD6F24D52360BDAF0,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m0B8F21A4589C68BA16A8340938BB44C980260CC9,
	VertexShakeA_Awake_m092957B0A67A153E7CD56A75A438087DE4806867,
	VertexShakeA_OnEnable_m52E2A036C9EB2C1D633BA7F43E31C36983972304,
	VertexShakeA_OnDisable_m52F58AF9438377D222543AA67CFF7B30FCCB0F23,
	VertexShakeA_Start_mDD8B5538BDFBC2BA242B997B879E7ED64ACAFC5E,
	VertexShakeA_ON_TEXT_CHANGED_mE7A41CEFDB0008A1CD15F156EFEE1C895A92EE77,
	VertexShakeA_AnimateVertexColors_m5FD933D6BF976B64FC0B80614DE5112377D1DC38,
	VertexShakeA__ctor_m63ED483A292CA310B90144E0779C0472AAC22CBB,
	U3CAnimateVertexColorsU3Ed__11__ctor_m440985E6DF2F1B461E2964101EA242FFD472A25A,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m74112773E1FD645722BC221FA5256331C068EAE7,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mA6858F6CA14AAE3DFB7EA13748E10E063BBAB934,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DD4F3768C9025EFAC0BFDBB942FEF7953FB20BE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m2F84864A089CBA0B878B7AC1EA39A49B82682A90,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m3106DAC17EF56701CBC9812DD031932B04BB730B,
	VertexShakeB_Awake_mFA9A180BD1769CC79E6325314B5652D605ABE58E,
	VertexShakeB_OnEnable_m4999DF4598174EDA2A47F4F667B5CE061DF97C21,
	VertexShakeB_OnDisable_m2FB32CBD277A271400BF8AF2A35294C09FE9B8E5,
	VertexShakeB_Start_m58786A0944340EF16E024ADB596C9AB5686C2AF1,
	VertexShakeB_ON_TEXT_CHANGED_mF8641640C828A9664AE03AF01CB4832E14EF436D,
	VertexShakeB_AnimateVertexColors_m06D25FE7F9F3EFF693DDC889BF725F01D0CF2A6F,
	VertexShakeB__ctor_m9D068774503CF8642CC0BAC0E909ECE91E4E2198,
	U3CAnimateVertexColorsU3Ed__10__ctor_mBE5C0E4A0F65F07A7510D171683AD319F76E6C6D,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m4DD41FA568ABBC327FA38C0E345EFB6F1A71C2C8,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_mDD84A4116FCAAF920F86BA72F890CE0BE76AF348,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m250CC96EC17E74D79536FDA4EB6F5B5F985C0845,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5A5869FEFA67D5E9659F1145B83581D954550C1A,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m496F1BFEADA21FFB684F8C1996EAB707CFA1C5F0,
	VertexZoom_Awake_m29C1DE789B968D726EDD69F605321A223D47C1A0,
	VertexZoom_OnEnable_mE3719F01B6A8590066988F425F8A63103B5A7B47,
	VertexZoom_OnDisable_mBB91C9EFA049395743D27358A427BB2B05850B47,
	VertexZoom_Start_mB03D03148C98EBC9117D69510D24F21978546FCB,
	VertexZoom_ON_TEXT_CHANGED_mFF049D0455A7DD19D6BDACBEEB737B4AAE32DDA7,
	VertexZoom_AnimateVertexColors_m632BD9DC8FB193AF2D5B540524B11AF139FDF5F0,
	VertexZoom__ctor_m454AF80ACB5C555BCB4B5E658A22B5A4FCC39422,
	U3CU3Ec__DisplayClass10_0__ctor_m8C69A89B34AA3D16243E69F1E0015856C791CC8A,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m8E51A05E012CCFA439DCF10A8B5C4FA196E4344A,
	U3CAnimateVertexColorsU3Ed__10__ctor_m7A5B8E07B89E628DB7119F7F61311165A2DBC4D6,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m974E92A444C6343E94C76BB6CC6508F7AE4FD36E,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m6DBC52A95A92A54A1801DC4CEE548FA568251D5E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m110CD16E89E725B1484D24FFB1753768F78A988B,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDE5E71C88F5096FD70EB061287ADF0B847732821,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m14B89756695EE73AEBB6F3A613F65E1343A8CC2C,
	WarpTextExample_Awake_m92842E51B4DBB2E4341ACB179468049FAB23949F,
	WarpTextExample_Start_m3339EDC03B6FC498916520CBCCDB5F9FA090F809,
	WarpTextExample_CopyAnimationCurve_m65A93388CC2CF58CD2E08CC8EF682A2C97C558FF,
	WarpTextExample_WarpText_mBE4B6E5B6D8AAE9340CD59B1FA9DFE9A34665E98,
	WarpTextExample__ctor_mBD48A5403123F25C45B5E60C19E1EA397FBA1795,
	U3CWarpTextU3Ed__8__ctor_m1943C34BBEAF121203BA8C5D725E991283A4A3BB,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m145D2DA1026419984AD79D5D62FBC38C9441AB53,
	U3CWarpTextU3Ed__8_MoveNext_mCE7A826C5E4854C2C509C77BD18F5A9B6D691B02,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD80368E9B7E259311C03E406B75161ED6F7618E3,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m07746C332D2D8CE5DEA59873C26F2FAD4B369B42,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m71D7F84D9DEF63BEC6B44866515DDCF35B142A19,
	NULL,
	JSONNode_get_Item_m77F15891BEC7ED659BFBC392555178B558747AD8,
	JSONNode_set_Item_mC6F47073D979B943286B2EAB1A6D0380AFE58A09,
	JSONNode_get_Item_m466B08DF2E30B20606697EC7AE043C2791DC6768,
	JSONNode_set_Item_m045530804B67FC5E2E57E497219F27ED70CE437E,
	JSONNode_get_Value_m2A9961ACC3D4BCBB028012CD79B619DCBD82A839,
	JSONNode_set_Value_mE8CD0E68E0E2B0A716F56B0FE9B988EC2BAD773A,
	JSONNode_get_Count_m260DDA50B8AFB98F5946E54B9EADD05891A82C8B,
	JSONNode_get_IsNumber_m6B495FE576572E9FC7999740C63980BCB65AD768,
	JSONNode_get_IsString_mBDE2CAF25E51CDA450074BE9DC81D834903BA392,
	JSONNode_get_IsBoolean_m13F16853C0F6D76D0AB6B7E866923A0632C108A2,
	JSONNode_get_IsNull_m6443A7B3540D725ED3ACA0038A74CE0346A31F8D,
	JSONNode_get_IsArray_m52DCDB47E4CB2673FDCECCD3BE9DD2D90B5C948F,
	JSONNode_get_IsObject_m237FE2EA3382DD9762ED426B49F46183F5EF39AB,
	JSONNode_get_Inline_m7A5B6C07F44EFEEDD80FD72580C32C0579041F4C,
	JSONNode_set_Inline_m18362F10F03DDCD1FF29B4868C3EA793D39AE7F6,
	JSONNode_Add_mB05F1A32B54A9A1223F9AC6A6A737836FA1F4E7E,
	JSONNode_Add_mDAF96580EAF3B9FF23888A8549BED7A98439075D,
	JSONNode_Remove_mF56C4223700DF4F1D5AE12BCD69C492C2487FA59,
	JSONNode_Remove_m7B5E0BC0A29C35857D7B10857A8C52C0E3DFB615,
	JSONNode_Remove_mE2CFD05512C25BD11EA4160CAAF88B8154D9DBE5,
	JSONNode_get_Children_m3E2D70DBCA2C8311F65A47B766668728392B1F89,
	JSONNode_get_DeepChildren_m891CB892AEA834980686ED760B952A86DC1E8725,
	JSONNode_ToString_m4CC464630B0AEEDD82AEB6B069690949AF569345,
	JSONNode_ToString_m1F607CB90F49115510B7CF5228733578E9AD41F2,
	NULL,
	NULL,
	JSONNode_get_Linq_m8569DB478533504290D9A09ECA0DF12F116122DA,
	JSONNode_get_Keys_mC3401CC91BBD9D1166EF8EFC0C87A820FC543D1B,
	JSONNode_get_Values_mF8FB164A48A169146D00EBA3F51D4C8E380C1930,
	JSONNode_get_AsDouble_m9A8E3EC46E4545BCBFA26B99C0F013067D2F0AE4,
	JSONNode_set_AsDouble_mCDBB05BD0AE82EEF0C4842F5A9205B8F4C858015,
	JSONNode_get_AsInt_mE4A3FCC1D91362D077C2ACF418ACAB43771B1FE6,
	JSONNode_set_AsInt_m12FCF0B7E45E17EA0456AE44EFEF0C8731603F50,
	JSONNode_get_AsFloat_m0D044C1F3FC35086783A4BAF506EA96DC997D050,
	JSONNode_set_AsFloat_m55FCE24DF60B37724DACCCF0A759522B2561DE92,
	JSONNode_get_AsBool_m902380F5939671ACBBB7EFA01A48F1A082B1FD9C,
	JSONNode_set_AsBool_m6097FD196A8C7BB156125363D1C1D3EF0EB67CD3,
	JSONNode_get_AsLong_m31250905C6F4BED9B1059009E064D07D609C4C18,
	JSONNode_set_AsLong_m8D29780DEA1458A2F9C33805432DB1554950ECF4,
	JSONNode_get_AsArray_m2D0890FDDA140528CAB44B1B6B3E34B26383ACC7,
	JSONNode_get_AsObject_m72F6D406BECA2FB0A24B20E0A353FDB8E409CA1B,
	JSONNode_op_Implicit_mA884A397C3152BDB411767FDE9EDC274A8904523,
	JSONNode_op_Implicit_m31D5D161C5F21186F90F99E09EC78CED5188C349,
	JSONNode_op_Implicit_m35AB6AB8F232F07E3226DEB06AB832F5C2BD7A8A,
	JSONNode_op_Implicit_m0039518B4032423C4CDC8C8B07790AE1779E8E25,
	JSONNode_op_Implicit_m6ECC1712EB162E1AC774A36E31AF31A1FCC5E659,
	JSONNode_op_Implicit_m8312FC9381D54EBF755C7B41D525C6EEAD0EC135,
	JSONNode_op_Implicit_mD0756B05030DAE5F5D4A3E57708D5EEEF6A362BB,
	JSONNode_op_Implicit_mE793F9163050BD78846F2C2A288A968E8F9C11AD,
	JSONNode_op_Implicit_mE052DB6EC3A2453BBDE1511230CEF2B3AC21211E,
	JSONNode_op_Implicit_mEA60633B7DA23EFD6583A825ED6A52BBFB327EEE,
	JSONNode_op_Implicit_m2BD30AE4D30506EF634EA264A90E018FF66FF1E4,
	JSONNode_op_Implicit_m057A1C72683211B58678B1448C260F747434DF40,
	JSONNode_op_Implicit_m5D106035AB428BDA4B6671950E8E5747771CBD7F,
	JSONNode_op_Equality_m6961ED452D3A120FE9908FFB96260DF98A47A8B3,
	JSONNode_op_Inequality_m2DF69DE99CD87AA07CE1200892E24EA22B351943,
	JSONNode_Equals_mE1B8A846783529B1E54786975A6A2396089A88DE,
	JSONNode_GetHashCode_m0A263555D1F0E6766A61692A7E1BC3546B2BC984,
	JSONNode_get_EscapeBuilder_m351C10540498042FC3ED424E3D9C08DCB16071FF,
	JSONNode_Escape_mC20A370D25C7B269E4707FF5CEC7062C470C416A,
	JSONNode_ParseElement_m978282020EF903D5D8F2F2B7B05074A5D1C3F2EF,
	JSONNode_Parse_m51FFFB4953A8D875B9D2DD5E032D131A149956E0,
	JSONNode__ctor_mF8F2893483161D3B7B9877B63C69063D26A5C353,
	Enumerator_get_IsValid_mBC273331DC1699FF46BD3621AE5059A54AD98BA6,
	Enumerator__ctor_mF21239C69620D815F8CD34F022BE18E9DAF9CB10,
	Enumerator__ctor_mAC4ED0FA4B083E2652E865A41EA5C74A49478EFE,
	Enumerator_get_Current_mDE6750203413E1069D0520793D6AA0B2527CB20E,
	Enumerator_MoveNext_m238CF072385A1106BEDEFCE33BA2B0DBE999758A,
	ValueEnumerator__ctor_mCC61CE3EDCF1AC94A84E031F2E89F8054C94A015,
	ValueEnumerator__ctor_m122732DF448B45E8E82956E07AC8314C60E28C29,
	ValueEnumerator__ctor_m7BA4BAD5FEBAC4054F71575B728DC27EC4080F0A,
	ValueEnumerator_get_Current_mAA24A52FDEB7160BD268193175388EACB41B7CE2,
	ValueEnumerator_MoveNext_m5B596A2EF2FF395EDA8F5CAB97C0789498D250C9,
	ValueEnumerator_GetEnumerator_m765261287A2C0AEF757B94994826F43951387E4C,
	KeyEnumerator__ctor_m6EA81E2BED4CA5194A7306D8B324F7356E37F80A,
	KeyEnumerator__ctor_mA6338E82A9F8AA19A1744352B4FE54103AD70405,
	KeyEnumerator__ctor_m526EA1364C367B83C931F4208CDD816BD02810EA,
	KeyEnumerator_get_Current_m8FBFEE52D4438AAF3E10AB4370B34FBB8E66B3C2,
	KeyEnumerator_MoveNext_m42FE2CEE808A7E065895BA333B7FBD2F3AEE032F,
	KeyEnumerator_GetEnumerator_mD4687B4D6D10E4D6870CBBECC680689A62A95C0B,
	LinqEnumerator__ctor_m9FD8AB1580F3D94C5C36D070DBE85E023ED36E30,
	LinqEnumerator_get_Current_m28F0BE4D9B5736F5BD79197C1895EAC1592EBAAF,
	LinqEnumerator_System_Collections_IEnumerator_get_Current_m6B6C12C7E8CD21DF513FCDCB4E88E454790B6FF0,
	LinqEnumerator_MoveNext_mCA8604B6E8D857CF16003E674048C05E29447819,
	LinqEnumerator_Dispose_m5D6A54C4B712D138739726323D5BEA50A4E12E32,
	LinqEnumerator_GetEnumerator_m4A9F0720F0C0964F91032AB8B8776F09DC70A90B,
	LinqEnumerator_Reset_m56B65E398518EF57070307FDC48069DFE37BC57B,
	LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_mB63F02D713868ABF87DAB18ABFD5D832F4D805A4,
	U3Cget_ChildrenU3Ed__40__ctor_mED3CB3E682749015A8491DF12C31487745A4248B,
	U3Cget_ChildrenU3Ed__40_System_IDisposable_Dispose_m278BD93CDDCDA8A33C819FE8B1B7BE4FEE90D06B,
	U3Cget_ChildrenU3Ed__40_MoveNext_mD6AE82A70820FAF1AFEC7896955955E990067CB8,
	U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m75DE45763814DF424576E7C517E1D7EFDBAD9F45,
	U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_Reset_m5375DEDEB65015D0634741057877D504BF18526F,
	U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_get_Current_m05E8F0D8A29155DCA5EA0D8B6ABA68AF7236F9C6,
	U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m2F120D821A756BC0938593FF8F6184A49710DFA3,
	U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerable_GetEnumerator_mB1CE5E0B30277A97EA1C6D3E337523DEA5B11757,
	U3Cget_DeepChildrenU3Ed__42__ctor_mB98C64BA2B81334A1830986688EA632F32DC2EDB,
	U3Cget_DeepChildrenU3Ed__42_System_IDisposable_Dispose_mA6F2AF3124A2E4A2DCB86C54AF4E31F08EA8AF3F,
	U3Cget_DeepChildrenU3Ed__42_MoveNext_m6568ACC95807B31FA819569F541B6EDAAD92F579,
	U3Cget_DeepChildrenU3Ed__42_U3CU3Em__Finally1_mFB16A1F4090D60071A16E8ED7E86DC487305AF73,
	U3Cget_DeepChildrenU3Ed__42_U3CU3Em__Finally2_m9B19E684C42ABCBC8476BDD436F5EB28D9C731AC,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m927BE2DB8EF13D247334ACBD9AD5F9D7312E13DC,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_Reset_m3B21F82F9CF8A35C11BD0D4B635E29F13507C5C3,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_get_Current_mF0F87CE545AB2B74799D13F35D5007A8C95F0CB3,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF23F7F01784F070DC30E80C7B2525BB08F1F729F,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerable_GetEnumerator_m60B68168D69C58711A46585D0A262C6CFA0C8B5F,
	JSONArray_get_Inline_mBA0C9AEBB7420DBDFD977C0F54CC237E8F2BE3E5,
	JSONArray_set_Inline_m731089F5D0FA649ED210518DC299635A8D86A1DC,
	JSONArray_get_Tag_m360EB078D7897D6D52783B8CDA6B736D014E97BC,
	JSONArray_get_IsArray_mA7B4EF5B0128FB64ACEB7EAC66FA3522991980AF,
	JSONArray_GetEnumerator_m6AF64AE0DD2A5AAB8C0E271BF0CAB8AA1FD32E17,
	JSONArray_get_Item_m8BE9047FC512840E6A4594560EDF86BB4E0FF657,
	JSONArray_set_Item_mBCD05590C34BC589B786E753B9FE796EBA3F6725,
	JSONArray_get_Item_mE18312128B02B505BA656D7F444B05A6769710AE,
	JSONArray_set_Item_mE4E0DE5133E60AF49E46FEDAD00D2A04349C0855,
	JSONArray_get_Count_mB71218A2D8288D0665C467844F7351D301FDAFDD,
	JSONArray_Add_mD1FBE0F0FC20E7415014B7FF21939592EBB0C9A1,
	JSONArray_Remove_m79500DBD9751A04C02756470A4D22DDCF9C97FEC,
	JSONArray_Remove_m64C3EBFE3DB5BE130232769DC43000E84589E674,
	JSONArray_get_Children_m733AE4C5816E51E6F86441110606489A0406AA91,
	JSONArray_WriteToStringBuilder_m9F23115433028794DCAC019F82EEFD946990D994,
	JSONArray__ctor_m92FFF2DC8E1425398814F50D4B253EB459B8477F,
	U3Cget_ChildrenU3Ed__22__ctor_m85868694A6F76D6658184E05C60FF81F01F77A15,
	U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_m2F364B8DD4640833161EE743D458FAF77BCAED9F,
	U3Cget_ChildrenU3Ed__22_MoveNext_mD1A93CD03B10AD9224ACD34AA4478BA4EB448AE5,
	U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_mE8C769C27FDCFEA9BDE25D788AE87EC9565F4C5F,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m9FB6C47800DA9AC5BD99118430A954370D30FA70,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_m5FDF47F215C3ECA0B88DBDBA67D9E23939FB6E75,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_mB7369D4C4D04908F6F69E55E0FDF6835418E9671,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mE9E5D91FED8686D7D44613FE3F2E5A67765D924E,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m64A7C394D254404FA1EE6C54C42CB33064B85573,
	JSONObject_get_Inline_mCDF2154366BEFF9E547918F999E7F3C7C4865F84,
	JSONObject_set_Inline_m7F048A7565E5A53FDB610D44B7CA75A314CB7A7A,
	JSONObject_get_Tag_mD57D6BCAD1C677B88693FD508129CFAD661F4FBD,
	JSONObject_get_IsObject_m9F72861BE5A0DB2888AA3CBEC82718E08DD71E93,
	JSONObject_GetEnumerator_m8912E3D1EA302655BB5701B53EB19437238BABDA,
	JSONObject_get_Item_m219B9BA37D800A5DFEAA14E4EECA375B3565BF96,
	JSONObject_set_Item_m1AC7334DBA67D0CB6C9549B83B3FFA75CF226AEF,
	JSONObject_get_Item_m5C2EDBE7B154A3FC1CC43616C4C40255B4D95652,
	JSONObject_set_Item_mFB6E61E3FA394B7D2CA01CC957A6A253642D109B,
	JSONObject_get_Count_m9109E9A81559A9006EE160CA6A0F3291C71F2D08,
	JSONObject_Add_m25BD208A0AC0F0223FD93FBCB42785B12A6E1A18,
	JSONObject_Remove_m34280FDB4512E61F42781475E492BE98514830C9,
	JSONObject_Remove_mD1B01E22A9C1FEE83A00ECDFD8E0D8A422F8E4C2,
	JSONObject_Remove_m51B998A7997D184A1A20359D512C6B5A1B825404,
	JSONObject_get_Children_m03D7227DE57F0BE2977FC0436C0DE48858650B7C,
	JSONObject_WriteToStringBuilder_m931DC8805C6B8F09617958EFDAEA957751EB2EAE,
	JSONObject__ctor_m8007967452F5257DC9F5DF2B78B411BFD4B6D6AB,
	U3CU3Ec__DisplayClass21_0__ctor_m6976B4CF7F93E28364B390F81E55DAD60BB141C1,
	U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m8B35D441B276B749481FF797FC51A256A7A56105,
	U3Cget_ChildrenU3Ed__23__ctor_mD7DD2FB8F14148B45EC4AC3A595DC1AFE369FC99,
	U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m85C7654E92BD35A4525D4F3E70FFD57B5CD497AF,
	U3Cget_ChildrenU3Ed__23_MoveNext_mB9E90BB01F42BA8913E7CD7AEA18FBEADFC46CC0,
	U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m0FEB12CB9C8CC84E25A44F9FC928D8EB8F4DF9DD,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mFB2B5F5F93F044ADBF11AF1C59D305BFE295D063,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_mE65CCEC3E7FCE86596AF14C5821DA3D5F76C34E3,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m3B0D1EE4EF3C849E2A702C9B9DB6F65FA70890D9,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m42B5EADBEE1E083675385176B87BCAB7C4FA0873,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_mC4F14551458D7F9F3DDC7610F078982D1C04DBD4,
	JSONString_get_Tag_m68B0FF9ADDC3E203E5D60BB10639AEABACA34D44,
	JSONString_get_IsString_m933985E37AE8A887A2039A9BAC7698F083BCD6E3,
	JSONString_GetEnumerator_m1CB9E437FC8622F3FE05D0AC12024D144747E0B8,
	JSONString_get_Value_mEAD2BD372A2C517E83233BA5F6E309745AA5E9B4,
	JSONString_set_Value_mB974D9B82AB8F9FAB84DCA99B8BD4B7C1C08ED00,
	JSONString__ctor_m1DD5FB9A4147F72A0ED5F773FF82FA269241AD19,
	JSONString_WriteToStringBuilder_mDF24D860FBF8E71F6F04799DD70F7700CE41D818,
	JSONString_Equals_m1C60B537E558E6DF85ACF3EF9FF43BF9A3CF5435,
	JSONString_GetHashCode_m979A74F84B4C0F45BF63D75DE1146490F743EE00,
	JSONNumber_get_Tag_m7C6E217E85B6161812496B63E5D371B910AAC856,
	JSONNumber_get_IsNumber_mFABFD0C9C4905CFB34A62700A1BD335F53E4214E,
	JSONNumber_GetEnumerator_m4D13E84756AEED9FCD7EFEEE4D01187DD049C596,
	JSONNumber_get_Value_mBC5AB046D134B1E54C228C9C1C2231F8448CD56D,
	JSONNumber_set_Value_m2264762BBD76F39DDC5DF3160910A44FBEFDE54C,
	JSONNumber_get_AsDouble_m8C004121700A7E7EB2B77ED223187227E33DE60B,
	JSONNumber_set_AsDouble_m8E17AF8C0E9AE0EF6E25D86CB1B119904ADC0558,
	JSONNumber_get_AsLong_mF96069F806F51121CBFE8847D9E0D312F05986BB,
	JSONNumber_set_AsLong_m541EF4E20CD8683CA860E0B969CECF7B71E2A357,
	JSONNumber__ctor_m1CE3527102D15EBC3A183E3519895E291CAC1D90,
	JSONNumber__ctor_m39FDDE1A9EFEE9C4F2498E531D12B97AA49A1BA5,
	JSONNumber_WriteToStringBuilder_mD311BC3C1EE3E159C43801EB214F084E567367F2,
	JSONNumber_IsNumeric_mE6C07226FABFDD425449643925B667C05C52D41D,
	JSONNumber_Equals_mC04BB811CCAF20E70AE696AE74ECFDF5DA888688,
	JSONNumber_GetHashCode_m976ADFE41037830524798C7E6AFE08006B5F77AD,
	JSONBool_get_Tag_m82CE84C4C89E157D4DB036B9F0745343C005C338,
	JSONBool_get_IsBoolean_m2671AE98710859611DF47E6BC58E6582C3A5B445,
	JSONBool_GetEnumerator_mA07A10A6111713F7AD09FF03D09A6028556094D9,
	JSONBool_get_Value_mBEA89869448B0B597758D5BF2A3B576CA0BB64E3,
	JSONBool_set_Value_mC960EE4083CA91D0059BE24661AFC06E131E2CFC,
	JSONBool_get_AsBool_mE04224144EAD0A9AD2F3B14BC0C68557A3BF22AC,
	JSONBool_set_AsBool_m88EDF61A5ABBFF3ECF723312852E14F3C60AE365,
	JSONBool__ctor_mBB02E388CFB96B99E84561FCFF68147F00391C58,
	JSONBool__ctor_m8CFB6AA78095EA003AB9B5EDD8932E8E0B01A1B9,
	JSONBool_WriteToStringBuilder_m82C70C80863730E8A22EE7A5B099C765F2E1D91E,
	JSONBool_Equals_m2671F40DA8F1128BA1451FE7066515C6E0C50D45,
	JSONBool_GetHashCode_mC5B59375A9EE9978A5ADD1A24ECEE3FC920836DB,
	JSONNull_CreateOrGet_m68ED6000156701E566B1EA9DDC5284299B0C9105,
	JSONNull__ctor_m909243259F39D10FA6FEB176474DEF9C9972D76B,
	JSONNull_get_Tag_m89A7F368EA6269874235F85E43AE82254AAFD41E,
	JSONNull_get_IsNull_m1174212D6379871AC361EF06FA05DD510FC55595,
	JSONNull_GetEnumerator_m16D254C74386D1A0AB2EFD1DE0EAF409C73B7686,
	JSONNull_get_Value_mB15431220D7D0B45CE002A204DF9E070CF78DBE0,
	JSONNull_set_Value_mAF0CD2E912EF772E0892EB4ABB77294F689CF20A,
	JSONNull_get_AsBool_m6F3817CD49ED7CC10C180D31D84ED4B0151C78CE,
	JSONNull_set_AsBool_m5717BC3921B7DE0683E9160B3816628B5CBC663D,
	JSONNull_Equals_m8A39CAD3A41E9584C434B90A1360C62B3E158DE6,
	JSONNull_GetHashCode_m74BE6286F06C6E7D5E35381E8BD27215117D9061,
	JSONNull_WriteToStringBuilder_mB5B78BFA6A4943319926C1B2AE93F68C7B9B5FFD,
	JSONNull__cctor_m49F440C5442212437C3A1CDAF32B864961BE534B,
	JSONLazyCreator_get_Tag_m1CB86FEA25328F1BE9CC01F6D020C9450E9F466E,
	JSONLazyCreator_GetEnumerator_m720BF0642A079A8BD44F6D650CF4D833DEF67757,
	JSONLazyCreator__ctor_m0B3625D19DDD8DBDBB45822FAABCE266FA4EE694,
	JSONLazyCreator__ctor_m02E2D630C60045F25A3AC001B7A17DF2D5D197B4,
	JSONLazyCreator_Set_mEF6EB64379EBE960F050C24D45EDCA4B6D404958,
	JSONLazyCreator_get_Item_m562D16AE7F1F0CACA5ED050B390B63F98EBC77B1,
	JSONLazyCreator_set_Item_m42894F9D00193BC7138C5D451E1B0BBD1BFE1084,
	JSONLazyCreator_get_Item_mF7AE3ADFBE062BF3B83FECCE0EF10F10996DE0CD,
	JSONLazyCreator_set_Item_m0107997E3B3CB75FACD86FB487C5D9416171CBEC,
	JSONLazyCreator_Add_mA8451EE34FEA0205B6BD6527AB46E5926451F49F,
	JSONLazyCreator_Add_mDC69A4E203B73054072D1575EC4CF20D95064F61,
	JSONLazyCreator_op_Equality_m7C4199B28912BE4C1AE6009F94C6FE07776923C5,
	JSONLazyCreator_op_Inequality_m8E4E9C09E420FE4E5A0AB54B63CFAEF2244B5F3B,
	JSONLazyCreator_Equals_m753939907CFDB1548B0DAAB38E4737EF17B50066,
	JSONLazyCreator_GetHashCode_m878E7AFF42AE5C43F4F643B6AEB25662491316F9,
	JSONLazyCreator_get_AsInt_mE1404FBC99CE4E8EF4ABBE0BDF661206BAC2C44D,
	JSONLazyCreator_set_AsInt_m13146E53FD6A2F7573B752BFF079E0AF6A5FAE74,
	JSONLazyCreator_get_AsFloat_m2600D4B0E1179583EFE268070C66EAC11D380E04,
	JSONLazyCreator_set_AsFloat_m9DCF79C70D4ED3728C12B709A6D95A0F0A057DE0,
	JSONLazyCreator_get_AsDouble_m41D6DF89CD7CEC00F36962068EE072D391EC0B38,
	JSONLazyCreator_set_AsDouble_mB7ABE38136DBEDA7CC9AC12A381322D6C49ADED9,
	JSONLazyCreator_get_AsLong_mFBA0000985629FA20509FA45A6A8B751C9CAC2B8,
	JSONLazyCreator_set_AsLong_mBD4640D2F347DEF793A631A44026A03D3D5D73A4,
	JSONLazyCreator_get_AsBool_m7D8AF5879C2C8036916AA6B15E22CB4B80412CF4,
	JSONLazyCreator_set_AsBool_m4DB409DB959182CAA610147A51A2ECDBAFEA6092,
	JSONLazyCreator_get_AsArray_m493C069A3624597885A7B6E00C82E829A84B47C4,
	JSONLazyCreator_get_AsObject_mE01B43B261A6A56F4FCE40AB11F3AAF90B7C292D,
	JSONLazyCreator_WriteToStringBuilder_mC9975859B1C42C9F5E507E604121D10B2FB2D93D,
	JSON_Parse_m9E6F3A67011C765E4352E350D1F400C9A52DC5F6,
	DOTweenModuleAudio_DOFade_m33AC3DD19DD85217123B580C7A2BCF1D5A90E5D4,
	DOTweenModuleAudio_DOPitch_m9F61FEB8419EC9F62B1A60CC36914980430E1456,
	DOTweenModuleAudio_DOSetFloat_mBA2509B5C802E771D824FBF098E131475FE27937,
	DOTweenModuleAudio_DOComplete_mDFE49D8B71D95F8C3EE7568ADE0F0051DBA56323,
	DOTweenModuleAudio_DOKill_mF453317F96B7CA4F9C5344E4DF6CF2FFBCFBE538,
	DOTweenModuleAudio_DOFlip_m7F1F6E6AEA8CFFBE6FABCCAAB2025B2887E17539,
	DOTweenModuleAudio_DOGoto_m24D7C34EFC1DFA1CE96792B8584CF060830CF066,
	DOTweenModuleAudio_DOPause_m125649DA4069CA21A7D0219FC33B35B9A6B0A367,
	DOTweenModuleAudio_DOPlay_mEBD2ADDF0E4DBC462F53F1CA6F0DE8E928F3C94B,
	DOTweenModuleAudio_DOPlayBackwards_m6EBE459898E93DFB0E7FA7E959E88B3016E92080,
	DOTweenModuleAudio_DOPlayForward_mAC90AEF47DBC4B1FC509BE40FEEB9CCC92BDB816,
	DOTweenModuleAudio_DORestart_m044617D37A19DED114558F22E1AB8784194BE514,
	DOTweenModuleAudio_DORewind_mA04F3C6E68A4E888BFFB6F7B27ABAFA201266D0B,
	DOTweenModuleAudio_DOSmoothRewind_m9311EB1FF7A31892537EFC587430EE03566C0810,
	DOTweenModuleAudio_DOTogglePause_m437CA4E9EB383779006AC19D86EBE51D58D67F2D,
	U3CU3Ec__DisplayClass0_0__ctor_m160A4BE74C4A0D2D75CE2C15B1F7E5E69163C973,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m8C4722C8715E07655B52CF10C76179595C3DAF28,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m79B9F6415F81447E5D2F5BE5C0F47E39A8C66038,
	U3CU3Ec__DisplayClass1_0__ctor_m709A9EE2A6B97DD5552A583951859A1C83FA2278,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_m137E279AEB9AE936DFD3C4158CED13A0C6079A37,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_mF3FE1A2CC1F0A9CFD31297F1BE45C12E810DD00D,
	U3CU3Ec__DisplayClass2_0__ctor_mDBF428719391180E42C26EE06A2AC47547F13E99,
	U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__0_m367153ADF53E32C8FC51F280B628B2A2190E64E4,
	U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__1_mBCC8DF43E3C17A45DC0CDD8FBB051F9F938F49A5,
	DOTweenModulePhysics_DOMove_m9218B91AF3CA4CB76EC980F3C19DC290C22DD0D3,
	DOTweenModulePhysics_DOMoveX_mDA0E3727E8E0A9CC8CA18919C55DBB67CC7E7618,
	DOTweenModulePhysics_DOMoveY_m4FA2CA0C0DE64203A16C528F12B7FA9EED027F87,
	DOTweenModulePhysics_DOMoveZ_m4A8887E9003AD195C398DB344BD2400AF5527873,
	DOTweenModulePhysics_DORotate_m34B2316F2E1B4FABB69B2F982D8D9DA5AC074DC6,
	DOTweenModulePhysics_DOLookAt_m35971281812CD6A5EA5B28427A009F2C2E4FE533,
	DOTweenModulePhysics_DOJump_mD421D5659C38733B5E5B6891253257E5C425F47B,
	DOTweenModulePhysics_DOPath_m8DA02562B715A7AA662FF9FE0960A11A61B15CD2,
	DOTweenModulePhysics_DOLocalPath_mA9EFBF2E389456FB671468302622565E9F66F5EE,
	DOTweenModulePhysics_DOPath_m420764C2597301E7048F11600713705AD0DADC1A,
	DOTweenModulePhysics_DOLocalPath_m720A2CA6D647E4D8AAA386F83F4FE96E08C73082,
	U3CU3Ec__DisplayClass0_0__ctor_m6C8A8063B7CCC58E82305177538D7FBF1CE44935,
	U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mB768E968FA1FD2B9AE13877E2692D758B495D013,
	U3CU3Ec__DisplayClass1_0__ctor_m479175628F7D1842EE3DCF05C86FAD7B511ABBCB,
	U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_mE0FB6A6EF157A4BC22104D4F91C164AA574DB227,
	U3CU3Ec__DisplayClass2_0__ctor_mB0ADB1BBDA7BF0DD9C2414079D2012E8D61404BE,
	U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m79746B44C774CACFDAFA83EFCC9E6D6D03AD82D6,
	U3CU3Ec__DisplayClass3_0__ctor_mBAA31984E2A716B3498B2B0D79CA25A892D748D0,
	U3CU3Ec__DisplayClass3_0_U3CDOMoveZU3Eb__0_m11FF22865EE53FAB3B972AC3B34D3BB01526DF77,
	U3CU3Ec__DisplayClass4_0__ctor_m71B8B5E5A7CFD905A28CFCEF9005FDBE70096CBF,
	U3CU3Ec__DisplayClass4_0_U3CDORotateU3Eb__0_m253899ED69AC81AED8238765AD9C3F808B81F9B8,
	U3CU3Ec__DisplayClass5_0__ctor_mE16BED5275B5EEEF36A20B3C783CC4E7B0D580EE,
	U3CU3Ec__DisplayClass5_0_U3CDOLookAtU3Eb__0_m1C50D196B81BA62AC84D966F2515E56293697921,
	U3CU3Ec__DisplayClass6_0__ctor_m6F040CE636E4D727C99FC1DF62BB42721707EC92,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__0_m777E7729EA52B752D4212827ED4DB839B6A1F93D,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__1_mB7EEF6913E4971D9438101C2DBB950BB0877E640,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__2_mC008279D66615921E8D8F95A88349CAAA4F3ACF3,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__3_mA4E1F014B086C15F0D7F66564619553D524535D2,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__4_m7190E82AA00FCA218DE01487303AC2AAB966BA14,
	U3CU3Ec__DisplayClass7_0__ctor_m36AD8AF752829C4A0601828F600EC8D181BB7F9E,
	U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_m424F1A2360CF360DEAEB2AE4B03929D8415D9D85,
	U3CU3Ec__DisplayClass8_0__ctor_m75DF5195919998D6D0FA3C9F4EE2052A1856612B,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_mCAB97E4D7EB96AF59820216D2F20F3B32E9CE914,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m409709C8E90B9323B6467D70BF2E623079B9C8F8,
	U3CU3Ec__DisplayClass9_0__ctor_m8B38DEC85AB1D858ED6289CA714E5A7DC6384424,
	U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_m57E3DDA730AB3A0A591C16CA8F9FEFD864FBD23B,
	U3CU3Ec__DisplayClass10_0__ctor_mEF982F61A1DB0156415F649C9BEA4F7D6BBB08AD,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m330A66B432859CDB4EFBABCA77577716AD33B665,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mF002FDBED4B8F0AD918205030DF8CB3791415483,
	DOTweenModulePhysics2D_DOMove_m129881ECAC47858FD6BDF4872DFC10009AA91774,
	DOTweenModulePhysics2D_DOMoveX_m58FD03E827955317D3A61BDE358C692C085BDC67,
	DOTweenModulePhysics2D_DOMoveY_m6CA0A894F764679E356EEAC3047063E66AE8507D,
	DOTweenModulePhysics2D_DORotate_m8FF3AA95400BD0ED1E539A13B9B33D8DCBE72CAE,
	DOTweenModulePhysics2D_DOJump_m2F08ADE05D23A793E24F4C9540D2159DADF8E655,
	U3CU3Ec__DisplayClass0_0__ctor_m4CB0F23C2FD78ED18921BA431C11EC8841878845,
	U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mACB8350BB439EC9BDAE8AAA9042BA1D2C2260840,
	U3CU3Ec__DisplayClass1_0__ctor_m51B26BC729B71979744C78CCF6EED60B2D14109B,
	U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m57744501683F232996387D707B7BD355F4F01547,
	U3CU3Ec__DisplayClass2_0__ctor_m657C45B791F1EA7BE1759E94B7B75A23390BA39A,
	U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_mB38D7BA656E8BFBDA07E30ADB33ED05B7EA2AAE5,
	U3CU3Ec__DisplayClass3_0__ctor_m478EC1218BEF6E85E06C40AB67A0D7463E600D49,
	U3CU3Ec__DisplayClass3_0_U3CDORotateU3Eb__0_m8C384490EBDB1B769B4D88A4182E40E46A2A380B,
	U3CU3Ec__DisplayClass4_0__ctor_mFF8025919ABCD075B0E67C3F07356DD3CFC60605,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__0_mE1774E966F8B4E8EC7EF821C59D3C5E8BF8FB071,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__1_m72CB74348BBB9FF7126C8670CA7A9E858279DB46,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__2_mB82162455CF0857F8934DC23F558BC2C6B2A51B1,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__3_m9EB716BAFAB813B716B78872E6249CE4405E4CEA,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__4_m6CF2D0D226343F9C2144DC7C2F468397EE74B74F,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__5_m8A4065ED3871AB729FEC6C9B32F403681C9ED206,
	DOTweenModuleSprite_DOColor_m1D8613D58BC87177BBFB8F8C48803084362CC46F,
	DOTweenModuleSprite_DOFade_mD42EB7C410F2D7E86EF154211E97C3D72FAB6D99,
	DOTweenModuleSprite_DOGradientColor_m1D99BA2B2479484B3104790FA7CF734042B56275,
	DOTweenModuleSprite_DOBlendableColor_m1CD3ADB6D2A6C868FA9A25527B33D1D36D3918D7,
	U3CU3Ec__DisplayClass0_0__ctor_mB3A38B7C4B2EBEC0CA4CB4BCD0996F70D0D38573,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m34BED3EF004629B2391C43F22D2ECC75EE20C056,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m8236490A867654780100440C6D27299ED4F26767,
	U3CU3Ec__DisplayClass1_0__ctor_m9A4008AB7FCC8A503A7CAED6CBBACDADF8E0A2F0,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_m5F0C4CC4570355DFDF8A0AC86BB45FA7127558B2,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_m353548C0500E2640824FDC1FE767DF2645A3FFE4,
	U3CU3Ec__DisplayClass3_0__ctor_m3C72E35866D164666CDD16DA3FD45D2AE46D9E24,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_m424C5E007014DBEFF422D4E1C016F8D14B192DE8,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_mE1792C420EAF86DF15D7258F252C269F8931578E,
	DOTweenModuleUI_DOFade_m70733D91ED98AB827DD5162868199BCA07B3C65F,
	DOTweenModuleUI_DOColor_m9CCD69C96E0EC2AF9796758A83BCAE38E8A99257,
	DOTweenModuleUI_DOFade_mCF2D26E252987610391CA58F42EBF89D497AB42C,
	DOTweenModuleUI_DOColor_mAB44E96C3BE821A668FD934C349F33DB052BD34B,
	DOTweenModuleUI_DOFade_m6E4705A5E978F074AC027B2622447F16464797C8,
	DOTweenModuleUI_DOFillAmount_m1701806AB0CE37D1A7BA5E97AC2A1CF2FE381A64,
	DOTweenModuleUI_DOGradientColor_mD41A204AAA1149EFD8F63003EFB3B5B86851393F,
	DOTweenModuleUI_DOFlexibleSize_mC1B116AD5E04DEE061C879DB93173AF3BF7BAFA2,
	DOTweenModuleUI_DOMinSize_m43AE2B4C0C85152C1F0C1A1A07C7C6647BCA50FB,
	DOTweenModuleUI_DOPreferredSize_m637389A7A4F0CAA593C1327F420E3F0CD7F4FBAC,
	DOTweenModuleUI_DOColor_mE71263080B1924D0D208D8D8B6E134726CF1C502,
	DOTweenModuleUI_DOFade_m9870943D62346E19D61067631D8E034995D0F093,
	DOTweenModuleUI_DOScale_mC78CB7454A8AA9419E06B38472C5751284669753,
	DOTweenModuleUI_DOAnchorPos_m5771DC15ABB88CC37D1A22B035E4D2FB638356B7,
	DOTweenModuleUI_DOAnchorPosX_mFC8A1A246CDB7E120FAC42CA43A7356F1F1A26B3,
	DOTweenModuleUI_DOAnchorPosY_m8371486009B6D9D9E5496D940591EBBBDE78AB51,
	DOTweenModuleUI_DOAnchorPos3D_m9152258BD8EF0A999624C76BC8149BD46607EF33,
	DOTweenModuleUI_DOAnchorPos3DX_mBC59977D6899FEAB2C3B45554EB28E27665577A3,
	DOTweenModuleUI_DOAnchorPos3DY_mD242D5809B7F59D96655054309976DD8C8A45B2A,
	DOTweenModuleUI_DOAnchorPos3DZ_m5CA72828C6290C08B5AE19AEEA4BACE05315583F,
	DOTweenModuleUI_DOAnchorMax_mF557034D626FD7CD2809F16F57071E5234A0A01D,
	DOTweenModuleUI_DOAnchorMin_m13FBF4F90CF58B50BDC27BAF0CBC7C4376DF4016,
	DOTweenModuleUI_DOPivot_m4294354A52355B2955965B3726DA2CC0A049F7A5,
	DOTweenModuleUI_DOPivotX_mAF48236E3A05FA8787920BE4F939A4E652D6EA1D,
	DOTweenModuleUI_DOPivotY_m478C274ACECBE8C0C0D9D43FF49DFF41DD28595F,
	DOTweenModuleUI_DOSizeDelta_mC80355FB2E78330D2EC907DC14F8B6A8179CB09A,
	DOTweenModuleUI_DOPunchAnchorPos_mA9B79DE480E85187CD9967AEAC55F29FB4C7B587,
	DOTweenModuleUI_DOShakeAnchorPos_m44811D5F919207AB4ACA282656C76F690E41A2C4,
	DOTweenModuleUI_DOShakeAnchorPos_mA36B5E94FEC480304F89946C695F8DCBF637DC72,
	DOTweenModuleUI_DOJumpAnchorPos_m0D6AE7D1C70F77C307C2F74A7DE5575B38310323,
	DOTweenModuleUI_DONormalizedPos_mA0AAE9E65E8F1808464E0EFE63F26A76D4C4D86B,
	DOTweenModuleUI_DOHorizontalNormalizedPos_mA1C4D197CF803EA1826D3D23A3A0DD665EDA535B,
	DOTweenModuleUI_DOVerticalNormalizedPos_m0CF6EC00B4AC886CEA5E8D99F9F5E26F254D52E0,
	DOTweenModuleUI_DOValue_mBD157558885F4CC9BB52696CA6E7FCFBBE289D31,
	DOTweenModuleUI_DOColor_mF7C94BF952B9549609B1F7A3892A6F02CEB56F04,
	DOTweenModuleUI_DOFade_m86A4E7965D2563BB1530EB2343AB8DA2EF7FDF01,
	DOTweenModuleUI_DOText_mB47F3908BD77013A7C5DF5CCFC2E3F4BB60E00F5,
	DOTweenModuleUI_DOBlendableColor_mF0936D1AF990F743A105B24F1D6988610F2496A9,
	DOTweenModuleUI_DOBlendableColor_m0E5CC97374C3B817CB3A04FFA87DCBBA545DB147,
	DOTweenModuleUI_DOBlendableColor_mAA670DFB22BE7C9256667D65F40FF487C2AECF14,
	Utils_SwitchToRectTransform_m927E0306521B99C5AF717563A4AB216A3846ACFC,
	U3CU3Ec__DisplayClass0_0__ctor_m02AC5FF72AD9F7271BAE9AB34F80D42AA9A9C484,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m160924332BA5EF66E12B6FE8BDA1DBC69059EAAD,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mCF3914E7F3A8F729B174AA946341922991E381CC,
	U3CU3Ec__DisplayClass1_0__ctor_m1C5DB32757E72E49AF272F1384712046C6A00253,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_m9467C7755B2D29380CDBCB4190BFC6A077E64C39,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m44B0F01B58106E8E85D97C2FBB8A460A0571FE77,
	U3CU3Ec__DisplayClass2_0__ctor_m4D9DFB7EE2AAC12968C4DF238BD125C654548FF1,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_mCAA3704209EDD53B81A477BF1AB73EA69F26F845,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m7B6FA582025F7A33540FA056A8EA424D3BD33BE7,
	U3CU3Ec__DisplayClass3_0__ctor_mC5C767AD26CB3FE0A493044C71B6CCEC8346C296,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m312CB07635E6BF7DDA4F7D69B3844ADDDF806465,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mC81F83EDCE139870DE9369D6478622169885365E,
	U3CU3Ec__DisplayClass4_0__ctor_m15E8C7F3799861E5CCA5AAD2CD1CD28224AA03E7,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m0B5F0F9301C56DDEB288ADD86745A1D635CE1416,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m18E36E9B1AFC305E378F79E25EE9A932B959588E,
	U3CU3Ec__DisplayClass5_0__ctor_m5DB0CA121DB5A86ADCD13BB04F71705F65B09E65,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m424FEBA1D754E6D82F1013FC993B129F0175C745,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m83B635790469064B562A721BEE7A6C83B43903C8,
	U3CU3Ec__DisplayClass7_0__ctor_m3F4D7A459A8733E43E7A64A569F9EA52B535D2B6,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_mF3831E19E398DAD98077121A417BFD6A74D3EBCB,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m605F371D6A593F9F1A3A249368409471CEAEC646,
	U3CU3Ec__DisplayClass8_0__ctor_mA6BE9E902B051E8864004E9F53E0D01796C27BEC,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m24F632B6998489E51C1E60F23FE8A05B9F9BF80A,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m5E36BA70B14EBA24EE5150746690346E78F966B0,
	U3CU3Ec__DisplayClass9_0__ctor_m286D59D82E41B5281985D79E488198A7E3537ACC,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mF535C0871CF853AED0D5A8ACB39FC367D1CB0A64,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m185139D594B32B631796CD61D79F0B48C5431765,
	U3CU3Ec__DisplayClass10_0__ctor_m11DD7C4B58AFC2CB99F683955907A640E74282C0,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_mAFCCC57700B3B00BDACF629FB898B3DEDF8C7668,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m15F5B7ECAA029DC5E8A50857A16A48A8A6D6992B,
	U3CU3Ec__DisplayClass11_0__ctor_m6D4ED0270AF0D5C0A75BF692250CB853EC2F6F79,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_m2D6173B6882E022ADB4AC6CBA3173304B8E6164F,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_mCE887CF626E5ACCB29B0E98C921AC7A3E9233A7E,
	U3CU3Ec__DisplayClass12_0__ctor_mD1F9075FEC29F490C6679A9D7598D759C0AB4DBE,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_m2385A603E1E66F822FECD5CEACCE771B5758D7B7,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_m99EB252D563A798BEE0B5A31C659851CD0700BAE,
	U3CU3Ec__DisplayClass13_0__ctor_m84FD04DEF9DEFE8BB6EE6B768471B2332B4E66EA,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_mDEEE77743125A9F4AF071993A48C0883B8938147,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_mEB2040061609C631073D063D8BA71C5AE458CB63,
	U3CU3Ec__DisplayClass14_0__ctor_m188221CF95D995E27ABFF9D447F3D54149D4B5F2,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m18F22E4AA3CBA7FEFF94AA2AA7438EBC5C4DBF3D,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m76DB47AD4C1DD0E522CD7E49E4C0BF890512FCC7,
	U3CU3Ec__DisplayClass15_0__ctor_m1D4F00D13860F242587ACB9888B000C3A767492B,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_m61698EC758F0F82BBA818949C0BD2F3CFADFAA57,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_mD35972634DF21705BA6A9B3DB5CF0D81CECC219B,
	U3CU3Ec__DisplayClass16_0__ctor_mC9174DECC9AB0C0FCA7E0BB5626F00A397552285,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m04D393C6058D10956299620A3367587B40EADADF,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m29D334493F9D5F6BCADC9E5725DF0A510EA47208,
	U3CU3Ec__DisplayClass17_0__ctor_mDCEE61F98BC23EF18977566788E29085B0DC4B49,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_m3F4080C62B849D50FF7C58A8257FFEE91BBAB680,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_m4263B6642D9E77CB38E01A24F3C737199ACC9F93,
	U3CU3Ec__DisplayClass18_0__ctor_m8463A61BCCD89F89CBF0F79CF66FA4EBE2FD1E4F,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m256585104D3EF107F980E00EBCA0AFE9AD81F6CF,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_m9A7522C9582F9572AF53FFCE8D3D745BE00DB9C0,
	U3CU3Ec__DisplayClass19_0__ctor_m15B509E8ABCF2C177192FF9870B15FEE66F4D038,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_m7C246FCA6BAD4A13638C1361F6680CF5211DFD15,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_m49E6856829E99FE417EC50159BC5FB0D1A77CBD7,
	U3CU3Ec__DisplayClass20_0__ctor_mDD65EA9F35AA085EDC5AB6EA7BCB430AB53E7C4C,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_mD207906E09F8A121195553ACBF187B8D0A3A6006,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m25FD7BC6C81DCBCB7936948A09EA50E27D3DF5E1,
	U3CU3Ec__DisplayClass21_0__ctor_m7CA9A2889EDB39EBF81C4320023731198C2E28F6,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_mB7F92CB811EC69B78D4DB72A07EBFE0065B81A96,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_mE3580800BF86444C99AFAD3651A10A8BA87ACA3D,
	U3CU3Ec__DisplayClass22_0__ctor_mE2C45E43AE15B030C3E9EA28C6FB89B27993353C,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_m965979FB6B275C69A73170025341021C75A3BD73,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_m4A27FCA6236E27F8F1156DF1A947D5E14C97900E,
	U3CU3Ec__DisplayClass23_0__ctor_mAFBDA472706665D20FBB1B5C8CE8C560842578A1,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mB04DAF8BA4880841E68A83B7182EB53669831FFD,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_mEE841F6ADEDD8942A5BAB79D9F181646759EE2B0,
	U3CU3Ec__DisplayClass24_0__ctor_mC59776621BA36942C91F3BFED7DE4391B0625F07,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_m1DF542C0FC15A842BC687B1294E2DE72BFF44B86,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_mB43317F02A9C33D46CE93B16DD1104D28BFFE75E,
	U3CU3Ec__DisplayClass25_0__ctor_mCF92B4AFC980734E8F9FBD06D7709D5B610C2AD8,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_m2D894FC2DD5FE9C3641758DA07FBB2415E191750,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_mAB31C40435305381E674D7BCDD36C1FA2D944535,
	U3CU3Ec__DisplayClass26_0__ctor_mE11542986539E00ECC4ACB110B932CB659280B9E,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_m8C345DF5B27AE5B17D74FCDB6CC68D367576F8F5,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_mF593BD39BAA9EA695EA52E6B025249916A3ACB21,
	U3CU3Ec__DisplayClass27_0__ctor_m3F5D43CB088C6A3C4D6CC51DD761FA23F832B806,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m0958BB22CF771F2BE804E7BDD83C361073F3581E,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mA6D65C9BAD704941C13ACD1DE3D3A6E8FA58A6B1,
	U3CU3Ec__DisplayClass28_0__ctor_m671FD2A8D4DC6429FA93C8A41C7AFC6B0CB50CCF,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_m71D55809BE5527F7FA76198ADEAE5E5917555699,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_m7A7AAA1B4D13FE941B7051A2395795B2019BA497,
	U3CU3Ec__DisplayClass29_0__ctor_m6841EBC72678519402C9678A4903856BA01D3E3C,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_mDD03C988E48C946E2051BBCB400D11AA475172C5,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_m0A7D3990B8A6B423B590109635761C4D7845AB7F,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_mB2EB9443C133F983C1AA76FAB66C80F28853B8F4,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_m7E2202F4B18C4169BD217DE526E9543C40EF29C0,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_mDA917A9381F14BC3DACB27C22CAF05FE45DBAC90,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_m3CC81967E2004B932868626D598B72943C76C21F,
	U3CU3Ec__DisplayClass30_0__ctor_m475CA4BAAE3F4C95D59EC2A5210019E22FD7E72D,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_m1F0943C5D1EA2BAE72905488B7BB7FD05AD5414B,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_m8CE913B6CB904A504400097D0AA6EDDBD7AE0B45,
	U3CU3Ec__DisplayClass31_0__ctor_m8CB90BDE592351E68684EB4D199BB309EC06F466,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_mDF3CB62908D4575E14BF7E98CA262F8149BB0E48,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mEB5E8758E0EF969FFADC04CA3147F5A888B40064,
	U3CU3Ec__DisplayClass32_0__ctor_m1A8D52B69FB8395ABBC9A4F7DF8E36466DB3BC95,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_m71DE61BFA6209DB829836838A7F1EB7F5C1C951C,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_mCB1CE1E45A17FD19D929A7C3EA95C7B492AFCCA2,
	U3CU3Ec__DisplayClass33_0__ctor_m0DF68C5C8156D45B1870272C41290E5B12FF8602,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_m145C72DAB8DD1E5AD043A2B5900812E0F0E58E83,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_mF3EAA5B02B860189FE7841E1642095AF9488F134,
	U3CU3Ec__DisplayClass34_0__ctor_m4E36DF0E111FF20A584BABCB272C7514CBAF59A2,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_m47F62A48B92963FB32511F3875D566F8FC71480B,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_m9541F5AD304519AFA79192889487ABC2E621EE3F,
	U3CU3Ec__DisplayClass35_0__ctor_mEB07972AA46E127DB75E68DDA3FB5A54DFE1A07C,
	U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__0_m8FFA77E8FB4FCE8F0125962D1192E052256AA512,
	U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__1_m857FA607EA2B021E9A5DF0AFA774242459924E3E,
	U3CU3Ec__DisplayClass36_0__ctor_mD71C2FD9E280F3A4DC631CA425A24E1AE0265CC8,
	U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__0_mE1A296C1F72132B2BF559B54C734121CF98679DC,
	U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__1_mDB35377C8C2F3FCAA36BDC6104B6123A01C159E7,
	U3CU3Ec__DisplayClass37_0__ctor_mDA142B56ABBC2597FCB543F5EA51FDE52DDA91E5,
	U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__0_mA59968C9C7B4E749CB08FBD0B6EEF08A1B66B362,
	U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__1_m6FD982BE0D5D355F1C0CF61FEB38A244956BE6E8,
	U3CU3Ec__DisplayClass38_0__ctor_mF5E51CB763FACE84BD5CD7EBDA60614BF88959E9,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_m5CA9724149FCD3E48658442157DA3249D8966F6C,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_m800BBDFF60B762B724C762D9C29F15474FB8A698,
	U3CU3Ec__DisplayClass39_0__ctor_m309F737816844D4A4CDC9518D1DE826E30C6D271,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_mFFFA826A2C8066C0B5695C8153E6788167CCAD86,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m7C0B3D7601C4F8711E55BDE39FF63F235CE9F904,
	DOTweenModuleUnityVersion_DOGradientColor_mC9C2768747A9B142795620CF5501BCCB0385A64A,
	DOTweenModuleUnityVersion_DOGradientColor_m8B5EA2127C5D9206A1BC6BAC15BD89D5D9077DCC,
	DOTweenModuleUnityVersion_WaitForCompletion_mCD910620513CFC66E6360BDF100567E534DA8C47,
	DOTweenModuleUnityVersion_WaitForRewind_m80AF71AC60E938C7E0D4FD01CCBF98667DEDB8A1,
	DOTweenModuleUnityVersion_WaitForKill_m5D2A034D4E6912EE8FF0AE063329807F3399FF95,
	DOTweenModuleUnityVersion_WaitForElapsedLoops_m347E45F2B3E7F133E50D93DE5712D9FB0E828A79,
	DOTweenModuleUnityVersion_WaitForPosition_m39F8AC8FBB5962A4CC8CEFB055C85B13284CC39F,
	DOTweenModuleUnityVersion_WaitForStart_mE7E9F02D5CAF079B0F1C2002B633EA88C18AF3D1,
	DOTweenModuleUnityVersion_DOOffset_m5FE227A1D7FE40DEBFD0D15E9E2E140611E49BD4,
	DOTweenModuleUnityVersion_DOTiling_m46B35DDADB688BEB85EB21A5593E240C5FD990A6,
	U3CU3Ec__DisplayClass8_0__ctor_m4E9AD608BFA5058EA275819037B5EE4E384311AA,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m313F8C26E8A72E7404065072AA78E9C737E35620,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m4E8A169B481398D90A901BAD32A1CC0BDD77BAE8,
	U3CU3Ec__DisplayClass9_0__ctor_m34A953C85477B2BD1B635BDAF8885267ACDAFEEA,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m32DCD53537E5D9250D043396A6636431DA4AC5DE,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_m586EE9E78A2967588A473314F2E4DDF7852A89A7,
	WaitForCompletion_get_keepWaiting_mC42F1D87B76571FEF5C96EB25CD2F233A7FBDDE2,
	WaitForCompletion__ctor_m194FFB6935EC44D8EBF9596755B410A7F3CAECDA,
	WaitForRewind_get_keepWaiting_mDC4ACCB00D1A64C9D59ED14620D3472036D2755E,
	WaitForRewind__ctor_m8FA908A77DFFAACB814B717915EAFBDA10B2FB36,
	WaitForKill_get_keepWaiting_mC3BC5155F3822FA2486800624680EA65464044D4,
	WaitForKill__ctor_m13C20AC2C9C2809AD51B8CEA30F11AACB4ECC64C,
	WaitForElapsedLoops_get_keepWaiting_mD62F90B0E8F34349F6CBCAD1DEE8C69F6AC9A4A0,
	WaitForElapsedLoops__ctor_mA2399269DDE36055552944650E8862464AAFF448,
	WaitForPosition_get_keepWaiting_mCE1EBD6DFC3462C603585CE8DD978F582FC0ADA6,
	WaitForPosition__ctor_m218039DD8AEC82CDB8BF269764EC8566A4E01F6F,
	WaitForStart_get_keepWaiting_mF6E49FFC399C5642C630865FCCB38FA56DCA890D,
	WaitForStart__ctor_m733FFDFE0C36F20AA645593347BDAA8D0610D508,
	DOTweenModuleUtils_Init_m9F7938AFFD814978DEE9140ED34B6E6647711324,
	DOTweenModuleUtils_Preserver_m5BBFCFD03A1E22FF9651F2407FB32FBDACFBEFDD,
	Physics_SetOrientationOnPath_m7909652CCC78E77ADE33C5BEB4BFF42AA536A7D1,
	Physics_HasRigidbody2D_m7B055F7D6B725DCE1616DE1BCAC6AF1E402946E6,
	Physics_HasRigidbody_m49C216B000D976B6AD22EA36C29BB922F4B02A2B,
	Physics_CreateDOTweenPathTween_mFA2C0271A146797AB71CD136F2A56ECA3A5AAB5C,
};
extern void Enumerator_get_IsValid_mBC273331DC1699FF46BD3621AE5059A54AD98BA6_AdjustorThunk (void);
extern void Enumerator__ctor_mF21239C69620D815F8CD34F022BE18E9DAF9CB10_AdjustorThunk (void);
extern void Enumerator__ctor_mAC4ED0FA4B083E2652E865A41EA5C74A49478EFE_AdjustorThunk (void);
extern void Enumerator_get_Current_mDE6750203413E1069D0520793D6AA0B2527CB20E_AdjustorThunk (void);
extern void Enumerator_MoveNext_m238CF072385A1106BEDEFCE33BA2B0DBE999758A_AdjustorThunk (void);
extern void ValueEnumerator__ctor_mCC61CE3EDCF1AC94A84E031F2E89F8054C94A015_AdjustorThunk (void);
extern void ValueEnumerator__ctor_m122732DF448B45E8E82956E07AC8314C60E28C29_AdjustorThunk (void);
extern void ValueEnumerator__ctor_m7BA4BAD5FEBAC4054F71575B728DC27EC4080F0A_AdjustorThunk (void);
extern void ValueEnumerator_get_Current_mAA24A52FDEB7160BD268193175388EACB41B7CE2_AdjustorThunk (void);
extern void ValueEnumerator_MoveNext_m5B596A2EF2FF395EDA8F5CAB97C0789498D250C9_AdjustorThunk (void);
extern void ValueEnumerator_GetEnumerator_m765261287A2C0AEF757B94994826F43951387E4C_AdjustorThunk (void);
extern void KeyEnumerator__ctor_m6EA81E2BED4CA5194A7306D8B324F7356E37F80A_AdjustorThunk (void);
extern void KeyEnumerator__ctor_mA6338E82A9F8AA19A1744352B4FE54103AD70405_AdjustorThunk (void);
extern void KeyEnumerator__ctor_m526EA1364C367B83C931F4208CDD816BD02810EA_AdjustorThunk (void);
extern void KeyEnumerator_get_Current_m8FBFEE52D4438AAF3E10AB4370B34FBB8E66B3C2_AdjustorThunk (void);
extern void KeyEnumerator_MoveNext_m42FE2CEE808A7E065895BA333B7FBD2F3AEE032F_AdjustorThunk (void);
extern void KeyEnumerator_GetEnumerator_mD4687B4D6D10E4D6870CBBECC680689A62A95C0B_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[17] = 
{
	{ 0x0600018F, Enumerator_get_IsValid_mBC273331DC1699FF46BD3621AE5059A54AD98BA6_AdjustorThunk },
	{ 0x06000190, Enumerator__ctor_mF21239C69620D815F8CD34F022BE18E9DAF9CB10_AdjustorThunk },
	{ 0x06000191, Enumerator__ctor_mAC4ED0FA4B083E2652E865A41EA5C74A49478EFE_AdjustorThunk },
	{ 0x06000192, Enumerator_get_Current_mDE6750203413E1069D0520793D6AA0B2527CB20E_AdjustorThunk },
	{ 0x06000193, Enumerator_MoveNext_m238CF072385A1106BEDEFCE33BA2B0DBE999758A_AdjustorThunk },
	{ 0x06000194, ValueEnumerator__ctor_mCC61CE3EDCF1AC94A84E031F2E89F8054C94A015_AdjustorThunk },
	{ 0x06000195, ValueEnumerator__ctor_m122732DF448B45E8E82956E07AC8314C60E28C29_AdjustorThunk },
	{ 0x06000196, ValueEnumerator__ctor_m7BA4BAD5FEBAC4054F71575B728DC27EC4080F0A_AdjustorThunk },
	{ 0x06000197, ValueEnumerator_get_Current_mAA24A52FDEB7160BD268193175388EACB41B7CE2_AdjustorThunk },
	{ 0x06000198, ValueEnumerator_MoveNext_m5B596A2EF2FF395EDA8F5CAB97C0789498D250C9_AdjustorThunk },
	{ 0x06000199, ValueEnumerator_GetEnumerator_m765261287A2C0AEF757B94994826F43951387E4C_AdjustorThunk },
	{ 0x0600019A, KeyEnumerator__ctor_m6EA81E2BED4CA5194A7306D8B324F7356E37F80A_AdjustorThunk },
	{ 0x0600019B, KeyEnumerator__ctor_mA6338E82A9F8AA19A1744352B4FE54103AD70405_AdjustorThunk },
	{ 0x0600019C, KeyEnumerator__ctor_m526EA1364C367B83C931F4208CDD816BD02810EA_AdjustorThunk },
	{ 0x0600019D, KeyEnumerator_get_Current_m8FBFEE52D4438AAF3E10AB4370B34FBB8E66B3C2_AdjustorThunk },
	{ 0x0600019E, KeyEnumerator_MoveNext_m42FE2CEE808A7E065895BA333B7FBD2F3AEE032F_AdjustorThunk },
	{ 0x0600019F, KeyEnumerator_GetEnumerator_mD4687B4D6D10E4D6870CBBECC680689A62A95C0B_AdjustorThunk },
};
static const int32_t s_InvokerIndices[864] = 
{
	6077,
	4863,
	9058,
	6077,
	4301,
	4863,
	4863,
	6077,
	6077,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	6077,
	6077,
	4863,
	4770,
	4863,
	6077,
	6077,
	4301,
	6077,
	4835,
	6077,
	5876,
	6077,
	5966,
	6077,
	5966,
	6077,
	6077,
	6077,
	1025,
	6077,
	6077,
	4301,
	4301,
	6077,
	4835,
	6077,
	5876,
	6077,
	5966,
	6077,
	5966,
	4835,
	6077,
	5876,
	6077,
	5966,
	6077,
	5966,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	4301,
	6077,
	4835,
	6077,
	5876,
	6077,
	5966,
	6077,
	5966,
	6077,
	6077,
	4863,
	6077,
	6077,
	6077,
	6077,
	5966,
	6077,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	1317,
	6077,
	1317,
	6077,
	5966,
	4863,
	5966,
	4863,
	5966,
	4863,
	5966,
	4863,
	5966,
	4863,
	6077,
	6077,
	4863,
	4863,
	2761,
	2761,
	1482,
	1482,
	1505,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	5966,
	6077,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	5966,
	6077,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	5966,
	6077,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	6077,
	6077,
	6077,
	6077,
	6077,
	4301,
	5966,
	6077,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	6077,
	5966,
	6077,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	6077,
	6077,
	6077,
	6077,
	4863,
	4301,
	4301,
	6077,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	6077,
	6077,
	5966,
	5966,
	6077,
	9424,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	6077,
	6077,
	6077,
	6077,
	4835,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	6077,
	4835,
	6077,
	6077,
	6077,
	2761,
	2761,
	1482,
	1482,
	1505,
	6077,
	6077,
	6077,
	6077,
	4863,
	4863,
	6077,
	6077,
	6077,
	6077,
	4863,
	6077,
	4863,
	4863,
	4863,
	4863,
	4835,
	6077,
	6077,
	6077,
	6077,
	4835,
	6077,
	6077,
	6077,
	5966,
	6077,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	6077,
	6077,
	6077,
	6077,
	4863,
	5966,
	6077,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	6077,
	6077,
	6077,
	6077,
	4863,
	5966,
	6077,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	6077,
	6077,
	6077,
	6077,
	4863,
	5966,
	6077,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	6077,
	6077,
	6077,
	6077,
	4863,
	5966,
	6077,
	6077,
	1935,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	6077,
	6077,
	4301,
	5966,
	6077,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	0,
	4298,
	2510,
	4301,
	2702,
	5966,
	4863,
	5935,
	5876,
	5876,
	5876,
	5876,
	5876,
	5876,
	5876,
	4770,
	2702,
	4863,
	4301,
	4298,
	4301,
	5966,
	5966,
	5966,
	4298,
	0,
	0,
	5966,
	6160,
	6161,
	5903,
	4800,
	5935,
	4835,
	6012,
	4901,
	5876,
	4770,
	5936,
	4836,
	5966,
	5966,
	9058,
	9058,
	9048,
	8872,
	9066,
	9127,
	9055,
	8917,
	9056,
	8934,
	9042,
	8815,
	8979,
	8037,
	8037,
	3497,
	5935,
	9391,
	9058,
	7290,
	9058,
	6077,
	5876,
	4528,
	4530,
	5712,
	5876,
	4528,
	4530,
	5032,
	5966,
	5876,
	6161,
	4528,
	4530,
	5032,
	5966,
	5876,
	6160,
	4863,
	5712,
	5966,
	5876,
	6077,
	5966,
	6077,
	5966,
	4835,
	6077,
	5876,
	5966,
	6077,
	5966,
	5966,
	5966,
	4835,
	6077,
	5876,
	6077,
	6077,
	5966,
	6077,
	5966,
	5966,
	5966,
	5876,
	4770,
	5935,
	5876,
	6159,
	4298,
	2510,
	4301,
	2702,
	5935,
	2702,
	4298,
	4301,
	5966,
	995,
	6077,
	4835,
	6077,
	5876,
	6077,
	5966,
	6077,
	5966,
	5966,
	5966,
	5876,
	4770,
	5935,
	5876,
	6159,
	4301,
	2702,
	4298,
	2510,
	5935,
	2702,
	4301,
	4298,
	4301,
	5966,
	995,
	6077,
	6077,
	3217,
	4835,
	6077,
	5876,
	6077,
	5966,
	6077,
	5966,
	5966,
	5966,
	5935,
	5876,
	6159,
	5966,
	4863,
	4863,
	995,
	3497,
	5935,
	5935,
	5876,
	6159,
	5966,
	4863,
	5903,
	4800,
	5936,
	4836,
	4800,
	4863,
	995,
	8815,
	3497,
	5935,
	5935,
	5876,
	6159,
	5966,
	4863,
	5876,
	4770,
	4770,
	4863,
	995,
	3497,
	5935,
	9391,
	6077,
	5935,
	5876,
	6159,
	5966,
	4863,
	5876,
	4770,
	3497,
	5935,
	995,
	9424,
	5935,
	6159,
	4863,
	2702,
	4863,
	4298,
	2510,
	4301,
	2702,
	4863,
	2702,
	8037,
	8037,
	3497,
	5935,
	5935,
	4835,
	6012,
	4901,
	5903,
	4800,
	5936,
	4836,
	5876,
	4770,
	5966,
	5966,
	995,
	9058,
	7610,
	7610,
	7148,
	8225,
	8225,
	8917,
	7514,
	8917,
	8917,
	8917,
	8917,
	8917,
	8917,
	8917,
	8917,
	6077,
	6012,
	4901,
	6077,
	6012,
	4901,
	6077,
	6012,
	4901,
	7177,
	7162,
	7162,
	7162,
	7178,
	6675,
	6407,
	6313,
	6313,
	7147,
	7147,
	6077,
	6067,
	6077,
	6067,
	6077,
	6067,
	6077,
	6067,
	6077,
	5981,
	6077,
	5981,
	6077,
	6067,
	6077,
	6067,
	6067,
	6077,
	6077,
	6067,
	6077,
	6067,
	4948,
	6077,
	6067,
	6077,
	6067,
	4948,
	7174,
	7162,
	7162,
	7610,
	6406,
	6077,
	6065,
	6077,
	6065,
	6077,
	6065,
	6077,
	6012,
	6077,
	6065,
	4946,
	6077,
	6065,
	4946,
	6077,
	7589,
	7610,
	7605,
	7589,
	6077,
	5879,
	4773,
	6077,
	5879,
	4773,
	6077,
	5879,
	4773,
	7610,
	7589,
	7610,
	7589,
	7610,
	7610,
	7605,
	7174,
	7174,
	7174,
	7589,
	7610,
	7618,
	7174,
	7162,
	7162,
	7177,
	7162,
	7162,
	7162,
	7174,
	7174,
	7618,
	7610,
	7610,
	7174,
	6406,
	6317,
	6318,
	6406,
	7174,
	7162,
	7162,
	7162,
	7589,
	7610,
	6401,
	7589,
	7589,
	7589,
	8438,
	6077,
	6012,
	4901,
	6077,
	5879,
	4773,
	6077,
	5879,
	4773,
	6077,
	5879,
	4773,
	6077,
	5879,
	4773,
	6077,
	6012,
	4901,
	6077,
	6065,
	4946,
	6077,
	6065,
	4946,
	6077,
	6065,
	4946,
	6077,
	5879,
	4773,
	6077,
	5879,
	4773,
	6077,
	6065,
	4946,
	6077,
	6065,
	4946,
	6077,
	6065,
	4946,
	6077,
	6065,
	4946,
	6077,
	6067,
	4948,
	6077,
	6067,
	4948,
	6077,
	6067,
	4948,
	6077,
	6067,
	4948,
	6077,
	6065,
	4946,
	6077,
	6065,
	4946,
	6077,
	6065,
	4946,
	6077,
	6065,
	4946,
	6077,
	6065,
	4946,
	6077,
	6065,
	4946,
	6077,
	6067,
	4948,
	6077,
	6067,
	4948,
	6077,
	6067,
	4948,
	6077,
	6065,
	4946,
	6077,
	6065,
	4946,
	6077,
	6077,
	6065,
	4946,
	6077,
	6012,
	4901,
	6077,
	6012,
	4901,
	6077,
	6012,
	4901,
	6077,
	5879,
	4773,
	6077,
	5879,
	4773,
	6077,
	5966,
	4863,
	6077,
	5879,
	4773,
	6077,
	5879,
	4773,
	6077,
	5879,
	4773,
	7605,
	7144,
	8310,
	8310,
	8310,
	7594,
	7608,
	8310,
	7172,
	7172,
	6077,
	6065,
	4946,
	6077,
	6065,
	4946,
	5876,
	4863,
	5876,
	4863,
	5876,
	4863,
	5876,
	2697,
	5876,
	2707,
	5876,
	4863,
	9424,
	9424,
	7302,
	8815,
	8815,
	6391,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	864,
	s_methodPointers,
	17,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
